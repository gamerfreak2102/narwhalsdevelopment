--[[---------------------------------------------------------------------------
DarkRP custom jobs
---------------------------------------------------------------------------
This file contains your custom jobs.
This file should also contain jobs from DarkRP that you edited.
Note: If you want to edit a default DarkRP job, first disable it in darkrp_config/disabled_defaults.lua
	Once you've done that, copy and paste the job to this file and edit it.
The default jobs can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/jobrelated.lua
For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomJobFields
Add jobs under the following line:
---------------------------------------------------------------------------]]
TEAM_RECRUIT = DarkRP.createJob("Recruit", {  
   color = Color(0, 51, 0, 255),
   model = {"models/ishi/halo_rebirth/player/offduty/male/offduty_eyecberg.mdl"},
   description = [[A UNSC Recruit, Pledged to fight in the Human-Covenant war, They trained in camp before they were stationed with the 405th]],
   weapons = {},
   command = "recruit",
   max = 0,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false, 
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC",
   comms = {
      ["RP"] = true,
      ["OPEN"] = true,
   },
})

TEAM_KRIFLEMAN = DarkRP.createJob("Lima 1-1 SDM", { 
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_br55", "tfa_revival_m6g", "tfa_revival_m7", "climb_swep2", "keys", "revival_lima"},
   command = "krifleman",
   max = 20,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   ammo = {["Grenade"] = 2},
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=60
   ply:SetMaxHealth(250)
end
})

TEAM_WSUPPORT = DarkRP.createJob("Lima 2-1 Support", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_g_michael.mdl","models/ishi/halo_rebirth/player/marines/female/marine_linda.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},   
   weapons = {"tfa_revival_m247_gpmg","tfa_revival_ma5b","tfa_rebirth_m6", "tfa_revival_m41", "revival_lima","keys","climb_swep2", "weapon_vj_flaregun"},
   description = [[Fearless, Ambitious and Loyal, Lima 2-1 was one of the few marine regiments to storm the front lines.]],
   command = "wsupport",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 2-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["WHISKEY"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=63
   ply:SetMaxHealth(275)
end
})
TEAM_WSHARPSHOOTER = DarkRP.createJob("Lima 2-1 Scout Sniper", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_heretic.mdl","models/ishi/halo_rebirth/player/marines/female/marine_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Fearless, Ambitious and Loyal, Lima 2-1 was one of the few marine regiments to storm the front lines.]],
   weapons = {"tfa_revival_srs992d","tfa_revival_m6s","revival_lima","tfa_revival_m7s", "tfa_revival_br55", "climb_swep2","keys", "weapon_vj_flaregun"},
   command = "wsharpshooter",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 2-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["WHISKEY"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(225)
   ply.HaloArmor=63
   ply:SetMaxHealth(225)
end
})
TEAM_WRIFLEMAN = DarkRP.createJob("Lima 2-1 CQC Specialist", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_g_yasser.mdl", "models/ishi/halo_rebirth/player/marines/female/marine_dominique.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Fearless, Ambitious and Loyal, Lima 2-1 was one of the few marine regiments to storm the front lines.]],
   weapons = {"tfa_revival_m45s","tfa_revival_ma5c","revival_lima","tfa_revival_m7s","climb_swep2","tfa_revival_m6s","keys"},
   command = "wrifleman",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 2-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["WHISKEY"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=58
   ply:SetMaxHealth(250)
end
})
TEAM_WCORPSMAN = DarkRP.createJob("Lima 2-1 Corpsman", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_g_gilberto.mdl","models/ishi/halo_rebirth/player/marines/female/marine_ltd.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Fearless, Ambitious and Loyal, Lima 2-1 was one of the few marine regiments to storm the front lines.]],
   weapons = {"tfa_revival_m7s","tfa_revival_m6s","tfa_revival_m45s","weapon_bactainjector", "tfa_revival_br55","revival_lima","med_kit","weapon_defibrillator","keys", "climb_swep2"},
   command = "wcorpsman",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 2-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["WHISKEY"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=43
   ply:SetMaxHealth(250)
end
})
TEAM_VSUPPORT = DarkRP.createJob("Lima 1-1 Support", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_g_michael.mdl","models/ishi/halo_rebirth/player/marines/female/marine_linda.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_ma5b","tfa_revival_m6g","revival_lima","keys","tfa_revival_saw","climb_swep2", "weapon_vj_flaregun"},
   command = "vsupport",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=60
   ply:SetMaxHealth(250)
end
})
TEAM_VSHARPSHOOTER = DarkRP.createJob("Lima 1-1 Scout Sniper", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_heretic.mdl","models/ishi/halo_rebirth/player/marines/female/marine_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_srs992d","tfa_revival_m6g", "tfa_revival_m7", "revival_lima","keys", "climb_swep2", "weapon_vj_flaregun"},
   command = "vsharpshooter",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=60
   ply:SetMaxHealth(250)
end
})
TEAM_VRIFLEMAN = DarkRP.createJob("Lima 1-1 Rifleman", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_ma5b","tfa_revival_m6g", "tfa_revival_m7","revival_lima","keys","climb_swep2"},
   command = "vrifleman",
   max = 20,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=60
   ply:SetMaxHealth(250)
end
})
TEAM_VCORPSMAN = DarkRP.createJob("Lima 1-1 Corpsman", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_g_gilberto.mdl","models/ishi/halo_rebirth/player/marines/female/marine_ltd.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_ma5b", "tfa_revival_m6g", "weapon_bactainjector", "med_kit", "revival_lima", "weapon_defibrillator", "keys", "climb_swep2"},
   command = "vcorpsman",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=60
   ply:SetMaxHealth(250)
end
})
TEAM_VSQUADPLEAD = DarkRP.createJob("Lima 1-1 Platoon Lead", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_g_michael.mdl","models/ishi/halo_rebirth/player/marines/female/marine_linda.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_br55","tfa_revival_m45s", "tfa_revival_m7","revival_lima","tfa_revival_m6g","keys","voice_amplifier","climb_swep2", "weapon_vj_flaregun", "dk_flare_gun"},
   command = "vsquadpleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=60
   ply:SetMaxHealth(250)
end
})
TEAM_HELLSANGELS = DarkRP.createJob("Hells Angels", {
   color = Color(0, 107, 31, 255),
   model = {"models/kaesar/mechmule/mechmule.mdl"},
   description = [[Marine Mech]],
   weapons = {"weapon_op_gau2", "tfa_revival_m41"},
   command = "hellsangel",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
   faction = 1,
   ammo = {["Grenade"] = 2},
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
   },
   PlayerSpawn = function(ply)
   ply:SetHealth(1000)
   ply.HaloArmor=85
   ply:SetMaxHealth(1000)
   ply:SetArmor(800)
   ply:SetRunSpeed(220)
   ply:SetWalkSpeed(180)
   end
})
TEAM_LSUPPORT = DarkRP.createJob("Chimera Support", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_saw","tfa_rebirth_ma5c","revival_chimera","tfa_revival_m6g","keys","climb_swep2", "weapon_vj_flaregun"},
   command = "lsupport",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=48
   ply:SetMaxHealth(275)
end
})
TEAM_LSHARPSHOOTER = DarkRP.createJob("Chimera Sharpshooter", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_srs99am","revival_chimera","tfa_rebirth_ma5c","keys","climb_swep2", "weapon_vj_flaregun"},
   command = "lsharpshooter",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=45
   ply:SetMaxHealth(275)
end
})
TEAM_LRIFLEMAN = DarkRP.createJob("Chimera Rifleman", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_m394","tfa_rebirth_ma5c","revival_chimera","tfa_revival_m6g","keys","climb_swep2"},
   command = "zrifleman",
   max = 20,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=45
   ply:SetMaxHealth(250)
end
})
TEAM_LCORPSMAN = DarkRP.createJob("Chimera Combat Medic", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5c", "weapon_bactainjector","med_kit","revival_chimera","weapon_defibrillator","tfa_revival_m6g","keys","climb_swep2","tfa_revival_m7"},
   command = "lcorpsman",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=45
   ply:SetMaxHealth(250)
end
})
TEAM_LIMAACTUAL = DarkRP.createJob("Chimera Actual", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_ma5c", "revival_chimera", "tfa_revival_m6g", "keys", "voice_amplifier", "tfa_revival_m90c", "climb_swep2", "dk_flare_gun"},
   command = "lima1actual",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=45
   ply:SetMaxHealth(250)
end
})
TEAM_BULLFROG = DarkRP.createJob("Bullfrog", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_revival_m90c", "tfa_revival_m7s", "tfa_revival_m247_gpmg", "revival_bullfrogs", "tfa_revival_m6s", "revival_c4", "climb_swep2"},
   command = "bullfrog",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Bullfrogs",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=65
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGR = DarkRP.createJob("Bullfrog Rifleman", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_revival_m7s", "tfa_revival_m6s","revival_bullfrogs", "tfa_revival_ma5c", "tfa_revival_br55", "revival_c4", "keys", "climb_swep2"},
   command = "bullfrogr",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Bullfrogs",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=67
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGMED = DarkRP.createJob("Bullfrog Corpsman", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_revival_m7s", "tfa_revival_m6s", "tfa_revival_m90c","revival_bullfrogs","tfa_revival_br55","revival_c4","weapon_bactainjector","weapon_defibrillator","keys","Med_kit","climb_swep2"},
   command = "bullfrogcorp",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Bullfrogs",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=66
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGDEMO = DarkRP.createJob("Bullfrog Demolitionist", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_revival_saw", "tfa_revival_m6s", "revival_bullfrogs", "tfa_revival_m41", "revival_c4","keys","climb_swep2", "tfa_revival_m7s"},
   command = "bullfrogdemo",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Bullfrogs",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=67
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGSNIPER = DarkRP.createJob("Bullfrog Marksman", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_revival_m7s", "tfa_revival_m6s","revival_bullfrogs","revival_c4", "tfa_rebirth_m394", "tfa_revival_srs994am","climb_swep2", "weapon_vj_flaregun"},
   command = "bullfrogsniper",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Bullfrogs",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=66
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGCO = DarkRP.createJob("Bullfrog Squad Leader", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_revival_m45s", "tfa_revival_m7s", "tfa_revival_m6s","revival_bullfrogs", "revival_c4","voice_amplifier","tfa_revival_ma5c","climb_swep2", "weapon_vj_flaregun", "dk_flare_gun"},
   command = "bullfrogsquadleader",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Bullfrogs",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=67
   ply:SetMaxHealth(300)
end
})
TEAM_TRIFLEMAN = DarkRP.createJob("Tombstone 2-4 Rifleman", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"weapon_cuff_mp", "tfa_revival_m7s", "tfa_revival_m6s","revival_tombstone","tfa_revival_m45s","tfa_revival_br55","weapon_sh_flashbang","keys","climb_swep2"},
   command = "trifleman",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Tombstone",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["TOMBSTONE"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(400)
end
})
TEAM_TSHARPSHOOTER = DarkRP.createJob("Tombstone 2-4 Sharpshooter", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"Weapon_cuff_mp", "tfa_revival_m7s","tfa_revival_m6s","revival_tombstone","tfa_revival_srs994am", "tfa_revival_m45s", "weapon_sh_flashbang","climb_swep2", "weapon_phase"},
   command = "tsharpshooter",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Tombstone",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["TOMBSTONE"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(400)
end
})
TEAM_TSUPPORT = DarkRP.createJob("Tombstone 2-4 Support", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"Weapon_cuff_mp", "tfa_revival_m7s", "tfa_revival_m6s","revival_tombstone","tfa_revival_m41","weapon_sh_flashbang","revival_c4", "climb_swep2", "tfa_revival_m45s"},
   command = "tsupport",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Tombstone",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["TOMBSTONE"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(400)
end
})
TEAM_TSQUADLEAD = DarkRP.createJob("Tombstone 2-4 Squad Leader", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"tfa_revival_m45s", "tfa_revival_m7s", "tfa_revival_m6s", "tfa_revival_spartan_laser", "revival_tombstone","Weapon_cuff_mp", "stungun","voice_amplifier",  "weapon_sh_flashbang", "climb_swep2", "dk_flare_gun", "weapon_phase"},
   command = "tsquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Tombstone",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["TOMBSTONE"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(400)
end
})
TEAM_TMEDIC = DarkRP.createJob("Tombstone 2-4 Corpsman", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"Weapon_cuff_mp", "tfa_revival_m7s", "tfa_rebirth_ma5c", "tfa_revival_m45s", "tfa_revival_m6s","revival_tombstone","weapon_defibrillator","med_kit","weapon_bactainjector", "weapon_sh_flashbang", "climb_swep2", "weapon_bactanade"},
   command = "tcorpsman",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Tombstone",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["TOMBSTONE"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(400)
end
})
TEAM_MILITARYPOLICERECRUIT = DarkRP.createJob("Military Police Recruit", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps. You are the Recruit you are going to be trained and trusted with the power of the MP.]],
   weapons = {"tfa_revival_br55","tfa_revival_m7","revival_generalmp","tfa_revival_m6g","Stunstick","Weapon_cuff_mp", "climb_swep2"},
   command = "militarypolicerecruit",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Military Police",
   faction = 1,
   comms = {
      ["UNSCMP"] = true,
      ["JSEC"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=65
   ply:SetMaxHealth(200)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICE = DarkRP.createJob("Military Police", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps]],
   weapons = {"tfa_revival_m90c","tfa_revival_br55","tfa_revival_m7","revival_generalmp","stungun","tfa_revival_m90c","tfa_revival_m6g","Stunstick","Weapon_cuff_mp", "climb_swep2"},
   command = "militarypolice",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Military Police",
   faction = 1,
   comms = {
      ["UNSCMP"] = true,
      ["JSEC"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(225)
   ply.HaloArmor=65
   ply:SetMaxHealth(225)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICESGT = DarkRP.createJob("Military Police Sergeant", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps.]],
   weapons = {"tfa_revival_m90c","tfa_revival_br55","tfa_revival_m7","revival_generalmp","stungun","tfa_revival_m90c","tfa_revival_m6g","Stunstick","Weapon_cuff_mp", "climb_swep2", "dk_flare_gun"},
   command = "militarypolicesgt",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Military Police",
   faction = 1,
   comms = {
      ["UNSCMP"] = true,
      ["JSEC"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=65
   ply:SetMaxHealth(250)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICERIOT = DarkRP.createJob("Military Police Riot", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps. You are an MP Riot with access to Riot Shields.]],
   weapons = {"tfa_revival_m6g","tfa_revival_br55","stungun","weapon_cuff_mp","revival_riot","stunstick","riot_shield","heavy_shield","tfa_revival_m45s","tfa_meh_ma5d_masterkey","weapon_sh_flashbang", "climb_swep2"},
   command = "militarypoliceriot",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Military Police",
   faction = 1,
   comms = {
      ["UNSCMP"] = true,
      ["JSEC"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=60
   ply:SetMaxHealth(350)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICERIOTLEADER = DarkRP.createJob("Military Police Chief", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps. You are an MP Leader.]],
   weapons = {"tfa_revival_br55","tfa_revival_m6g","tfa_rebirth_ma5c","revival_generalmp","Weapon_policeshield","Stungun","Voice_amplifier","tfa_revival_m90c","Stunstick","weapon_sh_flashbang","Weapon_cuff_mp", "climb_swep2", "dk_flare_gun"},
   command = "militarypoliceleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Military Police",
   faction = 1,
   comms = {
      ["UNSCMP"] = true,
      ["JSEC"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=65
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_EndeavorCrew = DarkRP.createJob("Endeavor Crew", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/deviliousmarines/dragon/dragonm.mdl"},
   description = [[You are an Infantry officer for naval.]],
   weapons = {"tfa_rebirth_ma5c","revival_navy","tfa_revival_m6g", "climb_swep2"},
   command = "endeavorcrew",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["ENDVOPS"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply:SetArmor(150)
   ply:SetMaxHealth(250)
   ply.HaloArmor=45
end
})
TEAM_NAVALOP = DarkRP.createJob("Naval Operations", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/dress/dress.mdl","models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl"},
   description = [[Operations deals with navigation, Weaponry and act as command of the bridge. Each Naval Officer will have a certain job to do such as Navigation (NAV), Weaponry (WEP). As a Crewman (CN) and up you are able to gun a Frigate during events and mini events on station to help protect the UNSC Revival or to help the UNSC overall during offensive or defensive missions. You will be able to pilot a frigate at a rank of Petty Officer 3 (PO3). If you are a Petty Officer 1 (PO1) and above you will have access to the MAC cannon during off station and on station events.]],
   weapons = {"tfa_revival_br55","tfa_rebirth_ma5c", "tfa_revival_m6g", "weapon_cuff_standard", "revival_navy", "keys", "climb_swep2"},
   command = "navalop",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Naval Operations",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVOPS"] = true,
   },
	ammo = {["Grenade"] = 2},
PlayerSpawn = function (ply)
   ply:SetHealth(150)
   ply.HaloArmor=10
   ply:SetMaxHealth(150)
 end
})
TEAM_NAVALOPOFFICER = DarkRP.createJob("Naval Operations Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/lasky/lasky.mdl","models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl"},
   description = [[Operations deals with navigation, Weaponry and act as command of the bridge. Each Naval Officer will have a certain job to do such as Navigation (NAV), Weaponry (WEP). As a Crewman (CN) and up you are able to gun a Frigate during events and mini events on station to help protect the UNSC Revival or to help the UNSC overall during offensive or defensive missions. You will be able to pilot a frigate at a rank of Petty Officer 3 (PO3). If you are a Petty Officer 1 (PO1) and above you will have access to the MAC cannon during off station and on station events.]],
   weapons = {"tfa_revival_br55","tfa_rebirth_ma5c", "tfa_revival_m6g", "weapon_cuff_standard", "revival_navy", "keys", "climb_swep2", "dk_flare_gun"},
   command = "navalopofficer",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Naval Operations",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVOPS"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function (ply)
   ply:SetHealth(150)
   ply.HaloArmor=10
   ply:SetMaxHealth(150)
 end
})
TEAM_NAVALADMIRAL = DarkRP.createJob("Naval Admiral", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/cutter/cutter.mdl"},
    description = [[You are to command on the bridge and ships.You are to give orders to all units and watch over units.]],
   weapons = {"tfa_revival_spartan_laser","tfa_rebirth_m394","tfa_rebirth_ma5c","tfa_revival_m6g","weapon_cuff_mp", "voice_amplifier", "revival_navy", "climb_swep2", "dk_flare_gun"},
   command = "navaladmiral",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "UNSC Central Fleet",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVOPS"] = true,
      ["NAVSPECWAR"] = true,
      ["SF"] = true,
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function (ply)
   ply:SetHealth(200)
   ply:SetMaxHealth(200)
 end
})
TEAM_ONISPECIALIST = DarkRP.createJob("ONI S.T.R.I.K.E. Team", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI's most unknown shadow force, only known for security. "To Our Last Breath, We Give Our All, First In Never Out."]],
   weapons = {"hard_sound_rifle", "revival_strike", "tfa_revival_ht_magnum", "weapon_cuff_mp","tfa_revival_ht_shotgun","tfa_revival_m7s","weapon_sh_flashbang","revival_c4","cloaking-infinite", "stungun", "climb_swep2", "tfa_revival_ht_assault_rifle", "weapon_bactainjector"},
   command = "onispecialist",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI Security Force", 
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["STRIKE"] = true,
      ["JSEC"] = true,
      ["SWORD"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["AI"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply:SetArmor(300)
   ply.HaloArmor=75
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(300)
   ply:SetWalkSpeed(170)
   ply:GiveAmmo(5, "grenade")
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_ONISTWO = DarkRP.createJob("ONI Section 2", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/female_02.mdl", "models/suno/player/zeus/female/hawke.mdl", "models/suno/player/zeus/male/formal_bill.mdlq", "models/suno/player/zeus/male/formal_louis.mdl", "models/suno/player/zeus/male/formal_male_07.mdl", "models/suno/player/zeus/male/formal_male_09.mdl"},
   description = [[Section Two is the propaganda branch of ONI; they are also responsible for psychological operations. Overtly, however, Section Two only deals with external communication and public morale.]],
   weapons = {"weapon_bactainjector", "tfa_meh_ma5d_masterkey", "tfa_revival_m6s", "huntstk", "keys", "tfa_revival_srs994am", "tfa_revival_m45s", "stungun", "weapon_cuff_mp", "weapon_medkit", "voice_amplifier", "gmod_camera", "cloaking-infinite", "stunstick", "climb_swep2"},
   command = "onisectiontwo",
   max = 7,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["S2"] = true,
      ["AI"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
end
})
TEAM_ONISREC = DarkRP.createJob("ONI Recruit", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/female_02.mdl", "models/suno/player/zeus/female/hawke.mdl", "models/suno/player/zeus/male/formal_bill.mdlq", "models/suno/player/zeus/male/formal_louis.mdl", "models/suno/player/zeus/male/formal_male_07.mdl", "models/suno/player/zeus/male/formal_male_09.mdl"},
   description = [[Null]],
   weapons = {"weapon_bactainjector", "tfa_revival_m7s", "tfa_revival_m6s", "keys", "stungun", "weapon_cuff_mp", "weapon_medkit", "cloaking-infinite", "stunstick", "climb_swep2"},
   command = "onirecruit",
   max = 8,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI",
   faction = 1,
   comms = {
      ["ONI"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply:SetArmor(250)
   ply.HaloArmor=34
   ply:SetMaxHealth(250)
end
})
TEAM_ONISTHREE = DarkRP.createJob("ONI Section 3", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/female_02.mdl", "models/suno/player/zeus/female/hawke.mdl", "models/suno/player/zeus/male/formal_bill.mdlq", "models/suno/player/zeus/male/formal_louis.mdl", "models/suno/player/zeus/male/formal_male_07.mdl", "models/suno/player/zeus/male/formal_male_09.mdl"},
   description = [[Section Three is the top-secret projects division of ONI that oversees the various groups responsible for creating new innovations through the use of advanced technology.]],
   weapons = {"weapon_bactainjector", "tfa_revival_br55", "tfa_revival_srs992d", "tfa_revival_m45s",  "keys", "tfa_revival_m6s", "stungun", "weapon_cuff_mp", "weapon_medkit","revival_c4","cloaking-infinite", "stunstick", "weapon_defibrillator", "climb_swep2"},
   command = "onisectionthree",
   max = 7,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["S3"] = true,
      ["AI"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
end
})

TEAM_ONISONE = DarkRP.createJob("ONI Section 1", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/female_02.mdl", "models/suno/player/zeus/female/hawke.mdl", "models/suno/player/zeus/male/formal_bill.mdlq", "models/suno/player/zeus/male/formal_louis.mdl", "models/suno/player/zeus/male/formal_male_07.mdl", "models/suno/player/zeus/male/formal_male_09.mdl"},
   description = [[Section One is the proper intelligence-gathering branch of ONI. As such, the section's services are most often utilized by UNSC forces. Its responsibilities include information gathering and codebreaking though military espionage.]],
   weapons = {"weapon_bactainjector","tfa_ishi_br55", "tfa_revival_m45s", "huntstk",  "keys", "tfa_revival_srs992d", "tfa_revival_m6s", "stungun", "weapon_cuff_mp", "weapon_medkit","revival_c4","cloaking-infinite", "stunstick", "weapon_sh_flashbang", "tfa_revival_m7s", "climb_swep2"},
   command = "onisectionone",
   max = 7,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["S1"] = true,
      ["AI"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
end
})

TEAM_ONISZERO = DarkRP.createJob("ONI Section 0", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/female_02.mdl", "models/suno/player/zeus/female/hawke.mdl", "models/suno/player/zeus/male/formal_bill.mdlq", "models/suno/player/zeus/male/formal_louis.mdl", "models/suno/player/zeus/male/formal_male_07.mdl", "models/suno/player/zeus/male/formal_male_09.mdl"},
   description = [[ONI Section REDACTED is the Internal Affairs department, charged with rooting out internal issues. Section REDACTED is very discreet in its work; the true nature and activities of the division are highly classified and unknown outside the ONI command hierarchy.]],
   weapons = {"weapon_bactainjector", "tfa_revival_m247_gpmg","tfa_revival_m45s", "keys", "huntstk", "hard_sound_rifle", "tfa_revival_m6s", "stungun", "weapon_cuff_mp", "weapon_medkit","cloaking-infinite", "stunstick", "tfa_revival_spartan_laser", "climb_swep2", "dk_flare_gun"},
   command = "onisectionzero",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI",
   faction = 1,
   comms = {
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["ONI"] = true,
      ["S0"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["STRIKE"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
      ["O-S"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
end
})
TEAM_ONISPN = DarkRP.createJob("ONI Spartan", {
   color = Color(0, 0, 0, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},
   description = [[ONI SPARTANs are a handfull of the most loyal and skilled SPARTANs directly under the Office of Naval Intelligence. They are the Enforcers of the Office, as well as a Special Operations Unit, that can handle the most secretive and risky operations under the command of ONICOM Staff.]],
   weapons = {"drc_energysword", "hard_sound_rifle", "revival_oni","weapon_bactainjector","tfa_halo2_smg_dual_anniversary","weapon_bactanade","weapon_fistsofreprisal","tfa_revival_ht_shotgun","tfa_revival_ht_magnum","stungun","weapon_cuff_mp","halo_frag","cloaking-infinite","tfa_revival_spartan_laser", "climb_swep2", "dk_flare_gun"},
   command = "onispartan",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "ONI",
   faction = 1,
   comms = {
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["ONI"] = true,
      ["S0"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["STRIKE"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
      ["O-S"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(600)
   ply.HaloArmor=75
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONISPCOMMAND = DarkRP.createJob("Spartan Commander", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[The incumbent Spartan Commandsr oversees all operations of the 51st SPARTAN Program as well as the Endeavor.]],
   weapons = {"weapon_bactainjector","weapon_bactanade","tfa_revival_ht_assault_rifle","tfa_revival_ht_magnum","weapon_fistsofreprisal","tfa_revival_ht_shotgun","tfa_m247h_22","stungun","weapon_cuff_mp","cloaking-infinite","chaosnarwhal_m99", "climb_swep2", "dk_flare_gun", "weapon_phase", "weapon_grapplehook"},
   command = "spartancommander",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["NEXUS"] = true,
      ["EDEN"] = true,
      ["XERXES"] = true,
      ["ECLIPSE"] = true,
      ["INVICTA"] = true,
      ["WARDEN"] = true,
      ["SIGMA"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
      ["ARES"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(800)
   ply.HaloArmor=80
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONIDIR = DarkRP.createJob("ONI Director", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/valk/haloreach/unsc/spartan/spartan.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/female_02.mdl", "models/suno/player/zeus/female/hawke.mdl", "models/suno/player/zeus/male/formal_bill.mdlq", "models/suno/player/zeus/male/formal_louis.mdl", "models/suno/player/zeus/male/formal_male_07.mdl", "models/suno/player/zeus/male/formal_male_09.mdl"},
   description = [[The Office of Naval Intelligence (ONI) Director is the primary leading flag officer for an assigned ONI Sector who serves as the command's administrative and operational director. They oversee and plan all wide-scale operations within their assigned sector, with their secondary tasking being to upkeep external relations among other branches.]],
   weapons = {"weapon_bactainjector", "weapon_grapplehook", "weapon_bactanade","tfa_halo2_smg_dual_anniversary","hard_sound_rifle","tfa_revival_ht_magnum","weapon_fistsofreprisal","tfa_revival_ht_shotgun","tfa_revival_spartan_laser","stungun","weapon_cuff_mp","cloaking-infinite","tfa_revival_spartan_laser", "climb_swep2", "dk_flare_gun"},
   command = "onidir",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "ONI",
   faction = 1,
   comms = {
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["ONI"] = true,
      ["S0"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["STRIKE"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
      ["O-S"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(700)
   ply.HaloArmor=80
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONIASSISTDIR = DarkRP.createJob("ONI Assistant Director", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/chaosnarwhal/reach/airassult.mdl", "models/chaosnarwhal/reach/commando.mdl", "models/chaosnarwhal/reach/cqb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/grenadier.mdl", "models/chaosnarwhal/reach/gungnir.mdl", "models/chaosnarwhal/reach/hazop.mdl", "models/chaosnarwhal/reach/jfo.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/mkv.mdl", "models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/odst.mdl", "models/chaosnarwhal/reach/operator.mdl", "models/chaosnarwhal/reach/pilot.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/chaosnarwhal/reach/scout.mdl", "models/chaosnarwhal/reach/security.mdl"},
   description = [[The Office of Naval Intelligence (ONI) Assistant Director is the secondary leading flag officer for an assigned ONI Sector who serves as the command's assistant administrative and operational director. They oversee and plan all wide-scale operations within their assigned sector, with their secondary tasking being to handle large-scale internal issues.]],
   weapons = {"weapon_bactainjector","weapon_bactanade", "weapon_grapplehook", "tfa_revival_ht_assault_rifle", "tfa_rebirth_m7s","tfa_revival_ht_magnum","weapon_fistsofreprisal","tfa_revival_ht_shotgun","tfa_revival_spartan_laser","stungun","weapon_cuff_mp","cloaking-infinite","tfa_revival_spartan_laser", "hard_sound_rifle", "tfa_halo2_smg_dual_anniversary", "climb_swep2", "dk_flare_gun"},
   command = "oniassistdir",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "ONI",
   faction = 1,
   comms = {
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["ONI"] = true,
      ["S0"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["STRIKE"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
      ["O-S"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(700)
   ply.HaloArmor=80
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONIBATTLEBOTBASIC = DarkRP.createJob("ONI Battlebot", {  
   color = Color(160, 160, 160, 255),
   model = {"models/dizcordum/reaper.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_revival_m7","tfa_rebirth_ma5c","tfa_revival_m90c","med_kit","tfa_ishi_m7057", "tfa_rebirth_srs99s2am", "stungun", "weapon_cuff_mp", "weapon_sh_flashbang", "weapon_defibrillator", "climb_swep2"},
   command = "onibattlebotbasic",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI",
   faction = 1,
   comms = {
      ["ONI"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply:SetArmor(350)
   ply.HaloArmor=60
   ply:SetMaxHealth(450)
end
})
TEAM_SPARTANTRAINEE = DarkRP.createJob("Spartan Trainee", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/suno/player/zeus/female/fang.mdl","models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"cloaking-infinite","tfa_revival_ma5c","tfa_revival_m6g", "climb_swep2"},
   command = "spartantrainee",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(400)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_NOMADSQUADLEADER = DarkRP.createJob("Sigma One", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[SIGMA is a binary team of Spartans currently assigned to the 51st.]],
   weapons = {"weapon_fistsofreprisal", "climb_swep2", "tfa_revival_ht_assault_rifle", "chaosnarwhal_m99", "tfa_revival_ht_magnum", "tfa_halo2_smg_dual_anniversary", "weapon_phase", "weapon_bactanade", "weapon_bactainjector", "stungun", "weapon_cuffs_mp", "tfa_revival_m41"},
   command = "sigmasquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Nomad",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["SIGMA"] = true,
	  ["NEXUS"] = true,
      ["EDEN"] = true,
      ["XERXES"] = true,
      ["ECLIPSE"] = true,
      ["INVICTA"] = true,
      ["WARDEN"] = true,
      ["ARES"] = true,
	  ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(230)
   ply:SetJumpPower(290)
end
})
TEAM_NOMADSHARPSHOOTER = DarkRP.createJob("Sigma Two", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[SIGMA is a binary team of Spartans currently assigned to the 51st.]],
   weapons = {"tfa_revival_ht_magnum","weapon_fistsofreprisal", "climb_swep2", "tfa_revival_ht_assault_rifle", "tfa_revival_ht_shotgun", "tfa_halo2_smg_dual_anniversary", "chaosnarwhal_m99", "weapon_bactanade", "weapon_bactainjector", "weapon_phase", "weapon_cuffs_mp", "stungun", "weapon_vj_flaregun"},
   command = "nomadsharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Nomad",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["NEXUS"] = true,
      ["EDEN"] = true,
      ["XERXES"] = true,
      ["ECLIPSE"] = true,
      ["INVICTA"] = true,
      ["WARDEN"] = true,
      ["SIGMA"] = true,
      ["ARES"] = true,
	  ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(230)
   ply:SetJumpPower(290)
end
})
TEAM_EDENSQUADLEADER = DarkRP.createJob("Eden Fireteam Leader", {
   color = Color(199, 175, 31, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Fireteam Eden specializes in Shock Combat Tactics and Field Medical Support.]],
   weapons = {"tfa_revival_ht_assault_rifle","revival_eden","tfa_revival_ht_magnum","weapon_fistsofreprisal","tfa_revival_ht_shotgun","tfa_halo2_smg_dual_anniversary","stungun","weapon_cuff_mp","cloaking-infinite","weapon_sh_flashbang","tfa_revival_spartan_laser", "weapon_bactanade", "climb_swep2", "dk_flare_gun", "weapon_phase"},
   command = "edensquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["EDEN"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_EDENRIFLEMAN = DarkRP.createJob("Eden Corpsman", {
   color = Color(199, 175, 31, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Fireteam Eden specializes in Shock Combat Tactics and Field Medical Support.]],
   weapons = {"tfa_revival_ht_magnum","tfa_revival_m7", "revival_eden","tfa_revival_ht_assault_rifle","weapon_fistsofreprisal","weapon_bactainjector","weapon_bactanade","med_kit","weapon_defibrillator","cloaking-infinite","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge", "climb_swep2", "tfa_revival_m45s"},
   command = "edenrifleman",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["EDEN"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_EDENSUPPORT = DarkRP.createJob("Eden Support", {
   color = Color(199, 175, 31, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Fireteam Eden specializes in Shock Combat Tactics and Field Medical Support.]],
   weapons = {"tfa_revival_m247_gpmg","tfa_revival_ht_assault_rifle","tfa_rebirth_m7", "tfa_revival_ht_magnum","revival_eden","weapon_fistsofreprisal","cloaking-infinite","revival_c4","stungun","weapon_cuff_mp","weapon_sh_flashbang", "climb_swep2"},
   command = "edensupport",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["EDEN"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_EDENSHARPSHOOTER = DarkRP.createJob("Eden Sharpshooter", {
   color = Color(199, 175, 31, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Fireteam Eden specializes in Shock Combat Tactics and Field Medical Support.]],
   weapons = {"weapon_grapplehook", "chaosnarwhal_m99","tfa_revival_m7s", "tfa_revival_ht_assault_rifle", "revival_eden","weapon_fistsofreprisal","tfa_revival_ht_magnum","cloaking-infinite","stungun","weapon_cuff_mp","weapon_sh_flashbang", "climb_swep2"},
   command = "edensharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["EDEN"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_INVICTASQUADLEADER = DarkRP.createJob("Invicta Fireteam Leader", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Invicta is a fireteam dedicated to shock and awe tactics.]],
   weapons = {"tfa_revival_ht_assault_rifle","tfa_revival_ht_magnum","weapon_fistsofreprisal","tfa_halo2_smg_dual_anniversary","tfa_revival_m247_gpmg","stungun","weapon_cuff_mp","cloaking-infinite","weapon_sh_flashbang","tfa_revival_spartan_laser", "weapon_bactanade", "climb_swep2", "dk_flare_gun", "weapon_phase"},
   command = "invictasquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["INVICTA"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_INVICTASHARPSHOOTER = DarkRP.createJob("Invicta Sharpshooter", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Invicta is a fireteam dedicated to shock and awe tactics.]],
   weapons = {"tfa_revival_m7s", "tfa_revival_ht_magnum", "chaosnarwhal_m99", "weapon_fistsofreprisal", "tfa_revival_ht_assault_rifle", "stungun", "weapon_cuff_mp", "cloaking-infinite", "weapon_sh_flashbang", "weapon_bactanade", "climb_swep2", "weapon_phase"},
   command = "invictasharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["INVICTA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_INVICTACORPSMAN = DarkRP.createJob("Invicta Corpsman", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Invicta is a fireteam dedicated to shock and awe tactics.]],
   weapons = {"tfa_revival_ht_magnum","tfa_revival_m7", "tfa_revival_ht_assault_rifle", "tfa_hr_swep_dmr", "weapon_fistsofreprisal", "stungun", "weapon_cuff_mp", "cloaking-infinite", "weapon_sh_flashbang", "weapon_bactanade", "weapon_bactainjector", "darkrp_defibrilator", "climb_swep2"},
   command = "invictacorpsman",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["INVICTA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_INVICTASUPPORT = DarkRP.createJob("Invicta Support", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Invicta is a fireteam dedicated to shock and awe tactics.]],
   weapons = {"tfa_revival_ht_assault_rifle", "tfa_revival_ht_magnum", "tfa_revival_m41","tfa_revival_ht_shotgun", "weapon_fistsofreprisal", "stungun", "weapon_cuff_mp", "cloaking-infinite", "weapon_sh_flashbang", "weapon_bactanade", "weapon_bactainjector", "darkrp_defibrilator", "climb_swep2"},
   command = "invictasupport",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["INVICTA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_AI = DarkRP.createJob("AI", {
   color = Color(51, 51, 255, 255),
   model = {"models/player/scifi_bill.mdl", "models/player/scifi_female_01.mdl", "models/player/scifi_wraith.mdl","models/player/scifi_zoey.mdl"},
   description = [[Artificial Intelligence created for multipurpose, help, and provide support to their hosts. Their ways of creation are unknown to the public eye.]],
   weapons = {"med_kit"},
   command = "ai",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "AI",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["ATC"] = true,
      ["AI"] = true,
   },
PlayerSpawn = function(ply)
	ply:SetNoTarget(true)
end
})
TEAM_SOSINFANTRY = DarkRP.createJob("SoS Elite", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the UNSC to stop the Great Journey]],
   weapons = {"drc_plasma_repeater", "weapon_plasmanade", "drc_plasma_rifle", "climb_swep2"},
   command = "sosinfantry",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(350)
   ply.HaloArmor=60
   ply:SetMaxHealth(500)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
end
})
TEAM_SPECOPS = DarkRP.createJob("SoS Spec-Ops", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"drc_t57b", "drc_plasma_repeater", "weapon_plasmanade", "cloaking-infinite",  "drc_plasma_rifle", "drc_energydaggers", "climb_swep2"},
   command = "sosspecops",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["SPEC-OPS"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply:SetArmor(400)
   ply.HaloArmor=60
   ply:SetMaxHealth(450)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
end
})
TEAM_RANGER = DarkRP.createJob("SoS Ranger", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"drc_t50srs", "drc_t51", "weapon_plasmanade", "drc_plasma_repeater", "climb_swep2", "weapon_vj_flaregun"},
   command = "sosranger",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["RANGER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply:SetArmor(400)
   ply.HaloArmor=60
   ply:SetMaxHealth(450)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(220)
end
})
TEAM_SOSOFFICER = DarkRP.createJob("SoS Officer", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"drc_plasma_repeater", "drc_t50", "weapon_plasmanade", "drc_energysword", "climb_swep2", "dk_flare_gun"},
   command = "sosofficer",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["OFFICER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply:SetArmor(450)
   ply.HaloArmor=60
   ply:SetMaxHealth(450)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(220)
end
})
TEAM_SOSLOWCOMMAND = DarkRP.createJob("SoS Low Command", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"drc_plasma_rifle", "drc_t50", "drc_t51", "weapon_plasmanade", "drc_energysword", "cloaking-infinite", "climb_swep2", "weapon_bactanade", "dk_flare_gun"},
   command = "soslowcommand",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["OFFICER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(500)
   ply.HaloArmor=60
   ply:SetMaxHealth(500)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(220)
end
})
TEAM_HONORGUARD = DarkRP.createJob("SoS Honor Guard", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"drc_plasma_rifle", "drc_t51", "drc_plasma_repeater", "drc_energystave", "weapon_bactanade", "weapon_plasmanade", "weapon_cuff_mp", "climb_swep2"},
   command = "soshonorguard",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["HG"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply:SetArmor(600)
   ply.HaloArmor=65
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(220)
end
})
TEAM_SOSHELIOS = DarkRP.createJob("SoS Helios", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the UNSC to stop the Great Journey]],
   weapons = {"drc_plasma_rifle", "drc_plasma_repeater", "drc_t33b", "cloaking-infinite", "weapon_bactanade", "keys", "drc_energystave", "weapon_cuff_mp", "weapon_plasmanade", "climb_swep2", "drc_energydaggers", "dk_flare_gun"},
   command = "soshelios",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["HG"] = true,
      ["OFFICER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply:SetArmor(600)
   ply.HaloArmor=65
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(322)
   ply:SetWalkSpeed(222)
end
})
TEAM_HIGHCOMMAND = DarkRP.createJob("SoS High Command", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"drc_plasma_rifle", "drc_t33a", "drc_t51", "weapon_plasmanade", "cloaking-infinite", "weapon_bactanade", "drc_t50srs", "drc_energysword", "weapon_cuff_mp", "climb_swep2", "dk_flare_gun"},
   command = "soshighcommand",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["OFFICER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply:SetArmor(600)
   ply.HaloArmor=65
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(220)
end
})
TEAM_SENIORCOMMAND = DarkRP.createJob("SoS Senior Command", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the UNSC to stop the Great Journey]],
   weapons = {"drc_plasma_rifle", "drc_plasma_repeater", "drc_t33a", "weapon_plasmanade", "weapon_bactanade", "drc_t50srs", "drc_t51", "cloaking-infinite", "drc_energysword", "weapon_cuff_mp", "climb_swep2", "dk_flare_gun"},
   command = "sosseniorcommand",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["HG"] = true,
      ["RANGER"] = true,
      ["SPEC-OPS"] = true,
      ["SHADOW"] = true,
      ["ASCETIC"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(500)
   ply.HaloArmor=65
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(220)
end
})
TEAM_SOSGRUNT = DarkRP.createJob("SoS Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/valk/haloreach/covenant/characters/grunt/grunt_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the UNSC to stop the Great Journey]],
   weapons = {"drc_plasma_pistol", "drc_plasma_rifle", "weapon_plasmanade", "keys", "climb_swep2"},
   command = "sosgrunt",
   max = 0,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["GRUNT"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=10
   ply:SetMaxHealth(275)
end
})
TEAM_SOSDEACONGRUNT = DarkRP.createJob("SoS Deacon Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/valk/haloreach/covenant/characters/grunt/grunt_player_honor.mdl"},
   description = [[A respectful unggoy member, blessed by the Arbiter of the Swords of Sanghelios]],
   weapons = {"weapon_medkit", "drc_plasma_rifle", "weapon_plasmanade", "drc_plasma_pistol", "climb_swep2"},
   command = "sosdeacongrunt",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["GRUNT"] = true,
      ["DEACON"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply:SetArmor(100)
   ply.HaloArmor=35
   ply:SetMaxHealth(275)
end
})
TEAM_SOSJACKAL = DarkRP.createJob("SoS Jackal", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/jackal.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Keys", "weapon_plasmanade", "drc_t51", "drc_plasma_pistol", "climb_swep2"},
   command = "sosjackal",
   max = 12,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["JACKAL"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=30
   ply:SetMaxHealth(150)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
end
})
TEAM_SOSJACKALSNIPER = DarkRP.createJob("SoS Jackal Sniper", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/jackal.mdl"},
   description = [[You are a Legendary Jackal Sniper, apart of the SoS]],
   weapons = {"weapon_grapplehook", "drc_t50srs", "drc_t51", "drc_plasma_pistol", "climb_swep2", "keys", "weapon_vj_flaregun"},
   command = "sosjackalsniper",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["JACKAL"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=40
   ply:SetMaxHealth(150)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
end
})
TEAM_ARBITER = DarkRP.createJob("SoS Arbiter", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_arb_h2a_player.mdl"},
   description = [[The Arbiter led the SoS to  y, exposing the exploits of the prophet's lies]],
   weapons = {"drc_energysword", "climb_swep2", "drc_plasma_rifle", "drc_plasma_rifle", "drc_plasma_rifleifle", "drc_t33a", "weapon_plasmanade", "cloaking-infinite", "weapon_cuff_mp","weapon_bactanade", "dk_flare_gun"},
   command = "sosarbiter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["HG"] = true,
      ["RANGER"] = true,
      ["SPEC-OPS"] = true,
      ["SHADOW"] = true,
      ["ASCETIC"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(600)
   ply.HaloArmor=70
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(220)
end
})
TEAM_MGALEKGOLO = DarkRP.createJob("SoS Mgalekgolo", {
   color = Color(127, 0, 255, 255),
   model = {"models/valk/haloreach/covenant/characters/hunter/hunter_player.mdl", "models/jq/halowars/hunter/captain/hunter_captain_original_player.mdl"},
   description = [[Mgalekgolo are groups of worms called Lekgolo, Spanning from 13 feet tall to massive heights depending on the planet's gravity. Rarely any sided with the SoS but these heavy troops will decimate armies.]],
   weapons = { "keys", "drc_assaultcannon", "drc_hunterspv3", "drc_lekgolo", "drc_plasmacannon", "climb_swep2"},
   command = "Mgalekgolo",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["HUNTER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(1200)
   ply.HaloArmor=85
   ply:SetMaxHealth(1200)
   ply:SetRunSpeed(200)
   ply:SetWalkSpeed(120)
end
})
TEAM_STAFF1 = DarkRP.createJob("Mod On Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/mod/mod.mdl",
	description = [[For use by Trial Moderators and Moderators only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "mod",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
   comms = {
   },
})
TEAM_STAFF2 = DarkRP.createJob("Admin on Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/red/red.mdl",
	description = [[For use by Admins and Senior Admins only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "aod",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
   comms = {
   },
})
TEAM_STAFF3 = DarkRP.createJob("Upper Staff on Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/gold/gold.mdl",
	description = [[For use by Developers and anyone above Head Admin only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "uod",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
   comms = {
	  ["UNSC"] = true,
     ["SCD"] = true,
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["NEXUS"] = true,
      ["EDEN"] = true,
      ["XERXES"] = true,
      ["ECLIPSE"] = true,
      ["INVICTA"] = true,
      ["WARDEN"] = true,
      ["SIGMA"] = true,
      ["ARES"] = true,
	  ["BAKER"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
	  ["RHINO"] = true,
	  ["WHISKEY"] = true,
     ["ATC"] = true,
     ["NAVAL"] = true,
	  ["ONI"] = true,
      ["STRIKE"] = true,
      ["JSEC"] = true,
	  ["UNSCMP"] = true,
	  ["ODST"] = true,
	  ["Tombstone"] = true,
	  ["DEVILDOG"] = true,
      ["WARDOG"] = true,
	  
   },
})
TEAM_STAFF4 = DarkRP.createJob("Gamemaker on Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/red/red.mdl",
	description = [[For use by gamemakers only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "god",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
   comms = {
   },
})

TEAM_XERXESFL = DarkRP.createJob("Xerxes Fireteam Leader", {
   color = Color(105, 105, 105, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},
   description = [[Fireteam Xerxes specializes in Covert Operations and Interrogation.]],
   weapons = {"climb_swep2", "chaosnarwhal_m99", "revival_xerxes", "tfa_revival_br85_22", "tfa_revival_ht_magnum", "tfa_revival_ht_shotgun", "weapon_fistsofreprisal", "stungun", "weapon_cuff_mp", "cloaking-infinite", "tfa_revival_saw", "weapon_sh_flashbang", "weapon_bactanade", "dk_flare_gun", "weapon_phase"},
   command = "xerxessquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Xerxes",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["XERXES"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor = 80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_XERXESRIFLEMAN = DarkRP.createJob("Xerxes Corpsman", {
   color = Color(105, 105, 105, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},  
   description = [[Fireteam Xerxes specializes in Covert Operations and Interrogation.]],
   weapons = {"tfa_revival_ht_magnum","tfa_revival_ht_assault_rifle", "tfa_hr_swep_dmr", "revival_xerxes", "tfa_revival_ht_shotgun", "weapon_fistsofreprisal", "weapon_bactainjector", "weapon_bactanade", "med_kit", "weapon_defibrillator", "cloaking-infinite","stungun","weapon_cuff_mp","weapon_sh_flashbang", "climb_swep2"},
   command = "xerxesrifleman",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Xerxes",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["XERXES"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_XERXESSUPPORT = DarkRP.createJob("Xerxes Support", {
   color = Color(105, 105, 105, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Fireteam Xerxes specializes in Covert Operations and Interrogation.]],
   weapons = {"tfa_revival_ht_assault_rifle", "tfa_revival_m41", "tfa_m247h", "climb_swep2", "tfa_revival_ht_magnum", "revival_xerxes", "weapon_fistsofreprisal", "cloaking-infinite", "revival_c4", "stungun", "weapon_cuff_mp", "weapon_sh_flashbang"},
   command = "xerxessupport",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Xerxes",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["XERXES"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end                                                       
})
TEAM_XERXESSHARPSHOOTER = DarkRP.createJob("Xerxes Sharpshooter", {
   color = Color(105, 105, 105, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Fireteam Xerxes specializes in Covert Operations and Interrogation.]],
   weapons = {"weapon_grapplehook","chaosnarwhal_m99", "tfa_revival_ht_magnum", "revival_xerxes", "weapon_fistsofreprisal", "tfa_revival_ht_assault_rifle", "tfa_revival_ht_shotgun", "cloaking-infinite", "stungun", "weapon_cuff_mp", "weapon_sh_flashbang", "climb_swep2", "weapon_vj_flaregun"},
   command = "xerxessharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Xerxes",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["XERXES"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_FREELANCERCHAMPION = DarkRP.createJob("Freelancer Grand Champion", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group01/spartan_male02_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male03_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl"},
   description = [[The grand champion of the freelancer tournaments.]],
   weapons = {"climb_swep2", "tfa_revival_m6g", "tfa_revival_m45s", "tfa_revival_ma5c", "tfa_m247h", "cloaking-infinite", "weapon_bactainjector", "med_kit", "tfa_revival_srs994am", "force_shield_weapon"},
   command = "flc1",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Freelancer",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(450)
   ply:SetMaxHealth(500)
   ply.HaloArmor=75
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_FREELANCERCHAMPION2 = DarkRP.createJob("Freelancer Champion", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group01/spartan_male02_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male03_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl"},
   description = [[The second place champion of the freelancer tournaments.]],
   weapons = {"climb_swep2", "tfa_m247h", "tfa_revival_m45s", "tfa_meh_ma5d_masterkey", "tfa_revival_m6g", "cloaking-infinite", "weapon_bactainjector", "med_kit", "tfa_revival_br55", "force_shield_weapon"},
   command = "flc2",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Freelancer",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(450)
   ply:SetMaxHealth(400)
   ply.HaloArmor=75
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_SOSHEAVYGRUNT = DarkRP.createJob("SoS Heavy grunt", {
    color = Color(127, 0, 255, 255),
    model = {"models/valk/haloreach/covenant/characters/grunt/grunt_player.mdl"},
    description = [[SOS Heavy Grunt (Donation)]],
    weapons = {"drc_t33b", "climb_swep2", "drc_plasma_pistol", "drc_plasma_rifle", "weapon_plasmanade"},
    command = "sosheavygrunt",
    max = 20,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Swords of Sanghelios",
    comms = {
      ["GRUNT"] = true,
   },
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply.HaloArmor=60
		ply:SetMaxHealth(300)
    end
})
TEAM_SOSJACKELMEDIC = DarkRP.createJob("SoS Jackal medic", {
    color = Color(127, 0, 255, 255),
    model = {"models/player/jackal.mdl"},
    description = [[Some Kig-Yar specialize in medicine and even perform medical duties in the field. Just be careful if they inject you with something..]],
    weapons = {"med_kit","weapon_defibrillator", "climb_swep2", "weapon_plasmanade", "drc_plasma_rifle", "drc_plasma_rifleifle", "weapon_bactainjector"},
    command = "sosjackelmedic",
    max = 20,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Swords of Sanghelios",
    faction = 1,
    comms = {
      ["JACKAL"] = true,
   },
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(150)
        ply.HaloArmor=30
		ply:SetRunSpeed(350)
      ply:SetMaxHealth(150)
		ply:SetWalkSpeed(200)
    end
})
TEAM_ALPHASIX = DarkRP.createJob("Alpha Six", {
   color = Color(86, 86, 86, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Alpha Six Squad. (Donation)]],
   weapons = {"tfa_revival_m90c", "tfa_revival_m7s", "tfa_revival_m247_gpmg", "revival_alpha", "tfa_revival_m6s", "revival_c4", "climb_swep2"},
   command = "alphasix",
    max = 3,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    hud = 1,
    category = "Alpha Six",
    faction = 1,
    comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply:SetMaxHealth(300)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
end
})
TEAM_ALPHASIXSQL = DarkRP.createJob("Alpha Six Squad Lead", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"tfa_revival_m7s", "tfa_meh_ma5d_masterkey", "tfa_revival_m6s", "tfa_revival_srs992d", "revival_alpha", "keys", "climb_swep2", "weapon_vj_flaregun", "dk_flare_gun"},
    command = "alphasixsquadlead",
    max = 1,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    hud = 1,
    category = "Alpha Six",
    faction = 1,
    comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply:SetMaxHealth(300)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXRIFLE = DarkRP.createJob("Alpha Six Rifleman", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"tfa_revival_srs992d", "tfa_revival_m6s", "revival_alpha", "keys", "tfa_revival_ma5c", "tfa_revival_m7s", "climb_swep2"},
    command = "alphasixsquadrifle",
    max = 2,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    hud = 1,
    category = "Alpha Six",
    faction = 1,
    comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply:SetMaxHealth(300)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXCORPS = DarkRP.createJob("Alpha Six Corpsman", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"tfa_revival_m7s", "tfa_revival_m6s", "tfa_revival_br55", "climb_swep2", "revival_alpha", "tfa_revival_srs992d", "weapon_medkit", "weapon_bactainjector", "weapon_defibrillator", "keys"},
    command = "alphasixsquadcorps",
    max = 1,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    hud = 1,
    category = "Alpha Six",
    faction = 1,
    comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply:SetMaxHealth(300)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXMARKSMAN = DarkRP.createJob("Alpha Six Marksman", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"tfa_revival_m7s", "tfa_rebirth_srs99s2am", "tfa_rebirth_m394", "tfa_revival_m6s", "revival_alpha", "keys", "climb_swep2", "weapon_vj_flaregun"},
    command = "alphasixsquadmarksman",
    max = 2,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    hud = 1,
    category = "Alpha Six",
    faction = 1,
    comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply:SetMaxHealth(300)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXDEMOLITIONIST = DarkRP.createJob("Alpha Six Demolitionist", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"climb_swep2", "tfa_revival_m41", "tfa_revival_m7s", "tfa_revival_ma5c", "tfa_revival_m6s", "revival_alpha", "keys", "revival_c4"},
    command = "alphasixsquad",
    max = 1,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    hud = 1,
    category = "Alpha Six",
    faction = 1,
    comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply:SetMaxHealth(300)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_SWORDFISHPILOT = DarkRP.createJob("Sword-fish Pilot", {
   color = Color(102, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[We are the Guardians Of The Sky. We are the few and the proud to sweep the enemies from darkness and bring the light that our bullets shine upon the earth and so we make sure our airspace is forever clear]],
   weapons = {"tfa_revival_m90c","tfa_rebirth_ma5c","revival_swordfish","keys", "tfa_revival_m6g", "climb_swep2"},
   command = "swordfishpilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Swordfish",
   faction = 1,
   comms = {
      ["SF"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(150)
	ply.HaloArmor=40
	ply:SetMaxHealth(150)
end
})
TEAM_SWORDFISHSL = DarkRP.createJob("Sword-fish Squad Leader", {
   color = Color(102, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[We are the Guardians Of The Sky. We are the few and the proud to sweep the enemies from darkness and bring the light that our bullets shine upon the earth and so we make sure our airspace is forever clear]],
   weapons = {"tfa_revival_m90c","tfa_rebirth_ma5c","revival_swordfish","keys", "tfa_revival_m6g", "climb_swep2", "dk_flare_gun"},
   command = "swordfishlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Swordfish",
   faction = 1,
   comms = {
      ["SF"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(150)
	ply.HaloArmor=40
	ply:SetMaxHealth(150)
end
})
TEAM_SWORDFISHCOPILOT = DarkRP.createJob("Sword-fish Co-Pilot", {
   color = Color(102, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Vulcan were pilots that undertook heavy tasks in the air]],
   weapons = {"tfa_revival_m90c","tfa_rebirth_ma5c","revival_swordfish","keys", "tfa_revival_m6g", "climb_swep2"},
   command = "swordfishcopilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Swordfish",
   faction = 1,
   comms = {
      ["SF"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(150)
	ply.HaloArmor=40
	ply:SetMaxHealth(150)
end
})
TEAM_REAPERPILOT = DarkRP.createJob("Reaper Pilot", {
   color = Color(51, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Nemesis are pilots that undertook heavy tasks in the air]],
   weapons = {"tfa_rebirth_ma5c","tfa_revival_m90c","tfa_revival_m7", "revival_reaper", "keys", "tfa_revival_m6s", "climb_swep2"},
   command = "reaperpilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Reaper",
   faction = 1,
   comms = {
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(200)
	ply.HaloArmor=40
	ply:SetMaxHealth(200)
end
})
TEAM_REAPERCOPILOT = DarkRP.createJob("Reaper Co-Pilot", {
   color = Color(51, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Nemesis were pilots that undertook heavy tasks in the air]],
   weapons = {"tfa_rebirth_ma5c","tfa_revival_m90c","tfa_revival_m7", "revival_reaper", "keys", "tfa_revival_m6s", "climb_swep2"},
   command = "reapercopilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Reaper",
   faction = 1,
   comms = {
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(200)
	ply.HaloArmor=40
	ply:SetMaxHealth(200)
end
})
TEAM_REAPERSL = DarkRP.createJob("Reaper Squad Leader", {
   color = Color(51, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Nemesis were pilots that undertook heavy tasks in the air]],
   weapons = {"tfa_rebirth_ma5c","tfa_revival_m90c","tfa_revival_m7", "revival_reaper", "keys", "tfa_revival_m6s", "climb_swep2", "dk_flare_gun"},
   command = "reapersquadlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Reaper",
   faction = 1,
   comms = {
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(200)
	ply.HaloArmor=40
	ply:SetMaxHealth(200)
end
})
TEAM_REAPERHPILOT = DarkRP.createJob("Reaper Heavy Pilot", {
   color = Color(51, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Nemesis were pilots that undertook heavy tasks in the air]],
   weapons = {"tfa_rebirth_ma5c","tfa_revival_m90c","tfa_revival_m7", "revival_reaper", "keys", "tfa_revival_m6s", "climb_swep2"},
   command = "reaperheavy",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Reaper",
   faction = 1,
   comms = {
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(225)
	ply.HaloArmor=40
	ply:SetMaxHealth(225)
end
})
TEAM_AIRBORNEHOP = DarkRP.createJob("Head Of Pilots", {
   color = Color(10, 10, 10, 250),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/keyes/keyes.mdl"},
   description = [[The Head Of Pilots was a High ranking officer commissioned to command and organize Pilot squads.]],
   weapons = {"tfa_revival_spartan_laser","tfa_rebirth_m394","tfa_rebirth_ma5c", "keys", "tfa_revival_m6g", "climb_swep2", "dk_flare_gun"},
   command = "headofpilots",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "MAJCOM",
   faction = 1,
   comms = {
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["SF"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(125)
	ply.HaloArmor=35
	ply:SetMaxHealth(125)
end
})
TEAM_UNSCAFG = DarkRP.createJob("General", {
   color = Color(10, 10, 10, 250),
   model = {"models/gonzo/unscofficers/lasky/lasky.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl"},
   description = [[You are the General in the UNSCAF]],
   weapons = {"tfa_revival_m90c","weapon_cuff_mp","voice_amplifier","tfa_revival_m6g", "climb_swep2", "dk_flare_gun"},
   command = "general",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "MAJCOM",
   faction = 1,
   comms = {
      ["REAPER"] = true,
      ["SF"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply.HaloArmor=10
end
})
TEAM_ODSTCOMMANDER = DarkRP.createJob("ODST Commander", {
   color = Color(0, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[ODST Commander.]],
   weapons = {"huntnte", "climb_swep2", "tfa_revival_m7s", "tfa_revival_m6s", "tfa_revival_m90c", "tfa_revival_srs992d", "voice_amplifier", "weapon_sh_flashbang", "keys", "dk_flare_gun"},
   command = "odstcommanderone",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ODST Command",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["TOMBSTONE"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
      ["LION"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
   ply:SetArmor(200)
end
})
TEAM_ODSTEXECUTIVE = DarkRP.createJob("ODST Platoon Sergeant", {
   color = Color(0, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[ODST Platoon Corpsman.]],
   weapons = {"huntnte", "climb_swep2", "tfa_revival_m7s", "tfa_revival_m6s", "tfa_revival_m90c", "tfa_revival_srs992d", "voice_amplifier", "weapon_sh_flashbang", "keys", "dk_flare_gun"},
   command = "odstsergeant",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ODST Command",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["TOMBSTONE"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
      ["LION"] = true,

   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
   ply:SetArmor(200)
end
})
TEAM_ODSTPLATOONSERGEANT = DarkRP.createJob("ODST Platoon Executive", {
   color = Color(0, 0, 0, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[ODST Platoon Executive.]],
   weapons = {"huntnte", "climb_swep2", "tfa_revival_m7s", "tfa_revival_m6s", "tfa_revival_m90c", "tfa_revival_srs992d", "voice_amplifier", "weapon_sh_flashbang", "keys", "dk_flare_gun"},
   command = "odstexecutive",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ODST Command",
   faction = 1,
   comms = {
      ["ODST"] = true,
      ["A6"] = true,
      ["TOMBSTONE"] = true,
      ["BULLFROG"] = true,
      ["NAVCOM"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
      ["LION"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
   ply:SetArmor(200)
end
})
TEAM_VALKRYIEAT = DarkRP.createJob("Valkyrie Assault", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Valkyrie]],
   weapons = {"tfa_revival_m45s","climb_swep2", "revival_valkyrie", "keys", "tfa_rebirth_m394", "tfa_rebirth_ma5c", "tfa_revival_m6g"},
   command = "valkyrieassaulter",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Valkyrie",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["VALKYRIE"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=45
   ply:SetMaxHealth(300)
   ply:SetArmor(100)
end
})
TEAM_VALKRYIEHT = DarkRP.createJob("Valkyrie Heavy Trooper", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Valkyrie]],
   weapons = {"climb_swep2", "revival_valkyrie", "tfa_revival_m41", "tfa_revival_m6g","tfa_revival_m45s", "tfa_revival_saw", "keys"},
   command = "valkyrieheavyrooper",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Valkyrie",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["VALKYRIE"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=40
   ply:SetMaxHealth(350)
   ply:SetArmor(250)
end
})
TEAM_VALKRYIESS = DarkRP.createJob("Valkyrie Sharpshooter", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Valkyrie]],
   weapons = {"tfa_revival_m45s","tfa_rebirth_srs99s2am", "climb_swep2", "revival_valkyrie", "tfa_revival_m6g","tfa_revival_m7"},
   command = "valkyriesharpshooter",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Valkyrie",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["VALKYRIE"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(300)
	ply.HaloArmor=40
	ply:SetMaxHealth(300)
   ply:SetArmor(100)
end
})
TEAM_VALKRYIEMED = DarkRP.createJob("Valkyrie Medic", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Valkyrie Medic]],
   weapons = {"weapon_defibrillator","weapon_bactainjector", "climb_swep2", "revival_valkyrie", "med_kit", "keys", "tfa_rebirth_ma5c", "tfa_revival_m6g", "tfa_revival_m7","tfa_revival_m45s"},
   command = "valkyriemedic",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Valkyrie",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["VALKYRIE"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=45
   ply:SetMaxHealth(300)
   ply:SetArmor(150)
end
})
TEAM_VALKRYIECLIPP = DarkRP.createJob("Valkyrie Clipper", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Valkyrie Clipper]],
   weapons = {"tfa_revival_m247_gpmg","tfa_revival_m7", "climb_swep2", "revival_valkyrie", "tfa_revival_m45s", "tfa_revival_m6s", "keys", "weapon_defibrillator", "weapon_bactainjector", "med_kit"},
   command = "valkyrieclipper",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Valkyrie",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["VALKYRIE"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=45
   ply:SetMaxHealth(400)
   ply:SetArmor(300)
end
})
TEAM_VALKRYIESTALK = DarkRP.createJob("Valkyrie Stalkers", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Valkyrie Stalkers]],
   weapons = {"tfa_revival_m45s","tfa_rebirth_m394","cloaking-infinite", "climb_swep2", "revival_valkyrie", "tfa_revival_m6s", "tfa_rebirth_srs99s2am", "keys"},
   command = "valkyriestalkers",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Valkyrie",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["VALKYRIE"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=40
   ply:SetMaxHealth(400)
   ply:SetArmor(150)
end
})
TEAM_MOSLEADER = DarkRP.createJob("MOS Squad Leader", {
   color = Color(200, 50, 0, 250),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[MOS, AKA Men of Steel. Mission Objective, Protect and take all orders from Fleet Admiral and only Fleet Admiral.]],
   weapons = {"tfa_revival_spartan_laser","tfa_revival_srs992d", "tfa_revival_ht_assault_rifle", "climb_swep2", "revival_mos", "revival_c4", "weapon_sh_flashbang", "weapon_cuff_mp", "weapon_bactainjector", "tfa_revival_ht_shotgun", "weapon_defibrillator", "med_kit", "tfa_revival_ht_magnum", "dk_flare_gun", "weapon_bactanade", "cloaking-infinite", "stungun"},
   command = "menofsteelleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Men Of Steel",
   faction = 1,
   comms = {
      ["MOS"] = true,
      ["NAVCOM"] = true,
      ["NAVOPS"] = true,
      ["NAVSPECWAR"] = true,
      ["SF"] = true,
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=70
   ply:SetMaxHealth(400)
   ply:SetArmor(400)
   ply:SetRunSpeed(264)
   ply:SetWalkSpeed(180)
end
})
TEAM_MOSMEMBER = DarkRP.createJob("MOS Squad Member", {
   color = Color(200, 50, 0, 250),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[MOS, AKA Men of Steel. Mission Objective, Protect and take all orders from Fleet Admiral and only Fleet Admiral.]],
   weapons = {"tfa_revival_spartan_laser","tfa_revival_srs992d", "tfa_revival_ma5c", "climb_swep2", "revival_mos", "revival_c4", "weapon_sh_flashbang", "weapon_cuff_mp", "weapon_bactainjector", "tfa_revival_m45s", "weapon_defibrillator", "med_kit", "tfa_revival_m6s", "cloaking-infinite", "stungun"},
   command = "menofsteelmember",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Men Of Steel",
   faction = 1,
   comms = {
      ["MOS"] = true,
      ["NAVCOM"] = true,
      ["NAVOPS"] = true,
      ["NAVSPECWAR"] = true,
      ["SF"] = true,
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=70
   ply:SetMaxHealth(400)
   ply:SetArmor(200)
   ply:SetRunSpeed(264)
   ply:SetWalkSpeed(180)
end
})
TEAM_STRIKECOMMAND = DarkRP.createJob("ONI S.T.R.I.K.E. Commander", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI's most unknown shadow force, only known for security. "To Our Last Breath, We Give Our All, First In Never Out."]],
   weapons = {"revival_strike", "tfa_revival_ht_magnum", "weapon_cuff_mp", "tfa_revival_ht_shotgun","tfa_revival_m7s", "weapon_sh_flashbang", "weapon_phase", "revival_c4", "cloaking-infinite", "stungun", "climb_swep2", "weapon_bactainjector", "tfa_revival_ht_assault_rifle", "weapon_bactanade", "hard_sound_rifle", "weapon_vj_flaregun", "dk_flare_gun"},
   command = "onistrikecommander",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI Security Force",
   faction = 1, 
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["STRIKE"] = true,
      ["JSEC"] = true,
      ["SWORD"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["S0"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply:SetArmor(300)
   ply.HaloArmor=75
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(300)
   ply:SetWalkSpeed(170)
end
})
TEAM_STRIKEXO = DarkRP.createJob("ONI S.T.R.I.K.E. XO", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI's most unknown shadow force, only known for security. "To Our Last Breath, We Give Our All, First In Never Out."]],
   weapons = {"tfa_revival_ht_assault_rifle", "revival_strike", "tfa_revival_ht_magnum", "weapon_cuff_mp", "tfa_revival_ht_shotgun", "tfa_revival_m7s", "hard_sound_rifle", "weapon_sh_flashbang", "revival_c4", "cloaking-infinite", "stungun", "climb_swep2", "weapon_bactainjector", "weapon_bactanade", "dk_flare_gun"},
   command = "onistrikexo",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "ONI Security Force", 
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["STRIKE"] = true,
      ["JSEC"] = true,
      ["SWORD"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply:SetArmor(300)
   ply.HaloArmor=75
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(300)
   ply:SetWalkSpeed(170)
end
})
TEAM_BATTALIONCOMMANDER = DarkRP.createJob("Baker Battalion Commander", {
   color = Color(161, 28, 28, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Battalion Commanding Officer is the last and the highest ranking in the Baker Command staff. They are to make sure the battalion is running in an optimal capacity and solve any issues brought up to him. This person will likely not be seen at the front lines without some sort of escort but will not shy away from combat if it is necessary.]],
   weapons = {"Voice_amplifier", "weapon_cuff_mp", "climb_swep2","tfa_revival_br55", "revival_bsm", "weapon_medkit", "tfa_meh_ma5d_masterkey"," tfa_revival_m6g", "tfa_revival_m247_gpmg", "dk_flare_gun"},
   command = "battalionco",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "HQ Command",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
      ["WHISKEY"] = true,
      ["WARDOG"] = true,
      ["DEVILDOG"] = true,
      ["RHINO"] = true,
      ["BAKER"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=55
   ply:SetMaxHealth(350)
end
})
TEAM_BATTALIONEXECUTIVE = DarkRP.createJob("Baker Battalion XO", {
   color = Color(161, 28, 28, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_g_john.mdl","models/ishi/halo_rebirth/player/odst/female/odst_xeria.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Battalion Executive Officer is the Second in Command of Baker battalion. They are primarily responsible for finding replacement candidates for Platoons leads should the previous incumbent fail in his duties, retire, or fall in combat. They also assist the CO in his duties and is capable of filling in for him should he be unable to preform his duties.]],
   weapons = {"Voice_amplifier", "weapon_cuff_mp", "climb_swep2","tfa_revival_m7", "revival_bsm", "weapon_medkit", "tfa_meh_ma5d_masterkey","tfa_revival_m6g", "tfa_revival_m247_gpmg", "dk_flare_gun"},
   command = "battalionexec",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "HQ Command", 
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
      ["WHISKEY"] = true,
      ["WARDOG"] = true,
      ["DEVILDOG"] = true,
      ["RHINO"] = true,
      ["BAKER"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=55
   ply:SetMaxHealth(350)
end
})
TEAM_SMMCMARINES = DarkRP.createJob("Baker Battalion SgtMaj", {
   color = Color(161, 28, 28, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_xeria.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Battalion Sargent Major of Baker Battalion they are the first in the chain of command of the Baker Staff. They are in charge of Baker NCOs and as such will be seen on the front line more often then his higher ranking counterparts]],
   weapons = {"Voice_amplifier", "tfa_revival_m247_gpmg", "weapon_cuff_mp", "climb_swep2","tfa_revival_m7", "revival_bsm", "weapon_medkit", "tfa_revival_m6g", "tfa_revival_br55", "dk_flare_gun"},
   command = "smmcmarines",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "HQ Command", 
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
      ["WHISKEY"] = true,
      ["WARDOG"] = true,
      ["DEVILDOG"] = true,
      ["RHINO"] = true,
      ["BAKER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=50
   ply:SetMaxHealth(350)
end
})
TEAM_REPORTER = DarkRP.createJob("UEG Reporter", {
   color = Color(121, 23, 196, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[The UEG Reporter is the person that is assigned to document the UNSC and their Allies and submit photos to higher and articles to highlight the efforts accomplished by the UNSC.]],
   weapons = {"Gmod_camera", "keys", "climb_swep2"},
   command = "unscreporter",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC Headquarters Element",
   faction = 1,
   comms = {
   },
PlayerSpawn = function(ply)
   ply.HaloArmor=25
end
})
TEAM_ALPHACOMM = DarkRP.createJob("UNICOM General", {
   color = Color(210, 180, 140, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[The Basic line infantry is responsible for most land-based military operations, as well as the protection of Naval vessels and installations from attack from enemy forces. The Basic Infantry Company is the backbone of the UNSC and they form the main body of any force that will be assigned to fight any battle.]],
   weapons = {"tfa_revival_m247_gpmg","tfa_halo2_smg_dual_anniversary","Voice_amplifier","weapon_cuff_mp","climb_swep2","revival_lima","med_kit","tfa_revival_m90c","tfa_revival_m6g", "dk_flare_gun"},
   command = "alphacocomm",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "HQ Command", 
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["UNSCA"] = true,
      ["BAKER"] = true,
      ["PHALANX"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetMaxHealth(300)
end
})
TEAM_ALPHACOSGT = DarkRP.createJob("UNICOM Drill Instructor", {
   color = Color(210, 180, 140, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[The Basic line infantry is responsible for most land-based military operations, as well as the protection of Naval vessels and installations from attack from enemy forces. The Basic Infantry Company is the backbone of the UNSC and they form the main body of any force that will be assigned to fight any battle.]],
   weapons = {"tfa_revival_m247_gpmg","tfa_revival_ma5b","Voice_amplifier","weapon_cuff_mp","climb_swep2","revival_lima","med_kit","tfa_revival_m90c","tfa_revival_m6g"},
   command = "alphacosgt",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "HQ Command", 
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["UNSCA"] = true,
      ["BAKER"] = true,
      ["DI"] = true,
      ["PHALANX"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetMaxHealth(300)
end
})
TEAM_LIMASARGT = DarkRP.createJob("Chimera Platoon sergeant", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl", "models/ishi/halo_rebirth/player/marines/female/marine_dominique.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"Voice_amplifier", "weapon_cuff_mp", "climb_swep2", "revival_chimera", "weapon_medkit", "tfa_revival_m90c"," tfa_revival_m6g", "tfa_revival_ma5c", "dk_flare_gun"},
   command = "limasquadpsgt",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=50
   ply:SetMaxHealth(250)
end
})
TEAM_LIMASQUADLEAD = DarkRP.createJob("Chimera Squad lead", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl","models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_m90c","tfa_rebirth_ma5c", "revival_chimera", "tfa_revival_m6g", "keys", "voice_amplifier", "climb_swep2", "dk_flare_gun"},
   command = "lsquadleader",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=50
   ply:SetMaxHealth(250)
end
})
TEAM_VSARGT = DarkRP.createJob("Lima 1-1 Platoon Sergeant", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_g_michael.mdl","models/ishi/halo_rebirth/player/marines/female/marine_linda.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_ma5b", "revival_lima", "tfa_revival_m6g", "keys", "voice_amplifier", "tfa_revival_m90c", "climb_swep2", "dk_flare_gun"},
   command = "vsquadpsgt",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=50
   ply:SetMaxHealth(250)
end
})
TEAM_KSQUADLEAD = DarkRP.createJob("Lima 1-1 AT", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_m41", "tfa_revival_ma5b", "climb_swep2", "revival_lima"},
   command = "ksquadleader",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 1-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=50
   ply:SetMaxHealth(250)
end
})
TEAM_KSARGT = DarkRP.createJob("Chimera AT", {
   color = Color(255, 130, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_eyecberg.mdl"},
   description = [[A UNSC grunt force, Chimera was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_revival_m6g","tfa_revival_m41", "tfa_revival_ma5c", "med_kit", "climb_swep2"},
   command = "ksquadpsgt",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Chimera",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CHIMERA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=50
   ply:SetMaxHealth(250)
end
})
TEAM_KSQUADPLEAD = DarkRP.createJob("Naval Field Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl", "models/gonzo/unscofficers/lasky/lasky.mdl"},
   description = [[Naval Field officer.]],
   weapons = {"Voice_amplifier","tfa_revival_br55", "tfa_rebirth_ma5c", "tfa_revival_m6g", "dk_flare_gun"},
   command = "Nfofficer",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   faction = 1,
   category = "Naval Operations",
   comms = {
      ["NAVCOM"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVOPS"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=50
   ply:SetArmor(150)
   ply:SetMaxHealth(250)
end
})
TEAM_DOGMAOFFLEAD = DarkRP.createJob("Cerberus Spec OPS Officer Lead", {
   color = Color(62, 44, 90, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Cerberus Jump Squad is the UNSC’s most mobile and agile unit. Utilizing Jet Packs they are able to get in and out of Hot Zones with ease. Cerberus Prides themselves on their ability to operate both on the ground as well as in Zero Gravity. Nothing is out of reach when these troopers strap on their packs and que up for take off.]],
   weapons = {"revival_c4", "med_kit", "revival_cerberus", "tfa_revival_srs992d", "climb_swep2", "darkrp_defibrilator", "weapon_sh_flashbang", "weapon_cuff_mp", "tfa_revival_m7s", "tfa_revival_m6s","tfa_revival_m45s", "dk_flare_gun"},
   command = "dogmaofflead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Cerberus",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CERBERUS"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetArmor(150)
   ply:SetMaxHealth(300)
end
})
TEAM_DOGMAWARLEAD = DarkRP.createJob("Cerberus Spec Ops Warrant Lead", {
   color = Color(62, 44, 90, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Cerberus Jump Squad is the UNSC’s most mobile and agile unit. Utilizing Jet Packs they are able to get in and out of Hot Zones with ease. Cerberus Prides themselves on their ability to operate both on the ground as well as in Zero Gravity. Nothing is out of reach when these troopers strap on their packs and que up for take off.]],
   weapons = {"revival_c4", "med_kit", "revival_cerberus", "tfa_revival_srs992d", "climb_swep2", "darkrp_defibrilator", "weapon_sh_flashbang", "weapon_cuff_mp", "tfa_revival_m7s", "tfa_revival_m6s","tfa_revival_m45s", "weapon_vj_flaregun", "dk_flare_gun"},
   command = "dogmawarlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Cerberus",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CERBERUS"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetArmor(150)
   ply:SetMaxHealth(300)
end
})
TEAM_DOGMAJTROOPC = DarkRP.createJob("Cerberus Spec OPS", {
   color = Color(62, 44, 90, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Cerberus Jump Squad is the UNSC’s most mobile and agile unit. Utilizing Jet Packs they are able to get in and out of Hot Zones with ease. Cerberus Prides themselves on their ability to operate both on the ground as well as in Zero Gravity. Nothing is out of reach when these troopers strap on their packs and que up for take off.]],
   weapons = {"revival_c4", "med_kit", "revival_cerberus", "tfa_revival_srs992d", "climb_swep2", "darkrp_defibrilator", "weapon_sh_flashbang", "weapon_cuff_mp", "tfa_revival_m7s", "tfa_revival_m6s","tfa_revival_m45s"},
   command = "dogmajumptroop",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Cerberus",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CERBERUS"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetArmor(150)
   ply:SetMaxHealth(300)
end
})
TEAM_RHINOOFFLEAD = DarkRP.createJob("Lima 3-1 Rhino HazOp", {
   color = Color(204, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_g_yasser.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Rhino Heavy Squad is the support squad that is quick to place effect fire on any objective that is in there way. Suppression is there priority, filling the bodies of their enemies with holes is their joy. If you ever need a quick response unit that specializes in pulling you and your men out of a tough position, you know who call. These tough beast will charge threw hell and high water to make sure that their mission is accomplished.]],
   weapons = {"voice_amplifier", "tfa_revival_m7", "tfa_revival_m247_gpmg", "climb_swep2", "revival_lima", "tfa_revival_br55", "tfa_meh_ma5d_masterkey", "weapon_bactainjector", "weapon_defibrillator"},
   command = "rhinoofflead",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 3-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["RHINO"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=60
   ply:SetMaxHealth(325)
end
})
TEAM_RHINOWARRLEAD = DarkRP.createJob("Lima 3-1 Rhino Fireteam Lead", {
   color = Color(204, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_g_yasser.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Rhino Heavy Squad is the support squad that is quick to place effect fire on any objective that is in there way. Suppression is there priority, filling the bodies of their enemies with holes is their joy. If you ever need a quick response unit that specializes in pulling you and your men out of a tough position, you know who call. These tough beast will charge threw hell and high water to make sure that their mission is accomplished.]],
   weapons = {"voice_amplifier", "tfa_revival_m247_gpmg", "tfa_revival_br55", "tfa_rebirth_m7", "tfa_revival_m6g", "tfa_revival_m45s", "climb_swep2", "revival_lima", "weapon_bactainjector", "weapon_defibrillator", "dk_flare_gun"},
   command = "rhinowarrlead",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 3-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["RHINO"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=60
   ply:SetMaxHealth(325)
end
})
TEAM_RHINOTROOPER = DarkRP.createJob("Lima 3-1 Rhino Commando", {
   color = Color(204, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_g_yasser.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Rhino Heavy Squad is the support squad that is quick to place effect fire on any objective that is in there way. Suppression is there priority, filling the bodies of their enemies with holes is their joy. If you ever need a quick response unit that specializes in pulling you and your men out of a tough position, you know who call. These tough beast will charge threw hell and high water to make sure that their mission is accomplished.]],
   weapons = {"tfa_revival_m6g", "tfa_revival_m247_gpmg", "climb_swep2", "revival_lima", "tfa_meh_ma5d_masterkey", "tfa_rebirth_m7", "tfa_revival_br55", "weapon_bactainjector", "med_kit", "weapon_defibrillator"},
   command = "rhinotrooper",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 3-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["RHINO"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=60
   ply:SetMaxHealth(325)
end
})
TEAM_WHISKEYOFFLEAD = DarkRP.createJob("Lima 2-1 Whiskey Platoon Lead", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Whiskey Spec Ops Squad is on of the most elite units that falls under the UNSC. Whiskey’s primary task is classified as Covert Operations that pit the best men and women that the UNSC has to offer against any foreign assalant that faces the UNSC. These Troopers will eliminate it at all cost and you will never see them coming. csgo_huntsman in the Wind as it were.]],
   weapons = {"voice_amplifier","tfa_revival_ma5c", "tfa_revival_m45s", "revival_lima", "tfa_revival_m7s", "climb_swep2", "tfa_revival_m6s", "dk_flare_gun"},
   command = "whiskeyofflead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 2-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["WHISKEY"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=48
   ply:SetMaxHealth(275)
end
})
TEAM_WHISKEYWARRLEAD = DarkRP.createJob("Lima 2-1 Whiskey Warrant Lead", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_hank.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_danlin.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[The Whiskey Spec Ops Squad is on of the most elite units that falls under the UNSC. Whiskey’s primary task is classified as Covert Operations that pit the best men and women that the UNSC has to offer against any foreign assalant that faces the UNSC. These Troopers will eliminate it at all cost and you will never see them coming. csgo_huntsman in the Wind as it were.]],
   weapons = {"Voice_amplifier", "tfa_revival_saw", "revival_lima", "keys", "tfa_revival_m45s", "tfa_revival_m7", "climb_swep2", "weapon_vj_flaregun", "dk_flare_gun"},
   command = "whiskeywarrlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 2-1 Platoon",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["WHISKEY"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=48
   ply:SetMaxHealth(250)
end
})
TEAM_SOSRGRUNT = DarkRP.createJob("SoS Ranger Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/valk/haloreach/covenant/characters/grunt/grunt_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Keys", "weapon_plasmanade", "drc_plasma_pistol", "drc_plasma_rifle", "climb_swep2"},
   command = "sosrgrunt",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["GRUNT"] = true,
      ["RANGER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=40
   ply:SetMaxHealth(200)
end
})
TEAM_HOJBLOODSTARS = DarkRP.createJob("HoJ Bloodstars", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys","drc_mauler", "weapon_plasmagrenade", "weapon_fists", "drc_spiker", "drc_plasma_rifle_brute", "drc_knife_brute", "climb_swep2", "cloaking-infinite", "weapon_ninjaskunai", "weapon_bactainjector"},
   command = "hojbloodstars",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply.HaloArmor=94
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(370)
   ply:SetWalkSpeed(250)
   ply:SetJumpPower(330)
end
})
TEAM_HOJBERSERKER = DarkRP.createJob("HoJ Berserker", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys", "weapon_plasmagrenade", "weapon_fists", "drc_mauler", "drc_bruteshot", "drc_plasma_rifle_brute", "climb_swep2", "drc_knife_brute", "drc_gravityhammer"},
   command = "hojb",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(1000)
   ply.HaloArmor=80
   ply:SetMaxHealth(1000)
   ply:SetRunSpeed(360)
   ply:SetWalkSpeed(260)
   ply:SetJumpPower(300)
end
})
TEAM_HOJCHIEF = DarkRP.createJob("HoJ Warlord", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys", "drc_fistofrukt", "weapon_plasmagrenade", "weapon_fists", "weapon_bactanade", "drc_bruteshot", "drc_plasma_rifle_brute", "drc_t33b", "drc_knife_brute", "climb_swep2", "drc_spiker", "drc_mauler", "dk_flare_gun"},
   command = "hojc",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
      ["JUMPER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(1000)
   ply.HaloArmor=80
   ply:SetMaxHealth(1000)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
   ply:SetJumpPower(300)
end
})
TEAM_HOJHIGH = DarkRP.createJob("HoJ High Command", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys", "drc_mauler", "drc_t51", "drc_plasma_repeater", "drc_bruteshot", "drc_focusrifle", "drc_t50srs", "drc_gravityhammer", "drc_t33a", "weapon_fists", "drc_knife_brute", "climb_swep2",  "dk_flare_gun", "weapon_bactanade"},
   command = "hojh",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
      ["JUMPER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(950)
   ply.HaloArmor=80
   ply:SetMaxHealth(950)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
   ply:SetJumpPower(300)
end
})
TEAM_HOJLOW = DarkRP.createJob("HoJ Low Command", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys", "drc_mauler", "drc_t51", "drc_plasma_repeater", "drc_bruteshot", "drc_focusrifle", "drc_t50srs", "drc_gravityhammer", "weapon_fists", "drc_knife_brute", "climb_swep2",  "dk_flare_gun"},
   command = "hojl",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
      ["JUMPER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(900)
   ply.HaloArmor=80
   ply:SetMaxHealth(900)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
   ply:SetJumpPower(300)
end
})
TEAM_HOJOFF = DarkRP.createJob("HoJ Officer", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys", "drc_mauler", "drc_t51", "weapon_fists", "drc_knife_brute", "climb_swep2", "dk_flare_gun"},
   command = "hojo",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
      ["JUMPER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(850)
   ply.HaloArmor=80
   ply:SetMaxHealth(850)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
   ply:SetJumpPower(300)
end
})
TEAM_HOJBRUTE = DarkRP.createJob("HoJ Brute", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys", "weapon_plasmagrenade", "weapon_fists", "drc_spiker", "drc_plasma_rifle_brute", "drc_knife_brute", "climb_swep2"},
   command = "hojbrute",
   max = 18,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply.HaloArmor=80
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
   ply:SetJumpPower(300)
end
})
TEAM_HOJJUMPER = DarkRP.createJob("HoJ Jiralhanae Jumper", {
   color = Color(255, 17, 0, 255),
   model = {"models/valk/haloreach/covenant/characters/brute/brute.mdl","models/valk/halo3/covenant/characters/brute/brute.mdl"},
   description = [[The Hearts of Jiralhanae are a mercenary faction whom are currently contracted to work alongside the Resurrection Detachment.]],
   weapons = {"keys", "weapon_plasmagrenade", "drc_spiker", "drc_plasma_rifle_brute", "drc_knife_brute", "drc_mauler", "climb_swep2", "weapon_fists"},
   command = "hojjumper",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 8,
   category = "Hearts of Jiralhanae",
   faction = 0,
   comms = {
      ["HOJ"] = true,
      ["BRUTE"] = true,
      ["JUMPER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=80
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(250)
   ply:SetJumpPower(320)
end
})
TEAM_SOSSPECOPSS = DarkRP.createJob("SoS Spec Ops Skirmisher", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/skirmisher.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Keys", "cloaking-infinite", "weapon_plasmanade", "drc_t50srs", "drc_plasma_rifle", "climb_swep2"},
   command = "sossoprskirm",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["JACKAL"] = true,
      ["SPEC-OPS"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=30
   ply:SetMaxHealth(200)
   ply:SetRunSpeed(380)
   ply:SetWalkSpeed(325)
end
})
TEAM_SOSOPGRUNT = DarkRP.createJob("SoS Spec Ops Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/valk/haloreach/covenant/characters/grunt/grunt_player.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"keys", "weapon_plasmanade", "cloaking-infinite", "drc_plasma_rifle", "drc_plasma_pistol", "climb_swep2"},
   command = "sosopgrunt",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["GRUNT"] = true,
      ["SPEC-OPS"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(225)
   ply.HaloArmor=40
   ply:SetMaxHealth(225)
end
})
TEAM_SOSRJACK = DarkRP.createJob("SoS Ranger Jackal", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/jackal.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"keys", "drc_t50srs", "drc_plasma_rifle", "climb_swep2"},
   command = "sosrjack",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["JACKAL"] = true,
      ["RANGER"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(175)
   ply.HaloArmor=30
   ply:SetMaxHealth(175)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(200)
end
})
TEAM_SOSMEDICSANG = DarkRP.createJob("Ascetic", {
   color = Color(127, 0, 255, 255),
   model = {"models/vuthakral/halo/custom/usp/sangheili_universal_hg_player.mdl"},
   description = [[Sangheili Ascetics are the most dedicated to the purification of Sangheili culture, Ascetics will try to convert non-SoS Sangheili to the Swords of Sanghelios without bloodshed and through reason.]],
   weapons = {"drc_energysword", "drc_plasma_rifle", "drc_t57b", "drc_t50srs", "climb_swep2", "Weapon_plasmanade", "Weapon_bactainjector", "Weapon_bactanade", "weapon_defibrillator"},
   command = "sossangmed",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Swords of Sanghelios",
   faction = 2,
   comms = {
      ["ELITE"] = true,
      ["ASCETIC"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(600)
   ply.HaloArmor=76
   ply:SetMaxHealth(500)
   ply:SetRunSpeed(320)
   ply:SetWalkSpeed(250)
end
})
TEAM_NAVALLASTCH = DarkRP.createJob("Naval Last Chancers", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_eyecberg.mdl","models/ishi/halo_rebirth/player/marines/female/marine_snowy.mdl"},
   description = [[The Last Chancers are a suicide squad hand picked from death row inmates, they are given a second chance, but at a price of being sent on high risk missions and into situations where they will likely die. They do this by choice, knowing that the alternative is death anyway. Their reward for service is freedom, if they survive...]],
   weapons = {"tfa_revival_m7s", "revival_navy", "tfa_revival_m90c", "tfa_rebirth_ma5c", "keys", "climb_swep2","tfa_revival_m6g", "tfa_revival_saw"},
   command = "navalchancer",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Task Groups",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["LASTCHANCER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=65
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(250)
end
})
TEAM_NAVALBROFFICER = DarkRP.createJob("Naval Bridge Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/keyes/keyes.mdl","models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl"},
   description = [[Operations deals with navigation, Weaponry and act as command of the bridge. Each Naval Officer will have a certain job to do such as Navigation (NAV), Weaponry (WEP). As a Crewman (CN) and up you are able to gun a Frigate during events and mini events on station to help protect the UNSC Revival or to help the UNSC overall during offensive or defensive missions. You will be able to pilot a frigate at a rank of Petty Officer 3 (PO3). If you are a Petty Officer 1 (PO1) and above you will have access to the MAC cannon during off station and on station events.]],
   weapons = {"revival_navy", "keys", "tfa_revival_m6g", "voice_amplifier", "climb_swep2", "dk_flare_gun"},
   command = "navalbrofficer",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Naval Operations",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVOPS"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function (ply)
   ply:SetHealth(150)
   ply.HaloArmor=10
   ply:SetMaxHealth(150)
 end
})

TEAM_NAVALCOMMANDER = DarkRP.createJob("Naval Commander", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/cutter/cutter.mdl","models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl"},
   description = [[Operations deals with navigation, Weaponry and act as command of the bridge. Each Naval Officer will have a certain job to do such as Navigation (NAV), Weaponry (WEP). As a Crewman (CN) and up you are able to gun a Frigate during events and mini events on station to help protect the UNSC Revival or to help the UNSC overall during offensive or defensive missions. You will be able to pilot a frigate at a rank of Petty Officer 3 (PO3). If you are a Petty Officer 1 (PO1) and above you will have access to the MAC cannon during off station and on station events.]],
   weapons = {"tfa_revival_spartan_laser","tfa_rebirth_ma5c","tfa_rebirth_m394","tfa_revival_m6g", "keys", "revival_navy", "voice_amplifier", "climb_swep2", "dk_flare_gun"},
   command = "navalcommander",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Naval Operations",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVOPS"] = true,
      ["NAVSPECWAR"] = true,
      ["SF"] = true,
      ["REAPER"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function (ply)
   ply:SetHealth(150)
   ply.HaloArmor=10
   ply:SetMaxHealth(150)
 end
})

TEAM_NAVALINTOFF = DarkRP.createJob("CENTCOM Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/lasky/lasky.mdl","models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl"},
   description = [[Operations deals with navigation, Weaponry and act as command of the bridge. Each Naval Officer will have a certain job to do such as Navigation (NAV), Weaponry (WEP). As a Crewman (CN) and up you are able to gun a Frigate during events and mini events on station to help protect the UNSC Revival or to help the UNSC overall during offensive or defensive missions. You will be able to pilot a frigate at a rank of Petty Officer 3 (PO3). If you are a Petty Officer 1 (PO1) and above you will have access to the MAC cannon during off station and on station events.]],
   weapons = {"tfa_revival_m6g", "keys", "revival_navy", "voice_amplifier", "climb_swep2", "dk_flare_gun"},
   command = "navalintoff",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval Operations",
   faction = 1,
   comms = {
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
PlayerSpawn = function (ply)
   ply:SetHealth(150)
   ply.HaloArmor=10
   ply:SetMaxHealth(150)
 end
})
TEAM_HELLSQL = DarkRP.createJob("Hellfire Merc", {
   color = Color(112, 0, 0, 255),
   model = {"models/halo4/spartan_pm/group04/spartan_male09_pm.mdl", "models/halo4/spartan_pm/group04/spartan_male09_pm.mdl"},
   description =  [[Hellfire Squad Lead.]] ,
   weapons = {"halo_cryo","weapon_grapplehook", "revival_hellfire", "tfa_revival_ma5c", "tfa_revival_m247_gpmg","tfa_revival_m41","tfa_revival_m6s","climb_swep2","Weapon_sh_flashbang", "Weapon_slam", "force_shield_weapon"},
   command = "hellfsql",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Hellfire",
   faction = 1,
   comms = {
      ["MISTHIOS"] = true,
      ["UNSCA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=53
   ply:SetArmor(400)
   ply:SetRunSpeed(300)
   ply:SetMaxHealth(600)
end
})

TEAM_URFBJ = DarkRP.createJob("URF Battlejumper", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Battlejumper, feet first into freedom!]],
   weapons = {"tfa_revival_saw", "tfa_revival_m45s", "tfa_revival_br55", "weapon_bactainjector", "keys", "revival_c4", "climb_swep2", "weapon_sh_flashbang"},
   command = "URFBJ",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
   faction = 3,
   comms = {
      ["URF"] = true,
      ["BATTLEJUMPER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=65
   ply:SetMaxHealth(350)
   ply:SetArmor(400)
   ply:SetRunSpeed(300)
   ply:SetWalkSpeed(200)
end
})

TEAM_URFSEC = DarkRP.createJob("URF I.S.E.C", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF I.S.E.C]],
   weapons = {"tfa_revival_m7s", "tfa_revival_m6s", "tfa_revival_srs992d", "stungun", "weapon_cuff_mp", "keys", "cloaking-infinite", "climb_swep2"},
   command = "URFSEC",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
   faction = 3,
   comms = {
      ["URF"] = true,
      ["ISEC"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=65
   ply:SetMaxHealth(350)
   ply:SetArmor(350)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(190)
end
})
TEAM_URFSOPS = DarkRP.createJob("URF Spec-Ops", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Squad Sniper]],
   weapons = {"tfa_revival_m90c", "tfa_revival_m7", "tfa_revival_m41", "cloaking-3", "stungun", "weapon_cuff_mp", "keys", "climb_swep2"},
   command = "URFSOPS",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
   faction = 3,
   comms = {
      ["URF"] = true,
      ["URF-SPEC-OPS"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(300)
   ply:SetArmor(300)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(190)
end
})

TEAM_URFINF = DarkRP.createJob("URF Infantry", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Squad Heavy]],
   weapons = {"tfa_revival_br55", "tfa_revival_m6g", "tfa_revival_m247_gpmg", "keys", "climb_swep2"},
   command = "URFINF",
   max = 0,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
   faction = 3,
   comms = {
      ["URF"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=60
   ply:SetMaxHealth(250)
   ply:SetArmor(250)
end
})

TEAM_URFCOM = DarkRP.createJob("URF Command", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Command Staff]],
   weapons = {"tfa_revival_br55", "tfa_revival_spartan_laser", "tfa_revival_m6s", "tfa_revival_m45s", "keys", "climb_swep2", "revival_c4", "weapon_bactanade", "weapon_vj_flaregun", "dk_flare_gun"},
   command = "URFCOM",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
   faction = 3,
   comms = {
      ["URF"] = true,
      ["BATTLEJUMPER"] = true,
      ["ISEC"] = true,
      ["URF-SPEC-OPS"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=65
   ply:SetMaxHealth(400)
   ply:SetArmor(400)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(190)
end
})
TEAM_SOSDRONE = DarkRP.createJob("SoS Drone", {
color = Color(127, 0, 255, 255),
model = {"models/player/jds_166/hcn_drone_gen3_playermodel.mdl"},
description = [[The Yanme'e are human sized insectoids that are covered in a natural chitinous exoskeleton that affords them limited armor protection against modern weaponry. These insectoids assist the SoS as an aerial unit, with their wings and flying capabilities.]],
weapons = {"weapon_plasmanade", "drc_plasma_rifle", "drc_plasma_pistol", "climb_swep2"},
command = "sosdrone",
max = 5,
salary = 0,
admin = 0,
vote = false,
hasLiscense = true,
candemote = false,
-- CustomCheck
medic = false,
chief = false,
mayor = false,
hobo = false,
cook = false,
category = "Swords of Sanghelios",
faction = 2,
comms = {
   ["OPEN"] = true,
   ["ALLIED"] = true,
   ["SOS"] = true,
   ["RP"] = true,
   ["DRONE"] = true,
},
PlayerSpawn = function(ply)
ply:SetHealth(250)
ply.HaloArmor=40
ply:SetMaxHealth(250)
ply:SetArmor(50)
end
})
TEAM_SILENTSHADOW = DarkRP.createJob("SoS Silent Shadow" , {
color = Color(0, 0, 0, 255),
model = {"models/sangheili/silentshadow/silent_shadow_player.mdl"},
description = [[The Silent Shadows are a subdivision of the Swords of Sanghelios Special Operations Branch. These warriors fight in stealth with their blood-red energy swords and their modified infiltration harness that has advanced active-camouflage.]],
weapons = {"weapon_cuff_admin", "drc_energysword", "cloaking-infinite", "weapon_plasmanade", "climb_swep2", "keys", "drc_plasma_rifle", "drc_plasma_rifleifle", "drc_plasma_rifle", "drc_energydaggers"},
command = "sosss",
max = 6,
salary = 0,
admin = 0,
vote = false,
hasLiscense = true,
candemote = false,
-- CustomCheck
medic = false,
chief = false,
mayor = false,
hobo = false,
cook = false,
hud = 6,
category = "Silent Shadow",
faction = 2,
comms = {
   ["OPEN"] = true,
   ["ALLIED"] = true,
   ["SPEC-OPS"] = true,
   ["SOS"] = true,
   ["RP"] = true,
   ["SHADOW"] = true,
},
PlayerSpawn = function(ply)
ply:SetHealth(600)
ply.HaloArmor=70
ply:SetMaxHealth(600)
ply:SetArmor(600)
ply:SetRunSpeed(310)
ply:SetWalkSpeed(250)
ply:SetJumpPower(250)
end
})
TEAM_SILENTSHADOWLEAD = DarkRP.createJob("First Blade Officer" , {
color = Color(0, 0, 0, 255),
model = {"models/sangheili/silentshadow/silent_shadow_player.mdl"},
description = [[The Silent Shadows are a subdivision of the Swords of Sanghelios Special Operations Branch. These warriors fight in stealth with their blood-red energy swords and their modified infiltration harness that has advanced active-camouflage.]],
weapons = {"drc_energysword", "climb_swep2", "cloaking-infinite", "drc_t33a", "drc_plasma_repeater", "drc_t50srs", "drc_plasma_rifle", "weapon_bactanade", "weapon_grapplehook", "drc_bm_plasmarifle", "dk_flare_gun"},
command = "sosfbo",
max = 1,
salary = 0,
admin = 0,
vote = false,
hasLiscense = true,
candemote = false,
-- CustomCheck
medic = false,
chief = false,
mayor = false,
hobo = false,
cook = false,
hud = 6,
category = "Silent Shadow",
faction = 2,
comms = {
   ["OPEN"] = true,
   ["ALLIED"] = true,
   ["SPEC-OPS"] = true,
   ["SOS"] = true,
   ["RP"] = true,
   ["SHADOW"] = true,
   ["OFFICER"] = true,
},
PlayerSpawn = function(ply)
ply:SetHealth(800)
ply.HaloArmor=80
ply:SetMaxHealth(800)
ply:SetArmor(800)
ply:SetRunSpeed(310)
ply:SetWalkSpeed(250)
ply:SetJumpPower(250)
end
})
TEAM_SILENTSHADOWSWORD = DarkRP.createJob("SS Swordsman" , {
color = Color(0, 0, 0, 255),
model = {"models/sangheili/silentshadow/silent_shadow_player.mdl"},
description = [[The Silent Shadows are a subdivision of the Swords of Sanghelios Special Operations Branch. These warriors fight in stealth with their blood-red energy swords and their modified infiltration harness that has advanced active-camouflage.]],
weapons = {"cloaking-infinite", "climb_swep2", "drc_plasma_repeater", "drc_bm_plasmarifle", "drc_plasma_rifle", "drc_t33a", "drc_energysword", "weapon_bactanade", "weapon_cuffs_mp", "weapon_grapplehook"},
command = "sossword",
max = 4,
salary = 0,
admin = 0,
vote = false,
hasLiscense = true,
candemote = false,
-- CustomCheck
medic = false,
chief = false,
mayor = false,
hobo = false,
cook = false,
hud = 6,
category = "Silent Shadow",
faction = 2,
comms = {
   ["OPEN"] = true,
   ["SPEC-OPS"] = true,
   ["ALLIED"] = true,
   ["SOS"] = true,
   ["RP"] = true,
   ["SHADOW"] = true,
},
PlayerSpawn = function(ply)
ply:SetHealth(800)
ply.HaloArmor=80
ply:SetMaxHealth(800)
ply:SetArmor(800)
ply:SetRunSpeed(310)
ply:SetWalkSpeed(250)
ply:SetJumpPower(250)
end
})
TEAM_SILENTSHADOWSHARP = DarkRP.createJob("SS Sharpshooter" , {
color = Color(0, 0, 0, 255),
model = {"models/sangheili/silentshadow/silent_shadow_player.mdl"},
description = [[The Silent Shadows are a subdivision of the Swords of Sanghelios Special Operations Branch. These warriors fight in stealth with their blood-red energy swords and their modified infiltration harness that has advanced active-camouflage.]],
weapons = {"cloaking-infinite", "climb_swep2", "drc_t50srs", "drc_plasma_rifle", "drc_plasma_rifleifle", "drc_plasma_pistol", "drc_energydaggers", "weapon_bactanade", "weapon_cuffs_mp", "weapon_grapplehook"},
command = "sossharp",
max = 2,
salary = 0,
admin = 0,
vote = false,
hasLiscense = true,
candemote = false,
-- CustomCheck
medic = false,
chief = false,
mayor = false,
hobo = false,
cook = false,
hud = 6,
category = "Silent Shadow",
faction = 2,
comms = {
   ["OPEN"] = true,
   ["ALLIED"] = true,
   ["SPEC-OPS"] = true,
   ["SOS"] = true,
   ["RP"] = true,
   ["SHADOW"] = true,
},
PlayerSpawn = function(ply)
ply:SetHealth(800)
ply.HaloArmor=80
ply:SetMaxHealth(800)
ply:SetArmor(600)
ply:SetRunSpeed(310)
ply:SetWalkSpeed(250)
ply:SetJumpPower(250)
end
})
TEAM_DEMONSQDL = DarkRP.createJob("Demon Merc", {
   color = Color(55, 0, 0, 255),
   model = {"models/halo4/spartan_pm/group04/spartan_male09_pm.mdl"},   
   description = [[The Demon Squad]],
   weapons = {"tfa_meh_ma5d_masterkey","weapon_grapplehook", "revival_demon", "tfa_revival_m6s", "stungun", "climb_swep2","weapon_cuff_mp", "tfa_revival_m41",  "Weapon_sh_flashbang", "Weapon_slam", "drc_bm_plasmarifle"},
   command = "DEMONSQDL",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Demon Squad",
   faction = 1,
   comms = {
      ["MISTHIOS"] = true,
      ["UNSCA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=48
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(300)
   ply:SetArmor(300)
end
})
TEAM_INFERNOSQL = DarkRP.createJob("Inferno Merc", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group04/spartan_male09_pm.mdl"},   
   description = [[The Inferno Squad]],
   weapons = {"weapon_grapplehook", "revival_inferno", "tfa_rebirth_ma5c", "tfa_revival_m7s","tfa_revival_m6g", "cloaking-infinite", "tfa_revival_srs994am","stungun", "climb_swep2", "weapon_cuff_mp",  "Weapon_sh_flashbang", "revival_c4", "csgo_huntsman"},
   command = "INFERNOSQL",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Inferno Squad",
   faction = 1,
   comms = {
      ["MISTHIOS"] = true,
      ["UNSCA"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=40
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(290)
   ply:SetArmor(250)
end
})
TEAM_GENERALARMY = DarkRP.createJob("Phalanx Battalion Commander", {
   color = Color(0, 102, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description =  [[Phalanx commander]] ,
   weapons = { "tfa_revival_m6s","Voice_amplifier", "tfa_revival_m247_gpmg", "tfa_revival_m45s", "tfa_halo2_smg_dual_anniversary", "revival_phalanx", "Weapon_sh_flashbang", "weapon_cuff_mp", "cloaking-infinite", "climb_swep2", "revival_c4", "dk_flare_gun"},
   command = "GeneralArmy",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "HQ Command",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CERBERUS"] = true,
      ["VALKYRIE"] = true,
      ["PHALANX"] = true,
      ["DM"] = true,
      ["INF"] = true,
      ["HF"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=53
   ply:SetArmor(250)
   ply:SetMaxHealth(350)
end
})
TEAM_XOARMY = DarkRP.createJob("Phalanx Battalion XO", {
   color = Color(0, 102, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description =  [[Phalanx XO]] ,
   weapons = { "tfa_revival_m6s","Voice_amplifier", "tfa_revival_m247_gpmg", "tfa_revival_m45s", "tfa_halo2_smg_dual_anniversary", "revival_phalanx", "Weapon_sh_flashbang", "weapon_cuff_mp", "cloaking-infinite", "climb_swep2", "revival_c4", "dk_flare_gun"},
   command = "XOARMY",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "HQ Command",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CERBERUS"] = true,
      ["VALKYRIE"] = true,
      ["PHALANX"] = true,
      ["DM"] = true,
      ["INF"] = true,
      ["HF"] = true,
      ["PHALANX"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=53
   ply:SetArmor(250)
   ply:SetMaxHealth(325)
end
})
TEAM_SMAARMY = DarkRP.createJob("Phalanx Battalion SGTMAJ", {
   color = Color(0, 102, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description =  [[Phalanx XO]] ,
   weapons = { "tfa_revival_m6s","Voice_amplifier", "tfa_revival_m247_gpmg", "tfa_revival_m45s", "tfa_halo2_smg_dual_anniversary", "revival_phalanx", "Weapon_sh_flashbang", "weapon_cuff_mp", "cloaking-infinite", "climb_swep2", "revival_c4", "dk_flare_gun"},
   command = "SMAARMY",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "HQ Command",
   faction = 1,
   comms = {
      ["UNSCA"] = true,
      ["CERBERUS"] = true,
      ["VALKYRIE"] = true,
      ["PHALANX"] = true,
      ["DM"] = true,
      ["INF"] = true,
      ["HF"] = true,
      ["PHALANX"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=53
   ply:SetArmor(250)
   ply:SetMaxHealth(300)
end
})
TEAM_WARDOGS = DarkRP.createJob("Lima 2-2 War Dogs", {
   color = Color(218, 242, 67, 255),
   model = {"models/ishi/halo_rebirth/player/odst/female/odst_bella.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl","models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl","models/ishi/halo_rebirth/player/odst/male/odst_jeffrey.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[You are the War Dogs of the marines. Essentially a more equipped elite unit meant to be the frontline of the UNSC Marine Force.]],
   weapons = {"tfa_revival_m45s", "tfa_revival_br55", "revival_lima", "climb_swep2", "weapon_slam", "tfa_rebirth_srs99s2am", "tfa_revival_m7", "weapon_cuff_mp", "stungun"},
   command = "wardogs",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 7,
   category = "Lima 2-2 Squad",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["BAKER"] = true,
      ["WARDOG"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=55
   ply:SetRunSpeed(300)
   ply:SetMaxHealth(300)
end
})
TEAM_SCORNED = DarkRP.createJob("Skog", {
   color = Color(0, 0, 0, 255),
   model = {"models/chaosnarwhal/reach/eva.mdl", "models/chaosnarwhal/reach/mkvb.mdl"},   
   description = [[NULL DATA]],
   description = [[The Mercenary for Hire]],
   weapons = {"weapon_bactanade", "weapon_sh_flashbang", "tfa_revival_ht_shotgun", "tfa_revival_ht_assault_rifle", "tfa_revival_spartan_laser", "weapon_fistsofreprisal", "climb_swep2", "tfa_revival_m6g"},
   command = "scorned",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Scorned",
   faction = 1,
   comms = {
      ["MISTHIOS"] = true,
      ["UNSCA"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerSpawn = function(ply)
        ply:SetHealth(600)
        ply:SetArmor(600)
        ply.HaloArmor = 75
        ply:SetMaxHealth(600)
        ply:SetRunSpeed(330)
        ply:SetWalkSpeed(210)
        ply:SetJumpPower(280)
end
})
TEAM_ELEADER = DarkRP.createJob("Eclipse Fireteam Leader", {
   color = Color(51, 51, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[ Eclipse performed Scorched Earth operations during the Human Covenant War and operated deep behind enemy lines as loose Commandos.]],
   weapons = {"tfa_revival_ht_magnum", "revival_nomad", "tfa_revival_ht_assault_rifle", "tfa_revival_ht_shotgun", "tfa_halo2_smg_dual_anniversary", "climb_swep2", "tfa_revival_spartan_laser", "weapon_bactanade", "weapon_bactainjector", "stungun", "weapon_cuff_mp", "cloaking-infinite", "weapon_sh_flashbang", "weapon_bactainjector ", "weapon_fistsofreprisal", "weapon_bactanade", "dk_flare_gun", "weapon_phase"},
   command = "eleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eclipse",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["ECLIPSE"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
   ply:SetJumpPower(280)
end
})
TEAM_ESUPPORT1 = DarkRP.createJob("Eclipse Support", {
   color = Color(51, 51, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[ Eclipse performed Scorched Earth operations during the Human Covenant War and operated deep behind enemy lines as loose Commandos.]],
   weapons = {"tfa_revival_ht_assault_rifle", "tfa_revival_ht_magnum", "tfa_m247h", "revival_nomad", "cloaking-infinite", "climb_swep2", "stungun", "weapon_cuff_mp", "weapon_sh_flashbang", "tfa_revival_m41", "weapon_fistsofreprisal", "weapon_bactainjector"},
   command = "esupport1",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eclipse",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["ECLIPSE"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
   ply:SetJumpPower(280)
end
})
TEAM_ESHARPSHOT = DarkRP.createJob("Eclipse Sharpshooter", {
   color = Color(51, 51, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[ Eclipse performed Scorched Earth operations during the Human Covenant War and operated deep behind enemy lines as loose Commandos.]],
   weapons = {"tfa_revival_m7s", "revival_nomad", "tfa_hr_swep_dmr", "climb_swep2", "chaosnarwhal_m99", "tfa_revival_ht_magnum", "cloaking-infinite", "stungun", "weapon_cuff_mp", "weapon_sh_flashbang", "weapon_fistsofreprisal",},
   command = "esharpshot",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eclipse",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["ECLIPSE"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
   ply:SetJumpPower(280)
end
})
TEAM_ECORPSMAN = DarkRP.createJob("Eclipse Corpsman", {
   color = Color(51, 51, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[ Eclipse performed Scorched Earth operations during the Human Covenant War and operated deep behind enemy lines as loose Commandos.]],
   weapons = {"tfa_revival_ht_magnum","tfa_revival_ht_shotgun", "tfa_revival_ht_assault_rifle", "tfa_hr_swep_dmr", "climb_swep2", "revival_nomad", "cloaking-infinite", "stungun", "weapon_cuff_mp", "weapon_sh_flashbang", "weapon_bactanade", "weapon_bactainjector", "weapon_defibrillator", "weapon_fistsofreprisal", "cloaking-infinite", "weapon_phase"},
   command = "ecorps",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Eclipse",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["ECLIPSE"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
   ply:SetJumpPower(280)
end
})
TEAM_ORIONSQL = DarkRP.createJob("ONI S.W.O.R.D. Commander", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[SWORD is a Strategic Combat Team within ONI tasked with making sure the sections get to their task and make it back alive.]],
   weapons = { "weapon_cuff_mp", "cloaking-infinite", "stungun", "tfa_revival_m7s", "tfa_revival_m45s", "climb_swep2", "hard_sound_rifle", "weapon_grapplehook", "tfa_revival_saw", "weapon_vj_flaregun", "dk_flare_gun", "weapon_bactanade", "weapon_bactainjector", "weapon_defibrillator"},
   command = "orionsql",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "S.W.O.R.D.",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=65
   ply:SetMaxHealth(600)
   ply:SetArmor(300)
   ply:SetRunSpeed(305)
end
})
TEAM_ORIONXO = DarkRP.createJob("ONI S.W.O.R.D. Executive", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[SWORD is a Strategic Combat Team within ONI tasked with making sure the sections get to their task and make it back alive.]],
   weapons = { "weapon_cuff_mp", "tfa_revival_saw", "cloaking-infinite", "stungun", "tfa_revival_m7s", "tfa_revival_m45s", "climb_swep2", "hard_sound_rifle", "weapon_grapplehook", "weapon_vj_flaregun", "dk_flare_gun", "weapon_bactainjector", "weapon_bactanade"},
   command = "orionxo",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "S.W.O.R.D.",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=60
   ply:SetMaxHealth(600)
   ply:SetArmor(300)
   ply:SetRunSpeed(305)
end
})
TEAM_ORIONOP = DarkRP.createJob("ONI S.W.O.R.D. Operator", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[SWORD is a Strategic Combat Team within ONI tasked with making sure the sections get to their task and make it back alive.]],
   weapons = { "weapon_cuff_mp", "cloaking-infinite", "stungun", "tfa_revival_m7s", "tfa_revival_srs992d", "tfa_revival_m45s", "climb_swep2", "tfa_revival_m6s", "weapon_bactanade", "weapon_bactainjector", "weapon_defibrillator", "weapon_sh_flashbang"},
   command = "orionop",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "S.W.O.R.D.",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=60
   ply:SetMaxHealth(600)
   ply:SetArmor(300)
   ply:SetRunSpeed(305)
end
})

TEAM_ORIONHVY = DarkRP.createJob("ONI S.W.O.R.D. Specialist", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[SWORD is a Strategic Combat Team within ONI tasked with making sure the sections get to their task and make it back alive.]],
   weapons = {"tfa_revival_ma5c", "revival_c4", "weapon_cuff_mp", "cloaking-infinite", "stungun", "tfa_revival_srs994am", "climb_swep2", "tfa_revival_m6s", "weapon_bactanade", "weapon_sh_flashbang", "tfa_revival_m45s"},
   command = "orionhvy",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "S.W.O.R.D.",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=60
   ply:SetMaxHealth(600)
   ply:SetArmor(300)
   ply:SetRunSpeed(305)
end
})
TEAM_ORIONHNT = DarkRP.createJob("ONI S.W.O.R.D. Hunter", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[SWORD is a Strategic Combat Team within ONI tasked with making sure the sections get to their task and make it back alive.]],
   weapons = {"tfa_revival_m7s", "tfa_revival_m45s", "hard_sound_rifle", "climb_swep2", "tfa_revival_m6s", "weapon_sh_flashbang", "weapon_cuff_mp", "cloaking-infinite", "stungun", "weapon_grapplehook", "weapon_bactanade"},
   command = "orionhnt",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "S.W.O.R.D.",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["AI"] = true,
      ["SCD"] = true,
      ["ATC"] = true,
      ["S1"] = true,
      ["S2"] = true,
      ["S3"] = true,
      ["SWORD"] = true,
      ["JSEC"] = true,
   },
   ammo = {["Grenade"] = 2},
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=60
   ply:SetMaxHealth(600)
   ply:SetArmor(300)
   ply:SetRunSpeed(305)
end
})
TEAM_ONISPLT = DarkRP.createJob("Spartan Mission Handler", {
   color = Color(160, 160, 160, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[The incumbent Spartan Mission Handler aids the Commander in overseeing all operations of the 51st SPARTAN program as well as the Endeavor.]],
   weapons = {"weapon_bactainjector","weapon_bactanade","chaosnarwhal_m99","tfa_halo2_smg_dual_anniversary","tfa_revival_ht_assault_rifle","tfa_revival_ht_magnum","weapon_fistsofreprisal","stungun","weapon_cuff_mp","cloaking-infinite","tfa_revival_spartan_laser", "climb_swep2", "dk_flare_gun", "weapon_phase", "weapon_grapplehook"},
   command = "spartanexec",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 5,
   category = "Spartan Command Staff",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["NEXUS"] = true,
      ["EDEN"] = true,
      ["XERXES"] = true,
      ["ECLIPSE"] = true,
      ["INVICTA"] = true,
      ["WARDEN"] = true,
      ["SIGMA"] = true,
      ["OFFICER"] = true,
      ["HIGHCOM"] = true,
      ["ARES"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(800)
   ply.HaloArmor=80
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_DEVILDOG = DarkRP.createJob("Lima 2-3 Devil Dogs", {
   color = Color(210, 180, 140, 255),
   model = {"models/ishi/halo_rebirth/player/odst/female/odst_bella.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl","models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/male/odst_jeffrey.mdl", "models/halo_reach/players/marine_female_2.mdl", "models/halo_reach/players/marine_male_2.mdl"},
   description = [[Generation 0 Wardogs who find their roots in the old US Marine Corps known as Devil dogs. They are a group of Marines who are tasked with guarding high ranking officials in UNICOM. Due to their tasking they allow some UNICOM officials to wear Devil dogs armor when going into combat. In recent years their numbers have declined due to budget cuts and an unwillingness to lower the standard of equipment or recruits. This may be a continuing trend, though some say they will one day rise again and get back to their full strength as a unit.]],
   weapons = {"tfa_revival_ht_shotgun", "climb_swep2", "revival_lima", "tfa_revival_ht_magnum", "tfa_revival_ht_assault_rifle", "tfa_revival_srs994am", "weapon_bactanade"},
   command = "devildog",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "Lima 2-3 Squad",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["LIMA"] = true,
      ["WHISKEY"] = true,
      ["WARDOG"] = true,
      ["DEVILDOG"] = true,
      ["RHINO"] = true,
      ["BAKER"] = true,
      ["DEVILDOG"] = true,
      ["MP"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(250)
   ply.HaloArmor=80
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
-- Monitors

-- FREELANCERS
TEAM_FAUSTCOMPANY = DarkRP.createJob("Faust Company", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group01/spartan_male02_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male03_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl"},
   description = [[Top of the line frontline infantry specializing in explosives and hazardous situations, Faust company leads the way for the Freelancer program.]],
   weapons = {"tfa_revival_m41", "climb_swep2", "revival_faust", "tfa_revival_m6g", "cloaking-infinite", "tfa_revival_ht_assault_rifle", "tfa_revival_m45s", "revival_c4", "weapon_bactainjector", "force_shield_weapon"},
   command = "FCAGENT",
   max = 12,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Freelancer",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
      ["FAUST"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(305)
   ply:SetWalkSpeed(185)
end
})
TEAM_HELLHOUNDAGENT = DarkRP.createJob("Hellhound Agent", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group01/spartan_male02_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male03_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl"},
   description = [[Not much is known about Hellhound squad, Other than they are the right hand of the director, caution is advised.]],
   weapons = {"climb_swep2", "tfa_revival_ht_shotgun", "revival_hellhound", "cloaking-infinite", "tfa_revival_m7s", "tfa_revival_m247_gpmg", "tfa_revival_srs992d", "revival_c4", "stungun", "weapon_bactainjector", "tfa_revival_ht_magnum","weapon_cuff_mp", "force_shield_weapon"},
   command = "HHAGENT",
   max = 7,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Freelancer",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
      ["NOX"] = true,
      ["FAUST"] = true,
      ["82ND"] = true,
      ["HELLHOUND"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(305)
   ply:SetWalkSpeed(185)
end
})
TEAM_NOXCOMPANY = DarkRP.createJob("Nox Company", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group01/spartan_male02_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male03_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl"},
   description = [[Top of the line when it comes to Sharpshooting, Interrogation, Recon, and Intel gathering. NOX Company acts as the eyes and ears of the freelancer program.]],
   weapons = {"climb_swep2", "tfa_revival_m7s", "revival_nox", "cloaking-infinite","tfa_revival_ht_assault_rifle", "tfa_revival_srs992d", "tfa_revival_ht_magnum", "tfa_revival_m7", "stungun", "weapon_cuff_mp", "weapon_bactainjector", "force_shield_weapon", "weapon_vj_flaregun"},
   command = "NCAGENT",
   max = 13,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Freelancer",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
      ["NOX"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(305)
   ply:SetWalkSpeed(185)
end
})
TEAM_82NDAIRBORNE = DarkRP.createJob("82nd Airborne", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group01/spartan_male02_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male03_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl"},
   description = [[High flying, and fast Paced, 82nd are the freelancers that get the freelancer program where they need to be.]],
   weapons = {"climb_swep2", "revival_82nd", "cloaking-infinite", "tfa_revival_ht_assault_rifle", "tfa_revival_ht_shotgun", "tfa_rebirth_srs99s2am", "tfa_revival_ht_magnum", "weapon_bactainjector", "force_shield_weapon"},
   command = "82ndAGENT",
   max = 8,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Freelancer",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
      ["82ND"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(305)
   ply:SetWalkSpeed(185)
end
})
TEAM_FREELANCER = DarkRP.createJob("Freelancer", {
   color = Color(32, 32, 32, 255),
   model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group01/spartan_male02_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male03_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl"},
   description = [[The Freelancer program is a group of mercenaries coming from all walks of life, none above the other. Rule one, Family first.]],
   weapons = {"climb_swep2", "cloaking-infinite", "revival_free", "tfa_revival_ht_magnum", "tfa_revival_m90c", "tfa_revival_ht_assault_rifle", "tfa_revival_m247_gpmg", "weapon_bactainjector", "force_shield_weapon"},
   command = "freelancer",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Freelancer",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(305)
   ply:SetWalkSpeed(185)
end
})

TEAM_HELLHOUNDSQUADLEAD = DarkRP.createJob("Hellhound Squad Lead", {
	color = Color(32, 32, 32, 255),
	model = {"models/halo4/spartan_pm/group01/spartan_male01_pm.mdl", "models/halo4/spartan_pm/group02/spartan_male04_pm.mdl", "models/halo4/spartan_pm/group03/spartan_male06_pm.mdl"},
	description = [[Leader of Hell Hound.]],
	weapons = {"tfa_halo2_smg_dual_anniversary", "climb_swep2", "tfa_m247h", "tfa_revival_ht_magnum", "tfa_revival_srs994am", "revival_hellhound", "tfa_revival_ht_shotgun", "cloaking-infinite", "Weapon_bactainjector", "revival_c4", "Weapon_sh_flashbang", "weapon_cuff_mp", "stungun", "force_shield_weapon", "dk_flare_gun"},
	command = "hellhoundsql",
	max = 2,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = true,
	candemote = false,
	-- CustomCheck
	medic = false,
	chief = false,
	mayor = false,
	hobo = false,
	cook = false,
   hud = 2,
	category = "Hellhound Squad Lead",
   faction = 1,
   comms = {
      ["FREELANCER"] = true,
      ["NOX"] = true,
      ["FAUST"] = true,
      ["82ND"] = true,
      ["HELLHOUND"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
	ply:SetHealth(600)
	ply.HaloArmor=70
	ply:SetMaxHealth(600)
	ply:SetArmor(400)
	ply:SetRunSpeed(305)
    ply:SetWalkSpeed(185)
end
})
TEAM_HEAVYSEN = DarkRP.createJob("Heavy Sentinel", {
    color = Color(100, 100, 100, 255),
    model = {"models/sentinelh3.mdl"},
    description = [[The Forerunners were the creators and builders of many magnificent installations such the Halo Array, the Ark, the Greater Ark, the Shield Worlds, as well as numerous artifacts that were found on the planets Reach, Earth, Sigma Octanus IV, Harvest, Arcadia, Sanghelios, Kholo and the Prophets Homeworld.They were the keepers of “The Mantle of Responsibility”]],
    weapons = {"climb_swep2", "drc_sentinelbeam_major", "force_shield_weapon"},
    command = "heavysen",
    max = 3,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    faction = 0,
    comms = {
      ["FORERUNNER"] = true,
   },
    PlayerSpawn = function(ply)
        ply:SetHealth(1500)
        ply:SetMaxHealth(1500)
        ply:SetArmor(1000)
        ply.HaloArmor=60
    end
})
TEAM_PROTECTORECUM = DarkRP.createJob("Monitor", {
    color = Color(100, 100, 100, 255),
    model = {"models/player/hobo387/didact.mdl"},
    description = [[The Forerunners were the creators and builders of many magnificent installations such the Halo Array, the Ark, the Greater Ark, the Shield Worlds, as well as numerous artifacts that were found on the planets Reach, Earth, Sigma Octanus IV, Harvest, Arcadia, Sanghelios, Kholo and the Prophets Homeworld.They were the keepers of “The Mantle of Responsibility”]],
    weapons = {"tfa_revival_spartan_laser", "climb_swep2", "drc_energysword", "drc_sentinelbeam_major", "drc_sentinelbeam_minor"},
    command = "protector",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    faction = 0,
    comms = {
      ["FORERUNNER"] = true,
   },
    PlayerSpawn = function(ply)
        ply:SetHealth(5000)
        ply:SetArmor(2000)
        ply:SetMaxHealth(5000)
        ply.HaloArmor = 65
    end
})
TEAM_SEN = DarkRP.createJob("Sentinel", {
    color = Color(100, 100, 100, 255),
    model = {"models/sentinelh3.mdl"},
    description = [[The Forerunners were the creators and builders of many magnificent installations such the Halo Array, the Ark, the Greater Ark, the Shield Worlds, as well as numerous artifacts that were found on the planets Reach, Earth, Sigma Octanus IV, Harvest, Arcadia, Sanghelios, Kholo and the Prophets Homeworld.They were the keepers of “The Mantle of Responsibility”]],
    weapons = {"drc_sentinelbeam_minor"},
    command = "sentinel",
    max = 8,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    faction = 0,
    comms = {
      ["FORERUNNER"] = true,
   },
    PlayerSpawn = function(ply)
        ply:SetHealth(750)
        ply:SetMaxHealth(750)
        ply:SetArmor(500)
        ply.HaloArmor=60
    end
})
TEAM_NEXUSSHARP = DarkRP.createJob("Nexus Sharpshooter", {
   color = Color(6, 16, 105, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Nexus is a binary team.]],
   weapons = {"tfa_revival_ht_magnum","weapon_fistsofreprisal", "weapon_bactanade", "weapon_sh_flashbang", "revival_nexus", "tfa_revival_ht_shotgun", "tfa_revival_ht_assault_rifle", "tfa_halo2_smg_dual_anniversary", "chaosnarwhal_m99", "climb_swep2", "weapon_phase"},
   command = "nexusss",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Nexus",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["NEXUS"] = true,
      ["EDEN"] = true,
      ["XERXES"] = true,
      ["ECLIPSE"] = true,
      ["INVICTA"] = true,
      ["WARDEN"] = true,
      ["SIGMA"] = true,
      ["ARES"] = true,
	  ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerSpawn = function(ply)
        ply:SetHealth(700)
        ply:SetArmor(600)
        ply.HaloArmor = 80
        ply:SetMaxHealth(700)
        ply:SetRunSpeed(330)
        ply:SetWalkSpeed(210)
        ply:SetJumpPower(280)
end
})
TEAM_NEXUSSUP = DarkRP.createJob("Nexus Support", {
   color = Color(6, 16, 105, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Nexus is a binary team.]],
   weapons = {"tfa_revival_ht_magnum","weapon_fistsofreprisal", "weapon_bactanade", "weapon_sh_flashbang", "revival_nexus", "tfa_revival_ht_shotgun", "tfa_m247h", "chaosnarwhal_m99", "tfa_halo2_smg_dual_anniversary", "climb_swep2", "weapon_phase"},
   command = "nexussup",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Nexus",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["NEXUS"] = true,
      ["EDEN"] = true,
      ["XERXES"] = true,
      ["ECLIPSE"] = true,
      ["INVICTA"] = true,
      ["WARDEN"] = true,
      ["SIGMA"] = true,
      ["ARES"] = true,
	  ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
    PlayerSpawn = function(ply)
        ply:SetHealth(700)
        ply:SetArmor(600)
        ply.HaloArmor = 80
        ply:SetMaxHealth(700)
        ply:SetRunSpeed(330)
        ply:SetWalkSpeed(210)
        ply:SetJumpPower(280)
end
})
TEAM_TASKPHX = DarkRP.createJob("Naval Special Warfare", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl", "models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl", "models/gonzo/unscofficers/lasky/lasky.mdl"},
   description = [[UNSC Naval Special Warfare is the premier special operations force of the UNSC Navy. Employing elite light infantry, it is almost solely responsible for prosecuting naval special operations (SO)]],
   weapons = {"tfa_revival_m45s", "tfa_revival_srs992d", "tfa_revival_m7s", "weapon_sh_flashbang", "revival_stg", "keys", "weapon_bactainjector", "weapon_defibrillator", "tfa_revival_m6s", "climb_swep2", "med_kit"},
   command = "taskphx",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "NAVSPECWAR",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVSPECWAR"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply:SetArmor(150)
   ply.HaloArmor=60
   ply:SetRunSpeed(300)
   ply:SetMaxHealth(350)
end
})

TEAM_TASKDIR = DarkRP.createJob("NAVSPECWAR Commander", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/cutter/cutter.mdl"},
   description = [[UNSC Naval Special Warfare is the premier special operations force of the UNSC Navy. Employing elite light infantry, it is almost solely responsible for prosecuting naval special operations (SO)]],
   weapons = {"tfa_revival_m45s", "tfa_revival_srs992d", "tfa_revival_m7s", "weapon_sh_flashbang", "tfa_revival_m6s", "revival_stg", "keys", "weapon_bactainjector", "weapon_defibrillator", "voice_amplifier", "weapon_cuff_mp", "climb_swep2", "med_kit", "dk_flare_gun"},
   command = "taskdir",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "NAVSPECWAR",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVSPECWAR"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=60
   ply:SetRunSpeed(300)
   ply:SetMaxHealth(400)
end
})

TEAM_TASKOF = DarkRP.createJob("NAVSPECWAR Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/lasky/lasky.mdl"},
   description = [[UNSC Naval Special Warfare is the premier special operations force of the UNSC Navy. Employing elite light infantry, it is almost solely responsible for prosecuting naval special operations (SO)]],
   weapons = {"tfa_revival_m45s", "tfa_revival_srs992d", "tfa_revival_m7s", "weapon_sh_flashbang", "tfa_revival_m6s", "revival_stg", "keys", "weapon_bactainjector", "weapon_defibrillator", "voice_amplifier", "weapon_cuff_mp", "climb_swep2", "med_kit", "weapon_vj_flaregun", "dk_flare_gun"},
   command = "taskof",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 1,
   category = "NAVSPECWAR",
   faction = 1,
   comms = {
      ["NAVCOM"] = true,
      ["NAVAL"] = true,
      ["ATC"] = true,
      ["NAVSPECWAR"] = true,
      ["OFFICER"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(375)
   ply:SetArmor(150)
   ply.HaloArmor=50
   ply:SetRunSpeed(300)
   ply:SetMaxHealth(375)
end
})
-- Fireteam Warden
TEAM_WARDENSQUADLEADER = DarkRP.createJob("Warden Fireteam Leader", {
   color = Color(255, 255, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Leader of Fireteam Warden.]],
   weapons = {"climb_swep2", "weapon_bactanade", "Weapon_fistsofreprisal","tfa_revival_ht_assault_rifle", "tfa_revival_ht_shotgun", "tfa_revival_m7s", "tfa_revival_m41", "weapon_bactainjector", "stungun", "weapon_defibrillator", "weapon_cuff_mp", "weapon_sh_flashbang", "dk_flare_gun", "weapon_phase",  "cloaking-infinite", "tfa_revival_ht_magnum"},
   command = "wardensquadlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Warden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["WARDEN"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_WARDENCORPSMAN = DarkRP.createJob("Warden Corpsman", {
   color = Color(255, 255, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Member of Fireteam Warden.]],
   weapons = {"climb_swep2", "Weapon_fistsofreprisal","tfa_revival_ht_assault_rifle", "tfa_revival_ht_shotgun", "tfa_revival_m7s", "weapon_bactanade", "weapon_medkit", "weapon_bactainjector", "stungun", "weapon_defibrillator", "weapon_cuff_mp", "cloaking-infinite", "tfa_revival_ht_magnum"},
   command = "wardencorpsman",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Warden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["WARDEN"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_WARDENCORPSMAN = DarkRP.createJob("Warden Sharpshooter", {
   color = Color(255, 255, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Sharpshooter in Fireteam Warden.]],
   weapons = {"climb_swep2", "Weapon_fistsofreprisal", "weapon_bactanade","tfa_revival_ht_assault_rifle", "chaosnarwhal_m99", "tfa_revival_m7s", "stungun", "weapon_cuff_mp", "weapon_defibrillator", "cloaking-infinite", "weapon_bactainjector",  "tfa_revival_ht_magnum"},
   command = "wardensharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Warden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["WARDEN"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_WARDENCORPSMAN = DarkRP.createJob("Warden Support", {
   color = Color(255, 255, 255, 255),
   model = {"models/chaosnarwhal/reach/mkvb.mdl", "models/chaosnarwhal/reach/eod.mdl", "models/chaosnarwhal/reach/mkiv.mdl", "models/chaosnarwhal/reach/recon.mdl", "models/suno/player/zeus/male/formal_male_09.mdl", "models/suno/player/zeus/female/fang.mdl"},   
   description = [[Support for Fireteam Warden.]],
   weapons = {"climb_swep2", "tfa_revival_ht_shotgun", "tfa_revival_m41", "weapon_bactanade", "tfa_revival_br55", "Weapon_fistsofreprisal", "tfa_revival_ht_assault_rifle", "weapon_bactainjector", "stungun", "weapon_defibrillator", "weapon_cuff_mp", "weapon_sh_flashbang", "drc_c12",  "cloaking-infinite", "tfa_revival_ht_magnum"},
   command = "wardensupport",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 2,
   category = "Warden",
   faction = 1,
   comms = {
      ["SPARTAN"] = true,
      ["ATC"] = true,
      ["WARDEN"] = true,
   },
   ammo = {["Grenade"] = 2},
PlayerSpawn = function(ply)
   ply:SetHealth(700)
   ply.HaloArmor=80
   ply:SetArmor(600)
   ply:SetMaxHealth(700)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_HeathenCC = DarkRP.createJob("Viking Lead", {
   color = Color(0, 0, 0, 255),
   model = {"models/sangheili/silentshadow/silent_shadow_player.mdl"},
   description = [[Viking Binary Lead. A mercenary for hire.]],
   weapons = {"drc_plasma_rifle", "drc_energysword", "drc_t50srs", "weapon_cuff_admin", "cloaking-infinite", "weapon_bactanade", "climb_swep2", "stungun", "weapon_fists", "dk_flare_gun"},
   command = "heathenCC",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6,
   category = "Viking Binary",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["OFFICER"] = true,
      ["VIKING"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(1250)
   ply:SetArmor(1000)
   ply.HaloArmor = 80
   ply:SetMaxHealth(1250)
   ply:SetRunSpeed(345)
   ply:SetWalkSpeed(230)
   ply:SetJumpPower(350)
end
})
TEAM_HeathenCC = DarkRP.createJob("Viking Support", {
   color = Color(0, 0, 0, 255),
   model = {"models/sangheili/silentshadow/silent_shadow_player.mdl"},
   description = [[Viking Binary Support. A mercenary for hire.]],
   weapons = {"drc_plasma_rifle", "drc_energysword", "drc_t50srs", "weapon_cuff_admin", "cloaking-infinite", "weapon_bactanade", "climb_swep2", "stungun", "weapon_fists"},
   command = "VikingSup",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   hud = 6, 
   category = "Viking Binary",
   faction = 1,
   comms = {
      ["ONI"] = true,
      ["VIKING"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(1250)
   ply:SetArmor(1000)
   ply.HaloArmor = 80
   ply:SetMaxHealth(1250)
   ply:SetRunSpeed(345)
   ply:SetWalkSpeed(230)
   ply:SetJumpPower(350)
end
})
TEAM_JANITOR = DarkRP.createJob("UNICOM Janitor", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/offduty/male/offduty_eyecberg.mdl"},
   description = [[Someone let the monkeys in, so someone's gotta clean up after them.]],
   weapons = {"tfa_revival_m6g", "tfa_revival_ma5b", "climb_swep2", "weapon_fists"},
   command = "janitor",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC",
   faction = 1,
   comms = {
      ["UNSCMC"] = true,
      ["UNSCA"] = true,
      ["LIMA"] = true,
      ["CHIMERA"] = true,
   },
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor = 10
   ply:SetMaxHealth(200)
end
})
--[[---------------------------------------------------------------------------
Define which team joining players spawn into and what team you change to if demoted
---------------------------------------------------------------------------]]
GAMEMODE.DefaultTeam = TEAM_RECRUIT
--[[---------------------------------------------------------------------------
Define which teams belong to civil protection
Civil protection can set warrants, make people wanted and do some other police related things
---------------------------------------------------------------------------]]
GAMEMODE.CivilProtection = {
	[TEAM_POLICE] = true,
	[TEAM_CHIEF] = true,
	[TEAM_MAYOR] = true,
}
--[[---------------------------------------------------------------------------
Jobs that are hitmen (enables the hitman menu)
---------------------------------------------------------------------------]]
DarkRP.addHitmanTeam(TEAM_MOB)
