AddCSLuaFile()

sound.Add( {
	name = "drc.halo_eject_rifle",
	channel = CHAN_AUTO,
	volume = 0.42,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/eject0.wav", 
	"vuthakral/halo/weapons/eject1.wav", 
	"vuthakral/halo/weapons/eject2.wav", 
	"vuthakral/halo/weapons/eject3.wav" }
} )

sound.Add( {
	name = "drc.srs99d_bolt",
	channel = CHAN_AUTO,
	volume = 0.42,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99d/eject0.wav", 
	"vuthakral/halo/weapons/srs99d/eject1.wav", 
	"vuthakral/halo/weapons/srs99d/eject2.wav", 
	"vuthakral/halo/weapons/srs99d/eject3.wav", 
	"vuthakral/halo/weapons/srs99d/eject4.wav", 
	"vuthakral/halo/weapons/srs99d/eject5.wav", 
	"vuthakral/halo/weapons/srs99d/eject6.wav", 
	"vuthakral/halo/weapons/srs99d/eject7.wav" }
} )

sound.Add( {
	name = "drc.m7_fire",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 80,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m7smg/fire1.wav",
	"vuthakral/halo/weapons/m7smg/fire2.wav",
	"vuthakral/halo/weapons/m7smg/fire3.wav",
	"vuthakral/halo/weapons/m7smg/fire4.wav",
	"vuthakral/halo/weapons/m7smg/fire5.wav",
	"vuthakral/halo/weapons/m7smg/fire7.wav",
	"vuthakral/halo/weapons/m7smg/fire8.wav",
	"vuthakral/halo/weapons/m7smg/fire9.wav",
	"vuthakral/halo/weapons/m7smg/fire10.wav",
	"vuthakral/halo/weapons/m7smg/fire11.wav",
	"vuthakral/halo/weapons/m7smg/fire12.wav",
	"vuthakral/halo/weapons/m7smg/fire13.wav",
	"vuthakral/halo/weapons/m7smg/fire14.wav",
	"vuthakral/halo/weapons/m7smg/fire15.wav",
	"vuthakral/halo/weapons/m7smg/fire16.wav",
	"vuthakral/halo/weapons/m7smg/fire17.wav" }
} )

sound.Add( {
	name = "drc.m7_dryfire",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m7smg/dryfire.wav" }
} )

sound.Add( {
	name = "drc.m7_melee",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m7smg/melee1.wav",
	"vuthakral/halo/weapons/m7smg/melee2.wav",
	"vuthakral/halo/weapons/m7smg/melee3.wav" }
} )

sound.Add( {
	name = "drc.m7_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m7smg/ready1.wav",
	"vuthakral/halo/weapons/m7smg/ready2.wav",
	"vuthakral/halo/weapons/m7smg/ready3.wav" }
} )

sound.Add( {
	name = "drc.m7_reload1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 55,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m7smg/reload0-1.wav",
	"vuthakral/halo/weapons/m7smg/reload0-2.wav",
	"vuthakral/halo/weapons/m7smg/reload0-3.wav" }
} )

sound.Add( {
	name = "drc.m7_reload2",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 55,
	pitch = { 87.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m7smg/reload2-1.wav",
	"vuthakral/halo/weapons/m7smg/reload2-2.wav",
	"vuthakral/halo/weapons/m7smg/reload2-3.wav" }
} )

sound.Add( {
	name = "drc.m7_reload3",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 55,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m7smg/reload3-1.wav",
	"vuthakral/halo/weapons/m7smg/reload3-2.wav",
	"vuthakral/halo/weapons/m7smg/reload3-3.wav" }
} )

sound.Add( {
	name = "drc.pose1",
	channel = CHAN_AUTO,
	volume = 1,
	level = 110,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m7smg/pose0-0.wav",
	"vuthakral/halo/weapons/m7smg/pose0-1.wav",
	"vuthakral/halo/weapons/m7smg/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.pose2",
	channel = CHAN_AUTO,
	volume = 1,
	level = 110,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m7smg/pose1-0.wav",
	"vuthakral/halo/weapons/m7smg/pose1-1.wav",
	"vuthakral/halo/weapons/m7smg/pose1-3.wav" }
} )

sound.Add( {
	name = "drc.pr_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 98, 101 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plas_rifle_fire.wav" }
} )

sound.Add( {
	name = "drc.pr_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.79,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/plasmarifle/dist0.wav",
	"vuthakral/halo/weapons/plasmarifle/dist1.wav",
	"vuthakral/halo/weapons/plasmarifle/dist2.wav" }
} )

sound.Add( {
	name = "drc.pr_melee",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plasrifle_melee1.wav", 
	"vuthakral/halo/weapons/plasmarifle/plasrifle_melee2.wav", 
	"vuthakral/halo/weapons/plasmarifle/plasrifle_melee3.wav" }
} )

sound.Add( {
	name = "drc.plasmarifle_overheat_end",
	channel = CHAN_ITEM,
	volume = 0.72,
	level = 56,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plasrifle_oh_exit.wav" }
} )

sound.Add( {
	name = "drc.plasmarifle_overheat",
	channel = CHAN_ITEM,
	volume = 0.92,
	level = 76,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plasrifle_overheat.wav" }
} )

sound.Add( {
	name = "drc.plasmarifle_pose0",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plasrifle_posing_zero.wav" }
} )

sound.Add( {
	name = "drc.plasmarifle_pose1",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plasrifle_posing_one.wav" }
} )

sound.Add( {
	name = "drc.plasmarifle_ready",
	channel = CHAN_AUTO,
	volume = 1,
	level = 50,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plasrifle_ready.wav" }
} )

sound.Add( {
	name = "drc.repeater_ready",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 46,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/ready0.wav", 
	"vuthakral/halo/weapons/plasmarepeater/ready1.wav", 
	"vuthakral/halo/weapons/plasmarepeater/ready2.wav" }
} )

sound.Add( {
	name = "drc.repeater_draw_hero",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/readyhero0.wav", 
	"vuthakral/halo/weapons/plasmarepeater/readyhero1.wav", 
	"vuthakral/halo/weapons/plasmarepeater/readyhero2.wav" }
} )

sound.Add( {
	name = "drc.repeater_ventopen",
	channel = CHAN_VOICE,
	volume = 0.165,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/vent_open0.wav", 
	"vuthakral/halo/weapons/plasmarepeater/vent_open1.wav", 
	"vuthakral/halo/weapons/plasmarepeater/vent_open2.wav" }
} )

sound.Add( {
	name = "drc.repeater_ventclose",
	channel = CHAN_WEAPON,
	volume = 0.22,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/vent_close0.wav", 
	"vuthakral/halo/weapons/plasmarepeater/vent_close1.wav", 
	"vuthakral/halo/weapons/plasmarepeater/vent_close2.wav" }
} )

sound.Add( {
	name = "drc.repeater_ventstart",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/vent_in.wav", }
} )

sound.Add( {
	name = "drc.repeater_ventloop",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 100 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/vent_loop.wav", }
} )

sound.Add( {
	name = "drc.repeater_fire",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/fire0.wav", 
	"vuthakral/halo/weapons/plasmarepeater/fire1.wav", 
	"vuthakral/halo/weapons/plasmarepeater/fire2.wav" }
} )

sound.Add( {
	name = "drc.repeater_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.39,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/dist0.wav",
	"vuthakral/halo/weapons/plasmarepeater/dist1.wav",
	"vuthakral/halo/weapons/plasmarepeater/dist2.wav" }
} )

sound.Add( {
	name = "drc.repeater_poke",
	channel = CHAN_AUTO,
	volume = 0.82,
	level = 48,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/poke.wav" }
} )

sound.Add( {
	name = "drc.repeater_pose0",
	channel = CHAN_AUTO,
	volume = 1,
	level = 30,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/pose0.wav" }
} )

sound.Add( {
	name = "drc.repeater_pose1",
	channel = CHAN_AUTO,
	volume = 1,
	level = 30,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/pose1.wav" }
} )

sound.Add( {
	name = "drc.repeater_melee0",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/melee1-0.wav", 
	"vuthakral/halo/weapons/plasmarepeater/melee1-1.wav", 
	"vuthakral/halo/weapons/plasmarepeater/melee1-2.wav" }
} )

sound.Add( {
	name = "drc.repeater_melee1",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/plasmarepeater/melee2-0.wav", 
	"vuthakral/halo/weapons/plasmarepeater/melee2-1.wav", 
	"vuthakral/halo/weapons/plasmarepeater/melee2-2.wav" }
} )

sound.Add( {
	name = "drc.dmr_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/dmr/fire0.wav", 
	"vuthakral/halo/weapons/dmr/fire1.wav", 
	"vuthakral/halo/weapons/dmr/fire2.wav" }
} )

sound.Add( {
	name = "drc.dmr_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/dmr/dist0.wav",
	"vuthakral/halo/weapons/dmr/dist1.wav",
	"vuthakral/halo/weapons/dmr/dist2.wav" }
} )

sound.Add( {
	name = "drc.dmr_melee1",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/dmr/melee1-0.wav", 
	"vuthakral/halo/weapons/dmr/melee1-1.wav", 
	"vuthakral/halo/weapons/dmr/melee1-2.wav" }
} )

sound.Add( {
	name = "drc.dmr_melee2",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/dmr/melee2-0.wav", 
	"vuthakral/halo/weapons/dmr/melee2-1.wav", 
	"vuthakral/halo/weapons/dmr/melee2-2.wav" }
} )

sound.Add( {
	name = "drc.dmr_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/dmr/draw0.wav",
	"vuthakral/halo/weapons/dmr/draw1.wav" }
} )

sound.Add( {
	name = "drc.dmr_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/dmr/reloadfull0.wav",
	"vuthakral/halo/weapons/dmr/reloadfull1.wav",
	"vuthakral/halo/weapons/dmr/reloadfull2.wav" }
} )

sound.Add( {
	name = "drc.dmr_reloadempty",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/dmr/reload0.wav",
	"vuthakral/halo/weapons/dmr/reload1.wav",
	"vuthakral/halo/weapons/dmr/reload2.wav" }
} )

sound.Add( {
	name = "drc.srs99c_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/srs99c/ready.wav" }
} )

sound.Add( {
	name = "drc.srs99c_fire",
	channel = CHAN_WEAPON,
	volume = 0.92,
	level = 95,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/srs99c/fire0.wav",
	"vuthakral/halo/weapons/srs99c/fire1.wav",
	"vuthakral/halo/weapons/srs99c/fire2.wav",
	"vuthakral/halo/weapons/srs99c/fire3.wav" }
} )

sound.Add( {
	name = "drc.srs99c_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.79,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/sniper_dist0.wav",
	"vuthakral/halo/weapons/sniper_dist1.wav",
	"vuthakral/halo/weapons/sniper_dist2.wav",
	"vuthakral/halo/weapons/sniper_dist3.wav" }
} )

sound.Add( {
	name = "drc.srs99c_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 65,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/srs99c/reloadfull1.wav" }
} )

sound.Add( {
	name = "drc.srs99c_reloadempty",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 65,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/srs99c/reload.wav" }
} )

sound.Add( {
	name = "drc.srs99c_pose",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/srs99c/pose.wav" }
} )

sound.Add( {
	name = "drc.srs99c_melee",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/srs99c/melee0.wav",
	"vuthakral/halo/weapons/srs99c/melee1.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_fireloop",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/focusrifle/loop.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_impact",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 70,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/focusrifle/impact0.wav",
	"vuthakral/halo/weapons/focusrifle/impact1.wav",
	"vuthakral/halo/weapons/focusrifle/impact2.wav",
	"vuthakral/halo/weapons/focusrifle/impact3.wav",
	"vuthakral/halo/weapons/focusrifle/impact4.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_firein",
	channel = CHAN_WEAPON,
	volume = 0.79,
	level = 90,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/focusrifle/in.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_fireout",
	channel = CHAN_WEAPON,
	volume = 0.79,
	level = 90,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/focusrifle/out.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_overheated",
	channel = CHAN_VOICE2,
	volume = 0.59,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/focusrifle/overheated.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_lockup",
	channel = CHAN_BODY,
	volume = 0.59,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/focusrifle/lockup_overheat.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_oh_in",
	channel = CHAN_VOICE,
	volume = 0.72,
	level = 80,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/focusrifle/oh0.wav",
	"vuthakral/halo/weapons/focusrifle/oh1.wav",
	"vuthakral/halo/weapons/focusrifle/oh2.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_oh_out",
	channel = CHAN_VOICE,
	volume = 0.72,
	level = 80,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/focusrifle/oh_exit0.wav",
	"vuthakral/halo/weapons/focusrifle/oh_exit1.wav",
	"vuthakral/halo/weapons/focusrifle/oh_exit2.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_ready",
	channel = CHAN_AUTO,
	volume = 0.62,
	level = 80,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/focusrifle/ready0.wav",
	"vuthakral/halo/weapons/focusrifle/ready1.wav",
	"vuthakral/halo/weapons/focusrifle/ready2.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_melee",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/focusrifle/melee0.wav",
	"vuthakral/halo/weapons/focusrifle/melee1.wav",
	"vuthakral/halo/weapons/focusrifle/melee2.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_pose0",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/focusrifle/pose0-1.wav",
	"vuthakral/halo/weapons/focusrifle/pose0-2.wav",
	"vuthakral/halo/weapons/focusrifle/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.focusrifle_pose1",
	channel = CHAN_BODY,
	volume = 1,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/focusrifle/pose2-0.wav",
	"vuthakral/halo/weapons/focusrifle/pose2-1.wav",
	"vuthakral/halo/weapons/focusrifle/pose2-2.wav" }
} )

sound.Add( {
	name = "drc.br55_h2a_fire",
	channel = CHAN_AUTO,
	volume = 0.59,
	level = 80,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/br55/fire0.wav",
	"vuthakral/halo/weapons/br55/fire1.wav",
	"vuthakral/halo/weapons/br55/fire2.wav",
	"vuthakral/halo/weapons/br55/fire3.wav",
	"vuthakral/halo/weapons/br55/fire4.wav",
	"vuthakral/halo/weapons/br55/fire5.wav",
	"vuthakral/halo/weapons/br55/fire6.wav" }
} )

sound.Add( {
	name = "drc.br55hb_fire",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 80,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/br55hb/fire0.wav",
	"vuthakral/halo/weapons/br55hb/fire1.wav",
	"vuthakral/halo/weapons/br55hb/fire2.wav",
	"vuthakral/halo/weapons/br55hb/fire3.wav",
	"vuthakral/halo/weapons/br55hb/fire4.wav",
	"vuthakral/halo/weapons/br55hb/fire5.wav",
	"vuthakral/halo/weapons/br55hb/fire6.wav" }
} )

sound.Add( {
	name = "drc.br55hb_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.39,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/br55hb/dist0.wav",
	"vuthakral/halo/weapons/br55hb/dist1.wav",
	"vuthakral/halo/weapons/br55hb/dist2.wav",
	"vuthakral/halo/weapons/br55hb/dist3.wav" }
} )

sound.Add( {
	name = "drc.br55hb_melee1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/br55hb/melee0-1.wav",
	"vuthakral/halo/weapons/br55hb/melee1-1.wav",
	"vuthakral/halo/weapons/br55hb/melee1-2.wav" }
} )

sound.Add( {
	name = "drc.br55hb_melee0",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/br55hb/melee0-1.wav",
	"vuthakral/halo/weapons/br55hb/melee0-1.wav",
	"vuthakral/halo/weapons/br55hb/melee0-2.wav" }
} )

sound.Add( {
	name = "drc.br55hb_reload",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/br55hb/reload0.wav",
	"vuthakral/halo/weapons/br55hb/reload0.wav" }
} )

sound.Add( {
	name = "drc.br55hb_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/br55hb/ready0.wav",
	"vuthakral/halo/weapons/br55hb/ready1.wav" }
} )

sound.Add( {
	name = "drc.br55hb_pose0",
	channel = CHAN_BODY,
	volume = 0.6,
	level = 57,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/br55hb/pose0-1.wav",
	"vuthakral/halo/weapons/br55hb/pose0-2.wav",
	"vuthakral/halo/weapons/br55hb/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.br55hb_pose1",
	channel = CHAN_BODY,
	volume = 0.6,
	level = 57,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/br55hb/pose1-0.wav",
	"vuthakral/halo/weapons/br55hb/pose1-1.wav",
	"vuthakral/halo/weapons/br55hb/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_fire",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 80,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma5c/fire0.wav",
	"vuthakral/halo/weapons/ma5c/fire1.wav",
	"vuthakral/halo/weapons/ma5c/fire2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 140,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma5c/dist0.wav",
	"vuthakral/halo/weapons/ma5c/dist1.wav",
	"vuthakral/halo/weapons/ma5c/dist2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_pose0",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma5c/pose1-0.wav",
	"vuthakral/halo/weapons/ma5c/pose1-1.wav",
	"vuthakral/halo/weapons/ma5c/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma5c/pose2-0.wav",
	"vuthakral/halo/weapons/ma5c/pose2-1.wav",
	"vuthakral/halo/weapons/ma5c/pose2-2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma5c/reload0.wav",
	"vuthakral/halo/weapons/ma5c/reload1.wav",
	"vuthakral/halo/weapons/ma5c/reload2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/ma5c/ready0.wav",
	"vuthakral/halo/weapons/ma5c/ready1.wav",
	"vuthakral/halo/weapons/ma5c/ready2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_melee0",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma5c/melee1-0.wav",
	"vuthakral/halo/weapons/ma5c/melee1-1.wav",
	"vuthakral/halo/weapons/ma5c/melee1-2.wav" }
} )

sound.Add( {
	name = "drc.ma5c_melee1",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma5c/melee2-1.wav",
	"vuthakral/halo/weapons/ma5c/melee2-2.wav" }
} )

sound.Add( {
	name = "drc.m6d_melee",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 50,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m6d/whip.wav",
	"vuthakral/halo/weapons/m6d/whip2.wav" }
} )

sound.Add( {
	name = "drc.m6d_ready",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 50,
	pitch = { 99.5, 100.5},
	sound = { "vuthakral/halo/weapons/m6d/ready.wav" }
} )

sound.Add( {
	name = "drc.m6d_pose",
	channel = CHAN_WEAPON,
	volume = 0.8,
	level = 47,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m6d/pose.wav" }
} )

sound.Add( {
	name = "drc.m6d_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m6d/reload.wav" }
} )

sound.Add( {
	name = "drc.m6d_fire",
	channel = CHAN_WEAPON,
	volume = 0.89,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m6d/fire.wav" }
} )

sound.Add( {
	name = "drc.m90_shellinsert",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m90/loop0.wav",
	"vuthakral/halo/weapons/m90/loop1.wav",
	"vuthakral/halo/weapons/m90/loop2.wav",
	"vuthakral/halo/weapons/m90/loop3.wav",
	"vuthakral/halo/weapons/m90/loop4.wav",
	"vuthakral/halo/weapons/m90/loop5.wav",
	"vuthakral/halo/weapons/m90/loop6.wav",
	"vuthakral/halo/weapons/m90/loop7.wav" }
} )

sound.Add( {
	name = "drc.m90_fire",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90/fire0.wav",
	"vuthakral/halo/weapons/m90/fire1.wav",
	"vuthakral/halo/weapons/m90/fire2.wav" }
} )

sound.Add( {
	name = "drc.m90_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.39,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90/dist0.wav",
	"vuthakral/halo/weapons/m90/dist1.wav",
	"vuthakral/halo/weapons/m90/dist2.wav" }
} )

sound.Add( {
	name = "drc.m90_pose1",
	channel = CHAN_AUTO,
	volume = 0.8,
	level = 47,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90/pose1-0.wav",
	"vuthakral/halo/weapons/m90/pose1-1.wav",
	"vuthakral/halo/weapons/m90/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.m90_pose1-66",
	channel = CHAN_AUTO,
	volume = 0.8,
	level = 47,
	pitch = { 69, 69 },
	sound = { "vuthakral/halo/weapons/m90/pose1-0.wav",
	"vuthakral/halo/weapons/m90/pose1-1.wav",
	"vuthakral/halo/weapons/m90/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.m90_pose2",
	channel = CHAN_AUTO,
	volume = 0.8,
	level = 47,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90/pose2-0.wav",
	"vuthakral/halo/weapons/m90/pose2-1.wav",
	"vuthakral/halo/weapons/m90/pose2-2.wav" }
} )

sound.Add( {
	name = "drc.m90_ready",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m90/ready0.wav",
	"vuthakral/halo/weapons/m90/ready1.wav",
	"vuthakral/halo/weapons/m90/ready2.wav" }
} )

sound.Add( {
	name = "drc.m90_melee1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90/melee1-0.wav",
	"vuthakral/halo/weapons/m90/melee1-1.wav",
	"vuthakral/halo/weapons/m90/melee1-2.wav" }
} )

sound.Add( {
	name = "drc.m90_melee2",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90/melee2-0.wav",
	"vuthakral/halo/weapons/m90/melee2-1.wav",
	"vuthakral/halo/weapons/m90/melee2-2.wav" }
} )

sound.Add( {
	name = "drc.m90_reloadexit",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90/exit.wav" }
} )

sound.Add( {
	name = "drc.m90_firepump",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90/pump.wav" }
} )

sound.Add( {
	name = "drc.m7s_fire",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m7s/fire0.wav",
	"vuthakral/halo/weapons/m7s/fire1.wav",
	"vuthakral/halo/weapons/m7s/fire2.wav" }
} )

sound.Add( {
	name = "drc.ma37_fire",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 80,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma37/fire0.wav",
	"vuthakral/halo/weapons/ma37/fire1.wav",
	"vuthakral/halo/weapons/ma37/fire2.wav" }
} )

sound.Add( {
	name = "drc.ma37_pose0",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma37/pose1-0.wav",
	"vuthakral/halo/weapons/ma37/pose1-1.wav",
	"vuthakral/halo/weapons/ma37/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.ma37_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma37/pose2-0.wav",
	"vuthakral/halo/weapons/ma37/pose2-1.wav",
	"vuthakral/halo/weapons/ma37/pose2-2.wav" }
} )

sound.Add( {
	name = "drc.ma37_reloadfull",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma37/rl_full0.wav",
	"vuthakral/halo/weapons/ma37/rl_full1.wav",
	"vuthakral/halo/weapons/ma37/rl_full2.wav" }
} )

sound.Add( {
	name = "drc.ma37_reloadempty",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma37/rl_empty0.wav",
	"vuthakral/halo/weapons/ma37/rl_empty1.wav",
	"vuthakral/halo/weapons/ma37/rl_empty2.wav" }
} )

sound.Add( {
	name = "drc.ma37_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 50,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/ma37/ready0.wav",
	"vuthakral/halo/weapons/ma37/ready1.wav",
	"vuthakral/halo/weapons/ma37/ready2.wav" }
} )

sound.Add( {
	name = "drc.ma37_ready_hero",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/ma37/ready_hero0.wav",
	"vuthakral/halo/weapons/ma37/ready_hero1.wav",
	"vuthakral/halo/weapons/ma37/ready_hero2.wav" }
} )

sound.Add( {
	name = "drc.ma37_melee0",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma37/melee1-0.wav",
	"vuthakral/halo/weapons/ma37/melee1-1.wav",
	"vuthakral/halo/weapons/ma37/melee1-2.wav" }
} )

sound.Add( {
	name = "drc.ma37_melee1",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma37/melee2-1.wav",
	"vuthakral/halo/weapons/ma37/melee2-2.wav" }
} )


sound.Add( {
	name = "drc.ma5b_melee",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 50,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma5b/melee.wav" }
} )

sound.Add( {
	name = "drc.ma5b_ready",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 50,
	pitch = { 99.5, 100.5},
	sound = { "vuthakral/halo/weapons/ma5b/ready.wav" }
} )

sound.Add( {
	name = "drc.ma5b_pose",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 47,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma5b/pose.wav" }
} )

sound.Add( {
	name = "drc.ma5b_reload0",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma5b/reload.wav" }
} )

sound.Add( {
	name = "drc.ma5b_reload1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma5b/reload.wav" }
} )

sound.Add( {
	name = "drc.ma5b_fire",
	channel = CHAN_WEAPON,
	volume = 0.89,
	level = 80,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/ma5b/fire0.wav",
	"vuthakral/halo/weapons/ma5b/fire1.wav",
	"vuthakral/halo/weapons/ma5b/fire3.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_fire",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/SPNKr/fire0.wav",
	"vuthakral/halo/weapons/SPNKr/fire1.wav",
	"vuthakral/halo/weapons/SPNKr/fire2.wav",
	"vuthakral/halo/weapons/SPNKr/fire3.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.79,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/SPNKr/dist0.wav",
	"vuthakral/halo/weapons/SPNKr/dist1.wav",
	"vuthakral/halo/weapons/SPNKr/dist2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_fp_rotate",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 140,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/SPNKr/fp_rl_fire1.wav",
	"vuthakral/halo/weapons/SPNKr/fp_rl_fire2.wav",
	"vuthakral/halo/weapons/SPNKr/fp_rl_fire3.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 57,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/SPNKr/pose1-0.wav",
	"vuthakral/halo/weapons/SPNKr/pose1-1.wav",
	"vuthakral/halo/weapons/SPNKr/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_pose2",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 57,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/SPNKr/pose2-0.wav",
	"vuthakral/halo/weapons/SPNKr/pose2-1.wav",
	"vuthakral/halo/weapons/SPNKr/pose2-2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_pose3",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 57,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/SPNKr/pose3-0.wav",
	"vuthakral/halo/weapons/SPNKr/pose3-1.wav",
	"vuthakral/halo/weapons/SPNKr/pose3-2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_reload1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 93,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/SPNKr/reload0.wav",
	"vuthakral/halo/weapons/SPNKr/reload1.wav",
	"vuthakral/halo/weapons/SPNKr/reload2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 60,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/SPNKr/ready0.wav",
	"vuthakral/halo/weapons/SPNKr/ready1.wav",
	"vuthakral/halo/weapons/SPNKr/ready2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_ready_hero",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 60,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/SPNKr/ready_hero0.wav",
	"vuthakral/halo/weapons/SPNKr/ready_hero1.wav",
	"vuthakral/halo/weapons/SPNKr/ready_hero2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_melee0",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 60,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/SPNKr/melee0.wav",
	"vuthakral/halo/weapons/SPNKr/melee1.wav",
	"vuthakral/halo/weapons/SPNKr/melee2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_melee1",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/SPNKr/melee2-1.wav",
	"vuthakral/halo/weapons/SPNKr/melee2-2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_rocket_explode",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/SPNKr/explode0.wav",
	"vuthakral/halo/weapons/SPNKr/explode1.wav",
	"vuthakral/halo/weapons/SPNKr/explode2.wav",
	"vuthakral/halo/weapons/SPNKr/explode3.wav",
	"vuthakral/halo/weapons/SPNKr/explode4.wav",
	"vuthakral/halo/weapons/SPNKr/explode5.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_rocket_explode_dist",
	channel = CHAN_AUTO,
	volume = 1,
	level = 140,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/SPNKr/explode_dist0.wav",
	"vuthakral/halo/weapons/SPNKr/explode_dist1.wav",
	"vuthakral/halo/weapons/SPNKr/explode_dist2.wav" }
} )

sound.Add( {
	name = "drc.SPNKr_rocket_flight",
	channel = CHAN_AUTO,
	volume = 0.82,
	level = 85,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/SPNKr/rocketloop.wav" }
} )

sound.Add( {
	name = "drc.needlerifle_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/needlerifle/fire0.wav", 
	"vuthakral/halo/weapons/needlerifle/fire1.wav", 
	"vuthakral/halo/weapons/needlerifle/fire2.wav" }
} )

sound.Add( {
	name = "drc.needlerifle_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.39,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/needlerifle/dist0.wav",
	"vuthakral/halo/weapons/needlerifle/dist1.wav",
	"vuthakral/halo/weapons/needlerifle/dist2.wav" }
} )

sound.Add( {
	name = "drc.needlerifle_melee",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/needlerifle/melee0-1.wav", 
	"vuthakral/halo/weapons/needlerifle/melee0-2.wav", 
	"vuthakral/halo/weapons/needlerifle/melee0-3.wav" }
} )

sound.Add( {
	name = "drc.needlerifle_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/needlerifle/ready0.wav",
	"vuthakral/halo/weapons/needlerifle/ready1.wav",
	"vuthakral/halo/weapons/needlerifle/ready2.wav" }
} )

sound.Add( {
	name = "drc.needlerifle_reload1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/needlerifle/reload0.wav",
	"vuthakral/halo/weapons/needlerifle/reload1.wav",
	"vuthakral/halo/weapons/needlerifle/reload2.wav" }
} )

sound.Add( {
	name = "drc.needlerifle_pose0",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/needlerifle/pose1-0.wav",
	"vuthakral/halo/weapons/needlerifle/pose1-1.wav",
	"vuthakral/halo/weapons/needlerifle/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.needlerifle_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/needlerifle/pose2-0.wav",
	"vuthakral/halo/weapons/needlerifle/pose2-1.wav",
	"vuthakral/halo/weapons/needlerifle/pose2-2.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 90,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/bruteshot/fire0.wav", 
	"vuthakral/halo/weapons/bruteshot/fire1.wav", 
	"vuthakral/halo/weapons/bruteshot/fire2.wav",
	"vuthakral/halo/weapons/bruteshot/fire3.wav",
	"vuthakral/halo/weapons/bruteshot/fire4.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_grenade_flight",
	channel = CHAN_AUTO,
	volume = 0.82,
	level = 85,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/bruteshot/grenade_loop.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_fire_dist",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 120,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/bruteshot/fire_dist0.wav", 
	"vuthakral/halo/weapons/bruteshot/fire_dist1.wav", 
	"vuthakral/halo/weapons/bruteshot/fire_dist2.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_explode",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/bruteshot/explode0.wav",
	"vuthakral/halo/weapons/bruteshot/explode1.wav",
	"vuthakral/halo/weapons/bruteshot/explode2.wav",
	"vuthakral/halo/weapons/bruteshot/explode3.wav",
	"vuthakral/halo/weapons/bruteshot/explode4.wav",
	"vuthakral/halo/weapons/bruteshot/explode5.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_explode_dist",
	channel = CHAN_AUTO,
	volume = 0.7,
	level = 120,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/bruteshot/explode_dist0.wav",
	"vuthakral/halo/weapons/bruteshot/explode_dist1.wav",
	"vuthakral/halo/weapons/bruteshot/explode_dist2.wav",
	"vuthakral/halo/weapons/bruteshot/explode_dist3.wav" }
} )


sound.Add( {
	name = "drc.bruteshot_melee",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/bruteshot/melee0.wav", 
	"vuthakral/halo/weapons/bruteshot/melee1.wav", 
	"vuthakral/halo/weapons/bruteshot/melee2.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/bruteshot/ready0.wav",
	"vuthakral/halo/weapons/bruteshot/ready1.wav",
	"vuthakral/halo/weapons/bruteshot/ready2.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/bruteshot/reload0.wav",
	"vuthakral/halo/weapons/bruteshot/reload1.wav",
	"vuthakral/halo/weapons/bruteshot/reload2.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/bruteshot/pose1-0.wav",
	"vuthakral/halo/weapons/bruteshot/pose1-1.wav",
	"vuthakral/halo/weapons/bruteshot/pose1-2.wav" }
} )

sound.Add( {
	name = "drc.bruteshot_pose2",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/bruteshot/pose2-0.wav",
	"vuthakral/halo/weapons/bruteshot/pose2-1.wav",
	"vuthakral/halo/weapons/bruteshot/pose2-2.wav" }
} )

sound.Add( {
	name = "drc.spiker_ammo",
	channel = CHAN_AUTO,
	volume = 1,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/spiker/spike_ammo1.wav",
	"vuthakral/halo/weapons/spiker/spike_ammo2.wav",
	"vuthakral/halo/weapons/spiker/spike_ammo3.wav" }
} )


sound.Add( {
	name = "drc.spiker_fire",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 84,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/spiker/fire0.wav", 
	"vuthakral/halo/weapons/spiker/fire1.wav", 
	"vuthakral/halo/weapons/spiker/fire2.wav", 
	"vuthakral/halo/weapons/spiker/fire3.wav", 
	"vuthakral/halo/weapons/spiker/fire4.wav", 
	"vuthakral/halo/weapons/spiker/fire5.wav" }
} )

sound.Add( {
	name = "drc.spiker_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.79,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/spiker/dist0.wav",
	"vuthakral/halo/weapons/spiker/dist1.wav",
	"vuthakral/halo/weapons/spiker/dist2.wav" }
} )

sound.Add( {
	name = "drc.spiker_melee",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/spiker/melee0.wav", 
	"vuthakral/halo/weapons/spiker/melee1.wav", 
	"vuthakral/halo/weapons/spiker/melee2.wav" }
} )

sound.Add( {
	name = "drc.spiker_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/spiker/ready0.wav",
	"vuthakral/halo/weapons/spiker/ready1.wav",
	"vuthakral/halo/weapons/spiker/ready2.wav" }
} )

sound.Add( {
	name = "drc.spiker_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/spiker/reload0.wav",
	"vuthakral/halo/weapons/spiker/reload1.wav",
	"vuthakral/halo/weapons/spiker/reload2.wav" }
} )

sound.Add( {
	name = "drc.spiker_pose",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/spiker/pose0.wav",
	"vuthakral/halo/weapons/spiker/pose1.wav",
	"vuthakral/halo/weapons/spiker/pose2.wav" }
} )

sound.Add( {
	name = "drc.m6s_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6s/fire0.wav", 
	"vuthakral/halo/weapons/m6s/fire1.wav", 
	"vuthakral/halo/weapons/m6s/fire2.wav", 
	"vuthakral/halo/weapons/m6s/fire3.wav" }
} )

sound.Add( {
	name = "drc.m6c_fire",
	channel = CHAN_WEAPON,
	volume = 0.76,
	level = 84,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6c/fire0.wav", 
	"vuthakral/halo/weapons/m6c/fire1.wav", 
	"vuthakral/halo/weapons/m6c/fire2.wav", 
	"vuthakral/halo/weapons/m6c/fire3.wav" }
} )

sound.Add( {
	name = "drc.m6c_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m6c/fire_dist0.wav",
	"vuthakral/halo/weapons/m6c/fire_dist1.wav",
	"vuthakral/halo/weapons/m6c/fire_dist3.wav",
	"vuthakral/halo/weapons/m6c/fire_dist4.wav" }
} )

sound.Add( {
	name = "drc.m6g_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6g/fire0.wav", 
	"vuthakral/halo/weapons/m6g/fire1.wav", 
	"vuthakral/halo/weapons/m6g/fire2.wav", 
	"vuthakral/halo/weapons/m6g/fire3.wav" }
} )

sound.Add( {
	name = "drc.m6gr_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6gr/fire0.wav", 
	"vuthakral/halo/weapons/m6gr/fire1.wav", 
	"vuthakral/halo/weapons/m6gr/fire2.wav", 
	"vuthakral/halo/weapons/m6gr/fire3.wav" }
} )

sound.Add( {
	name = "drc.magnum_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m6g/dist0.wav",
	"vuthakral/halo/weapons/m6g/dist1.wav",
	"vuthakral/halo/weapons/m6g/dist2.wav" }
} )

sound.Add( {
	name = "drc.m6gr_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m6gr/reload_full0.wav",
	"vuthakral/halo/weapons/m6gr/reload_full1.wav",
	"vuthakral/halo/weapons/m6gr/reload_full2.wav" }
} )

sound.Add( {
	name = "drc.m6gr_ready",
	channel = CHAN_AUTO,
	volume = 0.39,
	level = 50,
	pitch = { 99.5, 100.5},
	sound = { "vuthakral/halo/weapons/m6gr/ready0.wav",
	"vuthakral/halo/weapons/m6gr/ready1.wav" }
} )

sound.Add( {
	name = "drc.m6gr_melee",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 40,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/m6gr/melee0.wav",
	"vuthakral/halo/weapons/m6gr/melee1.wav",
	"vuthakral/halo/weapons/m6gr/melee2.wav" }
} )

sound.Add( {
	name = "drc.automag_ready",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6s/ready0.wav", 
	"vuthakral/halo/weapons/m6s/ready1.wav" }
} )

sound.Add( {
	name = "drc.automag_reload",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6s/reload0.wav", 
	"vuthakral/halo/weapons/m6s/reload1.wav",
	"vuthakral/halo/weapons/m6s/reload2.wav" }
} )

sound.Add( {
	name = "drc.automag_melee0",
	channel = CHAN_AUTO,
	volume = 0.32,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6s/melee0.wav", 
	"vuthakral/halo/weapons/m6s/melee1.wav",
	"vuthakral/halo/weapons/m6s/melee2.wav" }
} )

sound.Add( {
	name = "drc.automag_pose0",
	channel = CHAN_AUTO,
	volume = 0.22,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m6s/pose0.wav", 
	"vuthakral/halo/weapons/m6s/pose1.wav",
	"vuthakral/halo/weapons/m6s/pose2.wav",
	"vuthakral/halo/weapons/m6s/pose3.wav" }
} )

sound.Add( {
	name = "drc.srs99d_fire",
	channel = CHAN_WEAPON,
	volume = 1,
	level = 92,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99d/fire0.wav", 
	"vuthakral/halo/weapons/srs99d/fire1.wav", 
	"vuthakral/halo/weapons/srs99d/fire2.wav", 
	"vuthakral/halo/weapons/srs99d/fire3.wav" }
} )

sound.Add( {
	name = "drc.srs99d_ready",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99d/ready0.wav", 
	"vuthakral/halo/weapons/srs99d/ready1.wav" }
} )

sound.Add( {
	name = "drc.srs99d_reload",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99d/reload0.wav", 
	"vuthakral/halo/weapons/srs99d/reload1.wav",
	"vuthakral/halo/weapons/srs99d/reload2.wav" }
} )

sound.Add( {
	name = "drc.srs99d_pose0",
	channel = CHAN_AUTO,
	volume = 0.22,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99d/pose0-1.wav", 
	"vuthakral/halo/weapons/srs99d/pose0-2.wav",
	"vuthakral/halo/weapons/srs99d/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.srs99d_pose1",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99d/pose1-1.wav", 
	"vuthakral/halo/weapons/srs99d/pose1-2.wav",
	"vuthakral/halo/weapons/srs99d/pose1-3.wav" }
} )

sound.Add( {
	name = "drc.srs99d_melee0",
	channel = CHAN_AUTO,
	volume = 0.32,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99d/melee0.wav", 
	"vuthakral/halo/weapons/srs99d/melee1.wav",
	"vuthakral/halo/weapons/srs99d/melee2.wav" }
} )

sound.Add( {
	name = "drc.ma5d_fire",
	channel = CHAN_AUTO,
	volume = 1.0,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/ma5d/fire0.wav",
	"vuthakral/halo/weapons/ma5d/fire1.wav",
	"vuthakral/halo/weapons/ma5d/fire2.wav",
	"vuthakral/halo/weapons/ma5d/fire3.wav",
	"vuthakral/halo/weapons/ma5d/fire4.wav",
	"vuthakral/halo/weapons/ma5d/fire5.wav",
	"vuthakral/halo/weapons/ma5d/fire6.wav",
	"vuthakral/halo/weapons/ma5d/fire7.wav" }
} )

sound.Add( {
	name = "drc.ma5d_melee0",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 60,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/ma5d/melee0.wav",
	"vuthakral/halo/weapons/ma5d/melee1.wav",
	"vuthakral/halo/weapons/ma5d/melee2.wav",
	"vuthakral/halo/weapons/ma5d/melee3.wav",
	"vuthakral/halo/weapons/ma5d/melee4.wav" }
} )

sound.Add( {
	name = "drc.ma5d_reload",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 58,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/ma5d/reload0.wav",
	"vuthakral/halo/weapons/ma5d/reload1.wav",
	"vuthakral/halo/weapons/ma5d/reload2.wav",
	"vuthakral/halo/weapons/ma5d/reload3.wav",
	"vuthakral/halo/weapons/ma5d/reload4.wav" }
} )

sound.Add( {
	name = "drc.ma5d_ready",
	channel = CHAN_AUTO,
	volume = 0.99,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/ma5d/ready0.wav",
	"vuthakral/halo/weapons/ma5d/ready1.wav",
	"vuthakral/halo/weapons/ma5d/ready2.wav",
	"vuthakral/halo/weapons/ma5d/ready4.wav",
	"vuthakral/halo/weapons/ma5d/ready5.wav" }
} )

sound.Add( {
	name = "drc.pp_ready",
	channel = CHAN_AUTO,
	volume = 1,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/pp/ready0.wav",
	"vuthakral/halo/weapons/pp/ready1.wav",
	"vuthakral/halo/weapons/pp/ready2.wav",
	"vuthakral/halo/weapons/pp/ready3.wav",
	"vuthakral/halo/weapons/pp/ready4.wav",
	"vuthakral/halo/weapons/pp/ready5.wav" }
} )

sound.Add( {
	name = "drc.pp_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 99.5, 106 },
	sound = { "vuthakral/halo/weapons/pp/fire0.wav",
	"vuthakral/halo/weapons/pp/fire1.wav",
	"vuthakral/halo/weapons/pp/fire2.wav" }
} )

sound.Add( {
	name = "drc.pp_fireOC",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 90,
	pitch = { 99.5, 106 },
	sound = { "vuthakral/halo/weapons/pp/charge_fire_reach.wav" }
} )

sound.Add( {
	name = "drc.pp_charge",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/loop.wav" }
} )

sound.Add( {
	name = "drc.pp_pose0",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/pose0-1.wav",
	"vuthakral/halo/weapons/pp/pose0-2.wav",
	"vuthakral/halo/weapons/pp/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.pp_pose1",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/pose1-1.wav",
	"vuthakral/halo/weapons/pp/pose1-2.wav",
	"vuthakral/halo/weapons/pp/pose1-3.wav" }
} )

sound.Add( {
	name = "drc.pp_melee0",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/melee0-1.wav",
	"vuthakral/halo/weapons/pp/melee0-2.wav",
	"vuthakral/halo/weapons/pp/melee0-3.wav" }
} )

sound.Add( {
	name = "drc.pp_melee1",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/melee2-1.wav",
	"vuthakral/halo/weapons/pp/melee2-2.wav",
	"vuthakral/halo/weapons/pp/melee2-3.wav" }
} )

sound.Add( {
	name = "drc.pp_oh",
	channel = CHAN_ITEM,
	volume = 0.42,
	level = 56,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/overheat.wav" }
} )

sound.Add( {
	name = "drc.pp_oh_exit",
	channel = CHAN_ITEM,
	volume = 0.72,
	level = 56,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/exit0.wav",
	"vuthakral/halo/weapons/pp/exit1.wav",
	"vuthakral/halo/weapons/pp/exit2.wav" }
} )

sound.Add( {
	name = "drc.pp_stun",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 90,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/oc_hit0.wav",
	"vuthakral/halo/weapons/pp/oc_hit1.wav",
	"vuthakral/halo/weapons/pp/oc_hit2.wav" }
} )

sound.Add( {
	name = "drc.plasma_empty",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/pp/dryfire0.wav",
	"vuthakral/halo/weapons/pp/dryfire1.wav",
	"vuthakral/halo/weapons/pp/dryfire2.wav",
	"vuthakral/halo/weapons/pp/dryfire3.wav",
	"vuthakral/halo/weapons/pp/dryfire4.wav" }
} )

sound.Add( {
	name = "drc.halo_mag_empty",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/click.wav" }
} )

sound.Add( {
	name = "drc.plasma_impact_reach",
	channel = CHAN_AUTO,
	volume = 1,
	level = 70,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/plas_impact_reach0.wav",
	"vuthakral/halo/weapons/plas_impact_reach1.wav",
	"vuthakral/halo/weapons/plas_impact_reach2.wav",
	"vuthakral/halo/weapons/plas_impact_reach3.wav",
	"vuthakral/halo/weapons/plas_impact_reach4.wav" }
} )

sound.Add( {
	name = "drc.plasma_impact_reach_quiet",
	channel = CHAN_AUTO,
	volume = 1,
	level = 65,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/plas_impact_reach0.wav",
	"vuthakral/halo/weapons/plas_impact_reach1.wav",
	"vuthakral/halo/weapons/plas_impact_reach2.wav",
	"vuthakral/halo/weapons/plas_impact_reach3.wav",
	"vuthakral/halo/weapons/plas_impact_reach4.wav" }
} )

sound.Add( {
	name = "drc.spike_impact",
	channel = CHAN_AUTO,
	volume = 1,
	level = 60,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/spiker/spike_impact0.wav",
	"vuthakral/halo/weapons/spiker/spike_impact1.wav",
	"vuthakral/halo/weapons/spiker/spike_impact2.wav",
	"vuthakral/halo/weapons/spiker/spike_impact3.wav",
	"vuthakral/halo/weapons/spiker/spike_impact4.wav" }
} )

sound.Add( {
	name = "drc.gnr_ready",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 46,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/gnr/ready0.wav", 
	"vuthakral/halo/weapons/gnr/ready1.wav", 
	"vuthakral/halo/weapons/gnr/ready2.wav" }
} )

sound.Add( {
	name = "drc.gnr_melee0",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/gnr/melee0a.wav",
	"vuthakral/halo/weapons/gnr/melee0b.wav",
	"vuthakral/halo/weapons/gnr/melee0c.wav" }
} )

sound.Add( {
	name = "drc.gnr_pose0",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/gnr/pose0a.wav",
	"vuthakral/halo/weapons/gnr/pose0b.wav",
	"vuthakral/halo/weapons/gnr/pose0c.wav" }
} )

sound.Add( {
	name = "drc.gnr_pose1",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/gnr/pose1a.wav",
	"vuthakral/halo/weapons/gnr/pose1b.wav",
	"vuthakral/halo/weapons/gnr/pose1c.wav" }
} )

sound.Add( {
	name = "drc.gnr_pose2",
	channel = CHAN_BODY,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/gnr/pose2a.wav",
	"vuthakral/halo/weapons/gnr/pose2b.wav",
	"vuthakral/halo/weapons/gnr/pose2c.wav" }
} )

sound.Add( {
	name = "drc.gnr_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 90,
	pitch = { 99.5, 106 },
	sound = { "vuthakral/halo/weapons/gnr/fire0.wav",
	"vuthakral/halo/weapons/gnr/fire1.wav",
	"vuthakral/halo/weapons/gnr/fire2.wav" }
} )

sound.Add( {
	name = "drc.gnr_fire_dist",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 90,
	pitch = { 99.5, 106 },
	sound = { "vuthakral/halo/weapons/gnr/fire_dist0.wav",
	"vuthakral/halo/weapons/gnr/fire_dist1.wav",
	"vuthakral/halo/weapons/gnr/fire_dist2.wav" }
} )

sound.Add( {
	name = "drc.gnr_fireanim",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/gnr/fire_anim0a.wav",
	"vuthakral/halo/weapons/gnr/fire_anim0b.wav",
	"vuthakral/halo/weapons/gnr/fire_anim0c.wav" }
} )

sound.Add( {
	name = "drc.gnr_charge",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 45,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/gnr/charge_in.wav" }
} )

sound.Add( {
	name = "drc.gnr_overheat",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/gnr/overheat0.wav",
	"vuthakral/halo/weapons/gnr/overheat1.wav",
	"vuthakral/halo/weapons/gnr/overheat2.wav" }
} )

sound.Add( {
	name = "drc.m139_explode",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m139/explode0.wav",
	"vuthakral/halo/weapons/m139/explode1.wav",
	"vuthakral/halo/weapons/m139/explode2.wav" }
} )

sound.Add( {
	name = "drc.halofrag_dist",
	channel = CHAN_AUTO,
	volume = 1,
	level = 140,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/frag/explode_dist0.wav",
	"vuthakral/halo/weapons/frag/explode_dist1.wav",
	"vuthakral/halo/weapons/frag/explode_dist2.wav",
	"vuthakral/halo/weapons/frag/explode_dist3.wav" }
} )

sound.Add( {
	name = "drc.m139_flight",
	channel = CHAN_AUTO,
	volume = 0.82,
	level = 68,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m139/loop.wav" }
} )

sound.Add( {
	name = "drc.m139_fire",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m139/fire0.wav",
	"vuthakral/halo/weapons/m139/fire1.wav",
	"vuthakral/halo/weapons/m139/fire2.wav",
	"vuthakral/halo/weapons/m139/fire3.wav",
	"vuthakral/halo/weapons/m139/fire4.wav",
	"vuthakral/halo/weapons/m139/fire5.wav",
	"vuthakral/halo/weapons/m139/fire6.wav",
	"vuthakral/halo/weapons/m139/fire7.wav",
	"vuthakral/halo/weapons/m139/fire8.wav",
	"vuthakral/halo/weapons/m139/fire9.wav" }
} )

sound.Add( {
	name = "drc.m139_fire_dist",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m139/fire_dist0.wav",
	"vuthakral/halo/weapons/m139/fire_dist1.wav",
	"vuthakral/halo/weapons/m139/fire_dist2.wav",
	"vuthakral/halo/weapons/m139/fire_dist3.wav",
	"vuthakral/halo/weapons/m139/fire_dist4.wav",
	"vuthakral/halo/weapons/m139/fire_dist5.wav",
	"vuthakral/halo/weapons/m139/fire_dist6.wav",
	"vuthakral/halo/weapons/m139/fire_dist7.wav",
	"vuthakral/halo/weapons/m139/fire_dist8.wav",
	"vuthakral/halo/weapons/m139/fire_dist9.wav" }
} )

sound.Add( {
	name = "drc.m139_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 50,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m139/ready0a.wav",
	"vuthakral/halo/weapons/m139/ready0b.wav",
	"vuthakral/halo/weapons/m139/ready0c.wav" }
} )

sound.Add( {
	name = "drc.m139_ready_initial",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 50,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m139/ready1a.wav",
	"vuthakral/halo/weapons/m139/ready1b.wav",
	"vuthakral/halo/weapons/m139/ready1c.wav" }
} )

sound.Add( {
	name = "drc.m139_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 57,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m139/pose0a.wav",
	"vuthakral/halo/weapons/m139/pose0b.wav" }
} )

sound.Add( {
	name = "drc.m139_pose2",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 57,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m139/pose1.wav" }
} )

sound.Add( {
	name = "drc.m139_reload",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 93,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m139/reload0a.wav",
	"vuthakral/halo/weapons/m139/reload0b.wav",
	"vuthakral/halo/weapons/m139/reload0c.wav" }
} )

sound.Add( {
	name = "drc.m139_melee1",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 50,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m139/melee0a.wav",
	"vuthakral/halo/weapons/m139/melee0b.wav",
	"vuthakral/halo/weapons/m139/melee0c.wav" }
} )

sound.Add( {
	name = "drc.m139_melee2",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 50,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m139/melee1a.wav",
	"vuthakral/halo/weapons/m139/melee1b.wav",
	"vuthakral/halo/weapons/m139/melee1c.wav" }
} )

sound.Add( {
	name = "drc.halo_flashlighttoggle",
	channel = CHAN_WEAPON,
	volume = 1,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/flashlight.wav" }
} )

sound.Add( {
	name = "drc.plasma_switch",
	channel = CHAN_AUTO,
	volume = 1,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmanade/switch_plasma.wav" }
} )

sound.Add( {
	name = "drc.frag_switch",
	channel = CHAN_AUTO,
	volume = 1,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/frag/switch_frag.wav" }
} )

sound.Add( {
	name = "drc.frag_throw",
	channel = CHAN_AUTO,
	volume = 1,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/frag/frag_throw.wav" }
} )

sound.Add( {
	name = "drc.frag_pickup",
	channel = CHAN_AUTO,
	volume = 1,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/frag/1.wav",
	"vuthakral/halo/weapons/frag/2.wav",
	"vuthakral/halo/weapons/frag/3.wav" }
} )

sound.Add( {
	name = "drc.plasma_pickup",
	channel = CHAN_AUTO,
	volume = 1,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmanade/1.wav",
	"vuthakral/halo/weapons/plasmanade/2.wav",
	"vuthakral/halo/weapons/plasmanade/3.wav" }
} )

sound.Add( {
	name = "drc.plasma_throw",
	channel = CHAN_AUTO,
	volume = 1,
	level = 40,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/plasmanade/plasma_throw.wav" }
} )

sound.Add( {
	name = "drc.plasmagrenade_explode",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/plasmanade/explode0.wav",
	"vuthakral/halo/weapons/plasmanade/explode1.wav",
	"vuthakral/halo/weapons/plasmanade/explode2.wav",
	"vuthakral/halo/weapons/plasmanade/explode3.wav" }
} )

sound.Add( {
	name = "drc.plasmagrenade_explode_dist",
	channel = CHAN_AUTO,
	volume = 1,
	level = 140,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/plasmanade/explode_dist0.wav",
	"vuthakral/halo/weapons/plasmanade/explode_dist1.wav",
	"vuthakral/halo/weapons/plasmanade/explode_dist2.wav" }
} )


sound.Add( {
	name = "drc.energysword_ready",
	channel = CHAN_AUTO,
	volume = 0.55,
	level = 80,
	pitch = { 99, 101 },
	sound = "vuthakral/halo/custom/energysword/ready.wav"
} )

sound.Add( {
	name = "D_HaloES.Lunge",
	channel = CHAN_AUTO,
	volume = 0.85,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/custom/energysword/lunge1.wav",
	"vuthakral/halo/custom/energysword/lunge2.wav",
	"vuthakral/halo/custom/energysword/lunge3.wav" }
} )

sound.Add( {
	name = "D_HaloES.Melee",
	channel = CHAN_AUTO,
	volume = 1,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/custom/energysword/melee1.wav",
	"vuthakral/halo/custom/energysword/melee2.wav",
	"vuthakral/halo/custom/energysword/melee3.wav" }
} )

sound.Add( {
	name = "D_HaloES.HitWorld",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/custom/energysword/sword_hit_env1.wav",
	"vuthakral/halo/custom/energysword/sword_hit_env2.wav",
	"vuthakral/halo/custom/energysword/sword_hit_env3.wav",
	"vuthakral/halo/custom/energysword/sword_hit_env4.wav",
	"vuthakral/halo/custom/energysword/sword_hit_env5.wav" }
} )

sound.Add( {
	name = "D_HaloES.HitFlesh",
	channel = CHAN_AUTO,
	volume = 0.71,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/custom/energysword/sword_hit_char1.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char2.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char3.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char4.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char5.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char6.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char7.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char8.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char9.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char10.wav",
	"vuthakral/halo/custom/energysword/sword_hit_char11.wav" }
} )

sound.Add( {
	name = "drc.Needler_fire",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 80,
	pitch = { 98, 101 },
	sound = { "vuthakral/halo/weapons/Needler/fire0.wav",
		"vuthakral/halo/weapons/Needler/fire1.wav",
		"vuthakral/halo/weapons/Needler/fire2.wav" }
} )

sound.Add( {
	name = "drc.Needler_melee",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/Needler/melee.wav" }
} )

sound.Add( {
	name = "drc.Needler_reload",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/Needler/reload0.wav" }
} )

sound.Add( {
	name = "drc.Needler_pose",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/Needler/pose1.wav" }
} )

sound.Add( {
	name = "drc.Needler_ready",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/Needler/ready.wav" }
} )

sound.Add( {
	name = "drc.Needler_supercombine",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/Needler/supercombine.wav" }
} )

sound.Add( {
	name = "drc.Needler_explosion",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/Needler/expl1.wav",
		"vuthakral/halo/weapons/Needler/expl3.wav" }
} )

sound.Add( {
	name = "drc.Needler_reload_end",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/Needler/reload1.wav" }
} )

sound.Add( {
	name = "drc.m45_shellinsert",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m45/loop0.wav",
	"vuthakral/halo/weapons/m45/loop1.wav",
	"vuthakral/halo/weapons/m45/loop2.wav",
	"vuthakral/halo/weapons/m45/loop3.wav",
	"vuthakral/halo/weapons/m45/loop4.wav",
	"vuthakral/halo/weapons/m45/loop5.wav",
	"vuthakral/halo/weapons/m45/loop6.wav",
	"vuthakral/halo/weapons/m45/loop7.wav" }
} )

sound.Add( {
	name = "drc.m45_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 42,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m45/pose0-1.wav",
	"vuthakral/halo/weapons/m45/pose0-2.wav",
	"vuthakral/halo/weapons/m45/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.m45_pose2",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 42,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m45/pose1-1.wav",
	"vuthakral/halo/weapons/m45/pose1-2.wav",
	"vuthakral/halo/weapons/m45/pose1-3.wav" }
} )

sound.Add( {
	name = "drc.m45_ready",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m45/ready0.wav",
	"vuthakral/halo/weapons/m45/ready1.wav",
	"vuthakral/halo/weapons/m45/ready2.wav" }
} )

sound.Add( {
	name = "drc.m45_ready_initial",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m45/ready_hero0.wav",
	"vuthakral/halo/weapons/m45/ready_hero1.wav",
	"vuthakral/halo/weapons/m45/ready_hero2.wav" }
} )

sound.Add( {
	name = "drc.m45_melee1",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 42,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m45/melee0-1.wav",
	"vuthakral/halo/weapons/m45/melee0-1.wav",
	"vuthakral/halo/weapons/m45/melee0-2.wav" }
} )

sound.Add( {
	name = "drc.m45_melee2",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 42,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m45/melee1-1.wav",
	"vuthakral/halo/weapons/m45/melee1-1.wav",
	"vuthakral/halo/weapons/m45/melee1-2.wav" }
} )

sound.Add( {
	name = "drc.m45_reloadexit",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 50,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/m45/exit.wav" }
} )

sound.Add( {
	name = "drc.m45_firepump",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 42,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m45/pump0-1.wav",
		"vuthakral/halo/weapons/m45/pump0-2.wav",
		"vuthakral/halo/weapons/m45/pump0-3.wav",
		"vuthakral/halo/weapons/m45/pump1-1.wav",
		"vuthakral/halo/weapons/m45/pump1-2.wav",
		"vuthakral/halo/weapons/m45/pump1-3.wav",
		"vuthakral/halo/weapons/m45/pump2-1.wav",
		"vuthakral/halo/weapons/m45/pump2-2.wav",
		"vuthakral/halo/weapons/m45/pump2-3.wav" }
} )

sound.Add( {
	name = "drc.m90ce_fire",
	channel = CHAN_WEAPON,
	volume = 1,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90ce/fire0.wav" }
} )

sound.Add( {
	name = "drc.m90ce_ready",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 50,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90ce/ready.wav" }
} )

sound.Add( {
	name = "drc.m90ce_melee1",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 45,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90ce/melee.wav" }
} )

sound.Add( {
	name = "drc.m90ce_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 42,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90ce/pose0.wav" }
} )

sound.Add( {
	name = "drc.m90ce_firepump",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 42,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90ce/pump.wav" }
} )

sound.Add( {
	name = "drc.m90ce_shellinsert",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m90ce/loop0.wav",
	"vuthakral/halo/weapons/m90ce/loop1.wav",
	"vuthakral/halo/weapons/m90ce/loop2.wav",
	"vuthakral/halo/weapons/m90ce/loop3.wav",
	"vuthakral/halo/weapons/m90ce/loop4.wav",
	"vuthakral/halo/weapons/m90ce/loop5.wav",
	"vuthakral/halo/weapons/m90ce/loop6.wav" }
} )

sound.Add( {
	name = "drc.srs99am_fire",
	channel = CHAN_WEAPON,
	volume = 1,
	level = 92,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/fire0.wav", 
	"vuthakral/halo/weapons/srs99am/fire1.wav" }
} )

sound.Add( {
	name = "drc.srs99am_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/ready0.wav",
	"vuthakral/halo/weapons/srs99am/ready1.wav",
	"vuthakral/halo/weapons/srs99am/ready2.wav" }
} )

sound.Add( {
	name = "drc.srs99am_ready_hero",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/ready_hero0.wav",
	"vuthakral/halo/weapons/srs99am/ready_hero1.wav",
	"vuthakral/halo/weapons/srs99am/ready_hero2.wav" }
} )

sound.Add( {
	name = "drc.srs99am_reload",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 46,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/reload0.wav",
	"vuthakral/halo/weapons/srs99am/reload1.wav",
	"vuthakral/halo/weapons/srs99am/reload2.wav" }
} )

sound.Add( {
	name = "drc.srs99am_reload_empty",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 46,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/reload_empty0.wav",
	"vuthakral/halo/weapons/srs99am/reload_empty1.wav",
	"vuthakral/halo/weapons/srs99am/reload_empty2.wav" }
} )

sound.Add( {
	name = "drc.srs99am_melee0",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/melee0.wav",
	"vuthakral/halo/weapons/srs99am/melee1.wav",
	"vuthakral/halo/weapons/srs99am/melee2.wav" }
} )

sound.Add( {
	name = "drc.srs99am_pose0",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/pose0-1.wav",
	"vuthakral/halo/weapons/srs99am/pose0-2.wav",
	"vuthakral/halo/weapons/srs99am/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.srs99am_pose1",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/srs99am/pose1-1.wav",
	"vuthakral/halo/weapons/srs99am/pose1-2.wav",
	"vuthakral/halo/weapons/srs99am/pose1-3.wav" }
} )

sound.Add( {
	name = "drc.mauler_ready",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/mauler/ready0.wav",
	"vuthakral/halo/weapons/mauler/ready1.wav",
	"vuthakral/halo/weapons/mauler/ready2.wav",
	"vuthakral/halo/weapons/mauler/ready3.wav",
	"vuthakral/halo/weapons/mauler/ready4.wav",
	"vuthakral/halo/weapons/mauler/ready5.wav" }
} )

sound.Add( {
	name = "drc.mauler_reload",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/mauler/reload0.wav",
	"vuthakral/halo/weapons/mauler/reload1.wav",
	"vuthakral/halo/weapons/mauler/reload2.wav",
	"vuthakral/halo/weapons/mauler/reload3.wav",
	"vuthakral/halo/weapons/mauler/reload4.wav",
	"vuthakral/halo/weapons/mauler/reload5.wav" }
} )

sound.Add( {
	name = "drc.mauler_fire",
	channel = CHAN_WEAPON,
	volume = 0.9,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/mauler/fire0.wav",
	"vuthakral/halo/weapons/mauler/fire1.wav",
	"vuthakral/halo/weapons/mauler/fire2.wav" }
} )

sound.Add( {
	name = "drc.mauler_melee0",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/mauler/melee0.wav",
	"vuthakral/halo/weapons/mauler/melee1.wav",
	"vuthakral/halo/weapons/mauler/melee2.wav" }
} )

sound.Add( {
	name = "drc.mauler_pose0",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 32,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/mauler/pose0.wav",
	"vuthakral/halo/weapons/mauler/pose1.wav",
	"vuthakral/halo/weapons/mauler/pose2.wav" }
} )

sound.Add( {
	name = "drc.t33a_ready",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33a/ready.wav" }
} )

sound.Add( {
	name = "drc.t33a_melee",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/plasmarifle/plasrifle_melee1.wav", 
	"vuthakral/halo/weapons/plasmarifle/plasrifle_melee2.wav", 
	"vuthakral/halo/weapons/plasmarifle/plasrifle_melee3.wav" }
} )

sound.Add( {
	name = "drc.t33a_explode",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/t33a/explode0.wav",
	"vuthakral/halo/weapons/t33a/explode1.wav" }
} )

sound.Add( {
	name = "drc.fuelrod_flight",
	channel = CHAN_AUTO,
	volume = 0.82,
	level = 85,
	pitch = { 115, 115 },
	sound = { "vuthakral/halo/weapons/t33b/proj_loop.wav" }
} )

sound.Add( {
	name = "drc.t33a_fire",
	channel = CHAN_WEAPON,
	volume = 0.9,
	level = 80,
	pitch = { 100, 101.5 },
	sound = { "vuthakral/halo/weapons/t33a/fire0.wav" }
} )

sound.Add( {
	name = "drc.t33b_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33b/fire0.wav", 
	"vuthakral/halo/weapons/t33b/fire1.wav", 
	"vuthakral/halo/weapons/t33b/fire2.wav" }
} )

sound.Add( {
	name = "drc.t33b_explode",
	channel = CHAN_AUTO,
	volume = 0.62,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/t33b/explode0.wav",
	"vuthakral/halo/weapons/t33b/explode1.wav",
	"vuthakral/halo/weapons/t33b/explode2.wav" }
} )

sound.Add( {
	name = "drc.t33b_reload1",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33b/reload0.wav",
	"vuthakral/halo/weapons/t33b/reload1.wav",
	"vuthakral/halo/weapons/t33b/reload2.wav" }
} )

sound.Add( {
	name = "drc.t33b_melee0",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33b/melee0.wav",
	"vuthakral/halo/weapons/t33b/melee1.wav",
	"vuthakral/halo/weapons/t33b/melee2.wav" }
} )

sound.Add( {
	name = "drc.t33b_melee0",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33b/melee0.wav",
	"vuthakral/halo/weapons/t33b/melee1.wav",
	"vuthakral/halo/weapons/t33b/melee2.wav" }
} )

sound.Add( {
	name = "drc.t33b_ready",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 46,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33b/ready0.wav",
	"vuthakral/halo/weapons/t33b/ready1.wav",
	"vuthakral/halo/weapons/t33b/ready2.wav" }
} )

sound.Add( {
	name = "drc.t33b_pose1",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 69,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33b/pose0-1.wav",
	"vuthakral/halo/weapons/t33b/pose0-2.wav",
	"vuthakral/halo/weapons/t33b/pose0-3.wav" }
} )

sound.Add( {
	name = "drc.t33b_pose2",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 69,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t33b/pose1-1.wav",
	"vuthakral/halo/weapons/t33b/pose1-2.wav",
	"vuthakral/halo/weapons/t33b/pose1-3.wav" }
} )

sound.Add( {
	name = "drc.m7057_idle",
	channel = CHAN_AUTO,
	volume = 0.6,
	level = 42,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m7057/ft_idle.wav" }
} )

sound.Add( {
	name = "drc.m7057_melee",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 42,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m7057/melee0.wav" }
} )

sound.Add( {
	name = "drc.m7057_pose",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 44,
	pitch = { 98.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m7057/pose0.wav" }
} )

sound.Add( {
	name = "drc.m7057_fire",
	channel = CHAN_AUTO,
	volume = 0.9,
	level = 79,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m7057/fire0.wav" }
} )

sound.Add( {
	name = "drc.M90H2_shellinsert",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 43,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/m90h2/loop.wav" }
} )

sound.Add( {
	name = "drc.m90h2_fire",
	channel = CHAN_WEAPON,
	volume = 0.65,
	level = 80,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90h2/fireh2a_0.wav",
	"vuthakral/halo/weapons/m90h2/fireh2a_1.wav" }
} )

sound.Add( {
	name = "drc.m90h2_pose1",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 47,
	pitch = { 100, 100 },
	sound = { "" }
} )

sound.Add( {
	name = "drc.m90h2_pose2",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 47,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/m90h2/pose1.wav" }
} )

sound.Add( {
	name = "drc.m90h2_ready",
	channel = CHAN_AUTO,
	volume = 0.89,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m90h2/ready.wav" }
} )

sound.Add( {
	name = "drc.m90h2_melee1",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 45,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90h2/melee0.wav" }
} )

sound.Add( {
	name = "drc.m90h2_melee2",
	channel = CHAN_BODY,
	volume = 0.69,
	level = 45,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90h2/melee1.wav" }
} )

sound.Add( {
	name = "drc.m90h2_reloadexit",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/m90h2/exit.wav" }
} )

sound.Add( {
	name = "drc.m90h2_firepump",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 40,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/m90h2/pump.wav" }
} )

sound.Add( {
	name = "drc.sentinel_fireloop",
	channel = CHAN_AUTO,
	volume = 0.79,
	level = 90,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/sentinel/fire_loop.wav" }
} )

sound.Add( {
	name = "drc.sentinel_impact",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 70,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/sentinel/1untitled_marker_1.wav",
	"vuthakral/halo/weapons/sentinel/1untitled_marker_2.wav",
	"vuthakral/halo/weapons/sentinel/1untitled_marker_3.wav",
	"vuthakral/halo/weapons/sentinel/1untitled_marker_4.wav",
	"vuthakral/halo/weapons/sentinel/2untitled_marker_1.wav",
	"vuthakral/halo/weapons/sentinel/2untitled_marker_2.wav",
	"vuthakral/halo/weapons/sentinel/2untitled_marker_3.wav",
	"vuthakral/halo/weapons/sentinel/2untitled_marker_4.wav",
	"vuthakral/halo/weapons/sentinel/2untitled_marker_5.wav",
	"vuthakral/halo/weapons/sentinel/3untitled_marker_1.wav",
	"vuthakral/halo/weapons/sentinel/3untitled_marker_2.wav",
	"vuthakral/halo/weapons/sentinel/3untitled_marker_4.wav",
	"vuthakral/halo/weapons/sentinel/17.wav",
	"vuthakral/halo/weapons/sentinel/29.wav",
	"vuthakral/halo/weapons/sentinel/36.wav",
	"vuthakral/halo/weapons/sentinel/37.wav",
	"vuthakral/halo/weapons/sentinel/1untitled_marker_5.wav" }
} )

sound.Add( {
	name = "drc.sentinel_firein",
	channel = CHAN_WEAPON,
	volume = 0.79,
	level = 90,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/sentinel/fire_in.wav" }
} )

sound.Add( {
	name = "drc.sentinel_fireout",
	channel = CHAN_WEAPON,
	volume = 0.79,
	level = 90,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/sentinel/fire_out.wav" }
} )

sound.Add( {
	name = "drc.sentinel_overheated",
	channel = CHAN_VOICE2,
	volume = 0.59,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/sentinel/oh_loop.wav" }
} )

sound.Add( {
	name = "drc.sentinel_lockup",
	channel = CHAN_AUTO,
	volume = 0.59,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/sentinel/oh_in.wav" }
} )

sound.Add( {
	name = "drc.sentinel_oh_in",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/sentinel/oh_in.wav" }
} )

sound.Add( {
	name = "drc.sentinel_oh_out",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/sentinel/oh_out.wav" }
} )

sound.Add( {
	name = "drc.sentinel_ready",
	channel = CHAN_AUTO,
	volume = 0.62,
	level = 80,
	pitch = { 107.5, 110 },
	sound = { "vuthakral/halo/weapons/sentinel/sentinel_ready.wav" }
} )

sound.Add( {
	name = "drc.sentinel_melee",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 42.5,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/sentinel/sentinel_melee.wav" }
} )

sound.Add( {
	name = "drc.sentinel_pose0",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/sentinel/sentinel_posing.wav" }
} )

sound.Add( {
	name = "drc.sentinel_pose1",
	channel = CHAN_BODY,
	volume = 1,
	level = 37,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/sentinel/sentinel_posing.wav" }
} )

sound.Add( {
	name = "drc.t57b_fire",
	channel = CHAN_WEAPON,
	volume = 1.0,
	level = 80,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t57b/fire0.wav",
	"vuthakral/halo/weapons/t57b/fire1.wav",
	"vuthakral/halo/weapons/t57b/fire3.wav",
	"vuthakral/halo/weapons/t57b/fire4.wav" }
} )

sound.Add( {
	name = "drc.cov_carbine_impact",
	channel = CHAN_AUTO,
	volume = 1,
	level = 70,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/covy_plasma_hit7.wav",
	"vuthakral/halo/weapons/covy_plasma_hit8.wav",
	"vuthakral/halo/weapons/covy_plasma_hit9.wav",
	"vuthakral/halo/weapons/covy_plasma_hit10.wav",
	"vuthakral/halo/weapons/covy_plasma_hit11.wav",
	"vuthakral/halo/weapons/covy_plasma_hit12.wav",
	"vuthakral/halo/weapons/covy_plasma_hit13.wav",
	"vuthakral/halo/weapons/covy_plasma_hit14.wav" }
} )

sound.Add( {
	name = "drc.cov_carbine_ammo",
	channel = CHAN_AUTO,
	volume = 1,
	level = 60,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/t51c/carbine_ammo_pickup1.wav",
	"vuthakral/halo/weapons/t51c/carbine_ammo_pickup2.wav",
	"vuthakral/halo/weapons/t51c/carbine_ammo_pickup3.wav" }
} )

sound.Add( {
	name = "drc.carbine_click",
	channel = CHAN_AUTO,
	volume = 0.92,
	level = 60,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/t51c/carbine_misfire1.wav",
	"vuthakral/halo/weapons/t51c/carbine_misfire2.wav",
	"vuthakral/halo/weapons/t51c/carbine_misfire3.wav",
	"vuthakral/halo/weapons/t51c/carbine_misfire4.wav" }
} )


sound.Add( {
	name = "drc.t51_fire",
	channel = CHAN_WEAPON,
	volume = 0.62,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/t51c/carbine_new28.wav",
	"vuthakral/halo/weapons/t51c/carbine_new29.wav",
	"vuthakral/halo/weapons/t51c/carbine_new30.wav" }
} )

sound.Add( {
	name = "drc.t51_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.79,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/t51c/carbine_lod_far1.wav",
	"vuthakral/halo/weapons/t51c/carbine_lod_far2.wav",
	"vuthakral/halo/weapons/t51c/carbine_lod_far3.wav" }
} )

sound.Add( {
	name = "drc.carbineh3_melee",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/t51c/carbine_fp_melee_1_1.wav", 
	"vuthakral/halo/weapons/t51c/carbine_fp_melee_1_2.wav", 
	"vuthakral/halo/weapons/t51c/carbine_fp_melee_1_3.wav" }
} )

sound.Add( {
	name = "drc.carbineh3_ready",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/t51c/ready1.wav", 
	"vuthakral/halo/weapons/t51c/ready2.wav", 
	"vuthakral/halo/weapons/t51c/ready3.wav" }
} )

sound.Add( {
	name = "drc.carbineh3_reload1",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t51c/reload_empty1.wav", 
	"vuthakral/halo/weapons/t51c/reload_empty2.wav", 
	"vuthakral/halo/weapons/t51c/reload_empty3.wav" }
} )

sound.Add( {
	name = "drc.carbineh3_pose0",
	channel = CHAN_AUTO,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t51c/carbine_posing2_1.wav", 
	"vuthakral/halo/weapons/t51c/carbine_posing2_2.wav", 
	"vuthakral/halo/weapons/t51c/carbine_posing2_3.wav" }
} )

sound.Add( {
	name = "drc.carbineh3_pose1",
	channel = CHAN_WEAPON,
	volume = 0.52,
	level = 56,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t51c/carbine_posing2_1.wav", 
	"vuthakral/halo/weapons/t51c/carbine_posing2_2.wav", 
	"vuthakral/halo/weapons/t51c/carbine_posing2_3.wav" }
} )

sound.Add( {
	name = "drc.srs50_fire",
	channel = CHAN_WEAPON,
	volume = 0.92,
	level = 95,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/beamrifle/cov_snip_shot9.wav",
	"vuthakral/halo/weapons/beamrifle/cov_snip_shot10.wav" }
} )

sound.Add( {
	name = "drc.srs50_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.79,
	level = 110,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/beamrifle/beam_rifle_lod_far1.wav",
	"vuthakral/halo/weapons/beamrifle/beam_rifle_lod_far2.wav",
	"vuthakral/halo/weapons/beamrifle/beam_rifle_lod_far3.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_pose0",
	channel = CHAN_WEAPON,
	volume = 0.52,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/beamrifle/fp_beam_posing_var1_1.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_posing_var1_2.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_posing_var1_3.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_pose1",
	channel = CHAN_WEAPON,
	volume = 0.52,
	level = 50,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/beamrifle/fp_beam_posing_var2_1.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_posing_var2_2.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_posing_var2_3.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_ready",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 56,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/beamrifle/fp_beam_ready_1.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_ready_2.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_ready_3.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_lockup",
	channel = CHAN_AUTO,
	volume = 0.59,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/beamrifle/in.wav", 
	"vuthakral/halo/weapons/beamrifle/in2.wav", 
	"vuthakral/halo/weapons/beamrifle/in3.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_oh_loop",
	channel = CHAN_AUTO,
	volume = 0.59,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/beamrifle/loop.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_oh_out",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/beamrifle/out.wav" }
} )

sound.Add( {
	name = "drc.SRS50_Rotate",
	channel = CHAN_AUTO,
	volume = 0.92,
	level = 55,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/beamrifle/first_person_fire1.wav",
	"vuthakral/halo/weapons/beamrifle/first_person_fire2.wav",
	"vuthakral/halo/weapons/beamrifle/first_person_fire3.wav",
	"vuthakral/halo/weapons/beamrifle/first_person_fire4.wav" }
} )

sound.Add( {
	name = "drc.SRS50_Click",
	channel = CHAN_AUTO,
	volume = 0.92,
	level = 60,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/beamrifle/beam_rifle_misfire.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_melee0",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/beamrifle/fp_beam_melee_1_1.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_melee_1_2.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_melee_1_3.wav" }
} )

sound.Add( {
	name = "drc.T50SRS_melee1",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 50,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/beamrifle/fp_beam_melee_2_1.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_melee_2_2.wav", 
	"vuthakral/halo/weapons/beamrifle/fp_beam_melee_2_3.wav" }
} )

sound.Add( {
	name = "drc.cov_damage_large",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/damage/cov_damage_large/cov_damage_large1.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large2.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large3.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large4.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large5.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large6.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large7.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large8.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large9.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large10.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large11.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large12.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large13.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large14.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large15.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large16.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large17.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large18.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large19.wav",
	"vuthakral/halo/damage/cov_damage_large/cov_damage_large20.wav" }
} )

sound.Add( {
	name = "drc.concussion_fire",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 90,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/concussion/concussion_firing01.wav", 
	"vuthakral/halo/weapons/concussion/concussion_firing02.wav", 
	"vuthakral/halo/weapons/concussion/concussion_firing03.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing04.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing05.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing06.wav" }
} )

sound.Add( {
	name = "drc.concussion_fire_dist",
	channel = CHAN_AUTO,
	volume = 0.7,
	level = 120,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/concussion/concussion_firing_lod01.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing_lod02.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing_lod03.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing_lod04.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing_lod05.wav",
	"vuthakral/halo/weapons/concussion/concussion_firing_lod06.wav" }
} )

sound.Add( {
	name = "drc.ConcussionRifle_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 50,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/concussion/fp_cr_ready01.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_ready02.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_ready03.wav" }
} )

sound.Add( {
	name = "drc.ConcussionRifle_ready_initial",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/concussion/fp_cr_ready_init01.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_ready_init02.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_ready_init03.wav" }
} )

sound.Add( {
	name = "drc.ConcussionRifle_reload1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/concussion/fp_cr_reload01.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_reload02.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_reload03.wav" }
} )

sound.Add( {
	name = "drc.ConcussionRifle_melee",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/concussion/fp_cr_melee101.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_melee102.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_melee103.wav" }
} )

sound.Add( {
	name = "drc.ConcussionRifle_pose0",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 30,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/concussion/fp_cr_posing101.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_posing102.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_posing103.wav" }
} )

sound.Add( {
	name = "drc.ConcussionRifle_pose1",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 30,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/concussion/fp_cr_posing201.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_posing202.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_posing203.wav" }
} )

sound.Add( {
	name = "drc.hammer_boom",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 90,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/hammer/hammer_hit1.wav", 
	"vuthakral/halo/weapons/hammer/hammer_hit2.wav", 
	"vuthakral/halo/weapons/hammer/hammer_hit3.wav" }
} )

sound.Add( {
	name = "drc.hammer_ready",
	channel = CHAN_AUTO,
	volume = 0.55,
	level = 80,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/hammer/fp_gh_ready01.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_ready02.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_ready03.wav" }
} )

sound.Add( {
	name = "drc.hammer_lunge",
	channel = CHAN_AUTO,
	volume = 0.85,
	level = 50,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/hammer/fp_gh_lunge02_01new.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_lunge02_02new.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_lunge02_03new.wav" }
} )

sound.Add( {
	name = "drc.hammer_melee0",
	channel = CHAN_AUTO,
	volume = 1,
	level = 50,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/hammer/fp_gh_melee01_01.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_melee01_02.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_melee01_03.wav" }
} )

sound.Add( {
	name = "drc.hammer_melee1",
	channel = CHAN_AUTO,
	volume = 1,
	level = 50,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/hammer/fp_gh_melee02_01.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_melee02_02.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_melee02_03.wav" }
} )

sound.Add( {
	name = "drc.hammer_drop",
	channel = CHAN_AUTO,
	volume = 0.82,
	level = 90,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/hammer/hammer_drop1.wav",
	"vuthakral/halo/weapons/hammer/hammer_drop2.wav",
	"vuthakral/halo/weapons/hammer/hammer_drop3.wav",
	"vuthakral/halo/weapons/hammer/hammer_drop4.wav",
	"vuthakral/halo/weapons/hammer/hammer_drop5.wav" }
} )

sound.Add( {
	name = "drc.hammer_pose0",
	channel = CHAN_WEAPON,
	volume = 0.69,
	level = 30,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/hammer/fp_gh_posing01_01new.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_posing01_02new.wav",
	"vuthakral/halo/weapons/hammer/fp_gh_posing01_03new.wav" }
} )

sound.Add( {
	name = "drc.PlasmaCaster_fire",
	channel = CHAN_AUTO,
	volume = 0.72,
	level = 90,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t53/fire0.wav", 
	"vuthakral/halo/weapons/t53/fire1.wav", 
	"vuthakral/halo/weapons/t53/fire2.wav",
	"vuthakral/halo/weapons/t53/fire3.wav" }
} )

sound.Add( {
	name = "drc.PlasmaCaster_fire_oc",
	channel = CHAN_AUTO,
	volume = 0.92,
	level = 90,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/t53/fire_oc0.wav", 
	"vuthakral/halo/weapons/t53/fire_oc1.wav", 
	"vuthakral/halo/weapons/t53/fire_oc2.wav" }
} )

sound.Add( {
	name = "drc.PlasmaCaster_ready",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 50,
	pitch = { 97.5, 102.5 },
	sound = { "vuthakral/halo/weapons/concussion/fp_cr_ready01.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_ready02.wav",
	"vuthakral/halo/weapons/concussion/fp_cr_ready03.wav" }
} )

sound.Add( {
	name = "drc.PlasmaCaster_reload1",
	channel = CHAN_AUTO,
	volume = 0.69,
	level = 45,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/t53/reload2.wav",
	"vuthakral/halo/weapons/t53/reload2.wav",
	"vuthakral/halo/weapons/t53/reload2.wav",
	"vuthakral/halo/weapons/t53/reload2.wav" }
} )

sound.Add( {
	name = "drc.PlasmaCaster_charge",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 80,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/t53/charge.wav",
	"vuthakral/halo/weapons/t53/charge1.wav",
	"vuthakral/halo/weapons/t53/charge2.wav" }
} )