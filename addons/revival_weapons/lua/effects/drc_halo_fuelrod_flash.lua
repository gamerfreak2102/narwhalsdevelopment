function EFFECT:Init( data )
	local Pos = data:GetOrigin()
	
	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	self.DataNormal = data:GetNormal()
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	local sub = self.EndPos - self.StartPos
	self.Normal = sub:GetNormal()
	
	local emitter = ParticleEmitter( Pos )
	
--	EmitSound("drc.focusrifle_impact", self.EndPos, 0)
	
	for i = 1,2 do

		local particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		 
		if particle == nil then particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		
		if (particle) then
			particle:SetVelocity(Vector(math.random(-4,4),math.random(-4,4),math.random(0,0)))
			particle:SetLifeTime(0) 
			particle:SetDieTime(.2) 
			particle:SetStartAlpha(255)
			particle:SetEndAlpha(0)
			particle:SetStartSize(40.5870467639842) 
			particle:SetEndSize(40.012735977598182)
			particle:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle:SetAngleVelocity( Angle(15) ) 
			particle:SetRoll(math.Rand( 0, 360 ))
			particle:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle:SetGravity( Vector(0,0,0) ) 
			particle:SetAirResistance(-68.167394537726 )  
			particle:SetCollide(true)
			particle:SetBounce(0.1419790559388)
		end
	end
	
	local emitter2 = ParticleEmitter( Pos )
	
	for i = 1,13 do

		local particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		 
		if particle2 == nil then particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		
		if (particle2) then
			particle2:SetVelocity((-self.Normal+VectorRand() * 1):GetNormal() * math.Rand(35, 125));
			particle2:SetLifeTime(math.Rand(0.05, 0.5)) 
			particle2:SetDieTime(math.Rand(1,3)) 
			particle2:SetStartAlpha(255)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(2) 
			particle2:SetEndSize(0)
			particle2:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle2:SetGravity( Vector(0,0,-400) ) 
			particle2:SetAirResistance(0)  
			particle2:SetCollide(true)
			particle2:SetBounce(0)
		end
	end

	emitter:Finish()
		
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()
end

