function EFFECT:Init( data )
	local Pos = data:GetOrigin()
	
	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	self.DataNormal = data:GetNormal()
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	local sub = self.EndPos - self.StartPos
	self.Normal = sub:GetNormal()
	
	local emitter = ParticleEmitter( Pos )
	for i = 0,3 do
		timer.Simple(i * 0.01, function()
			for i = 1,40 do
			local emitter = ParticleEmitter( Pos )
			local particle = emitter:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
			
			if particle == nil then particle = emitter:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
			if (particle) then
				particle:SetVelocity(Vector(math.random(-3,3),math.random(-3,3),math.random(-6.5,6.5)) * 5 * i )
				particle:SetLifeTime(0) 
				particle:SetDieTime(math.Rand(0.1,0.3)) 
				particle:SetStartAlpha(255)
				particle:SetEndAlpha(0)
				particle:SetStartSize(math.Rand(5, 20)) 
				particle:SetEndSize(math.Rand(1.45, 4.5) * i)
				particle:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
				particle:SetAngleVelocity( Angle(math.Rand(5,45)) ) 
				particle:SetRoll(math.Rand( 0, 360 ))
				particle:SetColor(math.random(120,130),math.random(150,160),math.random(220,255),math.random(180,255))
				particle:SetGravity( Vector(0,0,0) ) 
				particle:SetAirResistance(100)  
				particle:SetCollide(true)
				particle:SetBounce(0.1419790559388)
			end
			emitter:Finish()
			
			local emitter69 = ParticleEmitter( Pos )
			local particle69 = emitter69:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
			
			if particle == nil then particle69 = emitter69:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
			if (particle) then
				particle69:SetVelocity(Vector(math.random(-3,3),math.random(-3,3),math.random(-6.5,6.5)) * 2.5 * i )
				particle69:SetLifeTime(0) 
				particle69:SetDieTime(math.Rand(0.1,0.3)) 
				particle69:SetStartAlpha(255)
				particle69:SetEndAlpha(0)
				particle69:SetStartSize(math.Rand(1, 3) * i) 
				particle69:SetEndSize(math.Rand(1, 7.5) * i)
				particle69:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
				particle69:SetAngleVelocity( Angle(math.Rand(5,45)) ) 
				particle69:SetRoll(math.Rand( 0, 360 ))
				particle69:SetColor(math.random(120,130),math.random(20,40),math.random(220,255),math.random(190,240))
				particle69:SetGravity( Vector(0,0,0) ) 
				particle69:SetAirResistance(100)  
				particle69:SetCollide(true)
				particle69:SetBounce(0.1419790559388)
			end
			emitter69:Finish()
			end
		end)
	end
	
	local emitter2 = ParticleEmitter( Pos )
	
	for i = 1,42 do
		local particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle2 == nil then particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle2) then
			particle2:SetVelocity((-self.Normal+VectorRand() * 45):GetNormal() * math.Rand(95, 295) * 3)
			particle2:SetLifeTime(math.Rand(0.05, 0.5)) 
			particle2:SetDieTime(math.Rand(1,3)) 
			particle2:SetStartAlpha(255)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(math.Rand(1,6)) 
			particle2:SetEndSize(0)
			particle2:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(math.random(200,220),math.random(120,180),math.random(150,200),math.random(180,255))
			particle2:SetGravity( Vector(0,0,-400) ) 
			particle2:SetAirResistance(0)  
			particle2:SetCollide(true)
			particle2:SetBounce(0)
		end
	end
	
local emitter5 = ParticleEmitter ( Pos )

	for i = 1,3 do
		local particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle5 == nil then particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle5) then
			particle5:SetVelocity((-self.Normal+VectorRand()):GetNormal() * 1500);
			particle5:SetLifeTime(0) 
			particle5:SetDieTime(math.Rand(0.33, 1) * i) 
			particle5:SetStartAlpha(255)
			particle5:SetEndAlpha(0)
			particle5:SetStartSize(5) 
			particle5:SetEndSize(0)
			particle5:SetStartLength(150)
			particle5:SetEndLength(50)
			particle5:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle5:SetAngleVelocity( Angle(0) ) 
			particle5:SetRoll(0)
			particle5:SetColor(math.random(30,40),math.random(180,200),math.random(190,200),math.random(180,255))
			particle5:SetGravity( Vector(0,0,-600) * i ) 
			particle5:SetAirResistance( 0 )  
			particle5:SetCollide(true)
			particle5:SetBounce(0.33 * i)
		end
	end
	
	local emitter6 = ParticleEmitter ( Pos )

	for i = 1,85 do
		local particle6 = emitter6:Add( "particle/particle_smokegrenade", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle6 == nil then particle6 = emitter6:Add( "particle/particle_smokegrenade", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle6) then
			particle6:SetVelocity((-self.Normal+VectorRand()):GetNormal() * 120 + Vector(0, 0, 2.5 * i));
			particle6:SetLifeTime(0) 
			particle6:SetDieTime(math.Rand(0.01, 0.01) * i) 
			particle6:SetStartAlpha(math.Rand(180,200))
			particle6:SetEndAlpha(0)
			particle6:SetStartSize(math.Rand(35,45)) 
			particle6:SetEndSize(math.Rand(45,145))
			particle6:SetStartLength(0)
			particle6:SetEndLength(0)
			particle6:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle6:SetAngleVelocity( Angle(math.Rand(3,5)) ) 
			particle6:SetRoll(0)
			particle6:SetColor(0, 0, 0, math.Rand(150,200))
			particle6:SetGravity( Vector(0,0,1) * i ) 
			particle6:SetAirResistance( 0 )  
			particle6:SetCollide(true)
			particle6:SetBounce(0.33 * i)
		end
	end

	emitter:Finish()
	emitter2:Finish()
	emitter5:Finish()
	emitter6:Finish()
		
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()
end

