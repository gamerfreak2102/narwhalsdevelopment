if SERVER then AddCSLuaFile() end

function EFFECT:Init(data)

	self.Start = data:GetOrigin()
	self.size = 0.5
	self.timescale = 0.9
	self.Emitter = ParticleEmitter(self.Start)
			
		for i = 1, math.Rand(35,45) do
			local p = self.Emitter:Add("effects/muzzleflash"..math.random(1,4), self.Start)

			p:SetDieTime(math.Rand(0.4, 0.5)*self.timescale)
			p:SetStartAlpha(149)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(30,40) * self.size)
			p:SetEndSize(120 * self.size)
			p:SetRoll(math.Rand(0,0))
			p:SetRollDelta(math.Rand(0,0))
			
			p:SetVelocity(VectorRand() * math.random(125,150) * self.size)
			p:SetColor(255, 255, 255)
		end
		
		for i = 1, math.Rand(10,25) do
			local p = self.Emitter:Add("sprites/heatwave", self.Start)

			p:SetDieTime(math.Rand(0.4, 0.45)*self.timescale)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(15,20) * self.size)
			p:SetEndSize(80 * self.size)
			p:SetRoll(math.Rand(0,0))
			p:SetRollDelta(math.Rand(0,0))
			
			p:SetVelocity(VectorRand() * math.random(100,125) * self.size)
			p:SetColor(255, 255, 255)
		end
		
		for i = 1, math.Rand(5,15) do
			local p = self.Emitter:Add("effects/muzzleflash"..math.random(1,4), self.Start)

			p:SetDieTime(math.Rand(0.3, 0.4)*self.timescale)
			p:SetStartAlpha(149)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(15,25) * self.size)
			p:SetEndSize(80 * self.size)
			p:SetRoll(math.Rand(0,0))
			p:SetRollDelta(math.Rand(0,0))
			
			p:SetVelocity(VectorRand() * math.random(100,125) * self.size)
			p:SetColor(255, 255, 255)
		end
	
	self.Emitter:Finish()
	
	-- surface.PlaySound("pillar/elite_shot_explode_0"..math.random(1,6)..".wav")
end

function EFFECT:Think()
	return false
end

function EFFECT:Render()
	return false
end