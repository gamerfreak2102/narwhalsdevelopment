function EFFECT:Init( data )
	local Pos = data:GetOrigin()
	
	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	self.DataNormal = data:GetNormal()
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	local sub = self.EndPos - self.StartPos
	self.Normal = sub:GetNormal()
	
	local emitter = ParticleEmitter( Pos )

	for i = 1,7 do
		local particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle == nil then particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle) then
			particle:SetVelocity(Vector(math.random(-4,4),math.random(-4,4),math.random(0,0)))
			particle:SetLifeTime(0) 
			particle:SetDieTime(.2) 
			particle:SetStartAlpha(255)
			particle:SetEndAlpha(0)
			particle:SetStartSize(20) 
			particle:SetEndSize(90)
			particle:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle:SetAngleVelocity( Angle(15) ) 
			particle:SetRoll(math.Rand( 0, 360 ))
			particle:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle:SetGravity( Vector(0,0,0) ) 
			particle:SetAirResistance(-68.167394537726 )  
			particle:SetCollide(true)
			particle:SetBounce(0.1419790559388)
		end
	end
	
	local emitter3 = ParticleEmitter( Pos )

	for i = 1,27 do
		local particle3 = emitter3:Add( "particle/particle_smokegrenade", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle3 == nil then particle3 = emitter3:Add( "particle/particle_smokegrenade", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle3) then
			particle3:SetVelocity((-self.Normal+VectorRand() * 45):GetNormal() * math.Rand(25, 95));
			particle3:SetLifeTime(0) 
			particle3:SetDieTime(.4) 
			particle3:SetStartAlpha(255)
			particle3:SetEndAlpha(0)
			particle3:SetStartSize(20) 
			particle3:SetEndSize(0)
			particle3:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle3:SetAngleVelocity( Angle(15) ) 
			particle3:SetRoll(math.Rand( 0, 360 ))
			particle3:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle3:SetGravity( Vector(0,0,0) ) 
			particle3:SetAirResistance(-68.167394537726 )  
			particle3:SetCollide(true)
			particle3:SetBounce(0.1419790559388)
		end
	end
	
	local emitter2 = ParticleEmitter( Pos )
	
	for i = 1,72 do
		local particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle2 == nil then particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle2) then
			particle2:SetVelocity((-self.Normal+VectorRand() * 45):GetNormal() * math.Rand(95, 295));
			particle2:SetLifeTime(math.Rand(0.05, 0.5)) 
			particle2:SetDieTime(math.Rand(2,7)) 
			particle2:SetStartAlpha(255)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(2) 
			particle2:SetEndSize(0)
			particle2:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle2:SetGravity( Vector(0,0,-400) ) 
			particle2:SetAirResistance(0)  
			particle2:SetCollide(true)
			particle2:SetBounce(0)
		end
	end
	
local emitter5 = ParticleEmitter ( Pos )

	for i = 1,38 do
		local particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle5 == nil then particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle5) then
			particle5:SetVelocity((-self.Normal+VectorRand() * math.Rand(15,45)):GetNormal() * math.Rand(305, 565));
			particle5:SetLifeTime(0) 
			particle5:SetDieTime(0.4) 
			particle5:SetStartAlpha(50)
			particle5:SetEndAlpha(0)
			particle5:SetStartSize(5) 
			particle5:SetEndSize(0)
			particle5:SetStartLength(50)
			particle5:SetEndLength(0)
			particle5:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle5:SetAngleVelocity( Angle(0) ) 
			particle5:SetRoll(0)
			particle5:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle5:SetGravity( Vector(0,0,0) ) 
			particle5:SetAirResistance( 0.5 )  
			particle5:SetCollide(true)
			particle5:SetBounce(0.1419790559388)
		end
	end

	emitter:Finish()
	emitter2:Finish()
	emitter5:Finish()
		
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()
end

