function EFFECT:Init( data )
	local Pos = data:GetOrigin()
	
	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	self.DataNormal = data:GetNormal()
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	local sub = self.EndPos - self.StartPos
	self.Normal = sub:GetNormal()
	
	local emitter = ParticleEmitter( Pos )

	for i = 1,2 do
		local particle = emitter:Add( "particle/warp5_warp", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle == nil then particle = emitter:Add( "particle/warp5_warp", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle) then
			particle:SetVelocity(Vector(math.random(-4,4),math.random(-4,4),math.random(0,0)))
			particle:SetLifeTime(0) 
			particle:SetDieTime(.2) 
			particle:SetStartAlpha(255)
			particle:SetLighting(false)
			particle:SetEndAlpha(0)
			particle:SetStartSize(20) 
			particle:SetEndSize(130)
			particle:SetAngles( Angle(21,3,5) )
			particle:SetAngleVelocity( Angle(15) ) 
			particle:SetRoll(math.Rand( 0, 360 ))
			particle:SetColor(255, 255, 255, 255)
			particle:SetGravity( Vector(0,0,0) ) 
			particle:SetAirResistance(-68.167394537726 )  
			particle:SetCollide(true)
			particle:SetBounce(0.1419790559388)
		end
	end
	
	local emitter2 = ParticleEmitter( Pos )

	for i = 1,7 do
		local particle2 = emitter2:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle2 == nil then particle = emitter2:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle2) then
			particle2:SetVelocity(Vector(math.random(-4,4),math.random(-4,4),math.random(0,0)))
			particle2:SetLifeTime(0) 
			particle2:SetDieTime(.2) 
			particle2:SetStartAlpha(255)
			particle2:SetLighting(false)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(2) 
			particle2:SetEndSize(40)
			particle2:SetAngles( Angle(21,3,5) )
			particle2:SetAngleVelocity( Angle(15) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(40, 120, 255, 255)
			particle2:SetGravity( Vector(0,0,0) ) 
			particle2:SetAirResistance(-68.167394537726 )  
			particle2:SetCollide(true)
			particle2:SetBounce(0.1419790559388)
		end
	end
	
	emitter:Finish()
	emitter2:Finish()
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()
end

