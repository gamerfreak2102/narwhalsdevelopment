function EFFECT:Init( data )
	local Pos = data:GetOrigin()
	
	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	self.DataNormal = data:GetNormal()
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	local sub = self.EndPos - self.StartPos
	self.Normal = sub:GetNormal()
	
	local emitter = ParticleEmitter( Pos )
	for i = 0, 5 do
		timer.Simple(i * 0.1, function()
			for i = 1,100 do
			local emitter = ParticleEmitter( Pos )
			local particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
			
			if particle == nil then particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
			if (particle) then
				particle:SetVelocity(Vector(math.random(-3,3),math.random(-3,3),math.random(-6.5,6.5)) * i )
				particle:SetLifeTime(0) 
				particle:SetDieTime(0.55) 
				particle:SetStartAlpha(255)
				particle:SetEndAlpha(0)
				particle:SetStartSize(math.Rand(5, 20)) 
				particle:SetEndSize(math.Rand(80, 250))
				particle:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
				particle:SetAngleVelocity( Angle(45) ) 
				particle:SetRoll(math.Rand( 0, 360 ))
				particle:SetColor(math.random(0,15),math.random(90,100),math.random(22,33),math.random(180,255))
				particle:SetGravity( Vector(0,0,0) ) 
				particle:SetAirResistance(100)  
				particle:SetCollide(true)
				particle:SetBounce(0.1419790559388)
			end
			emitter:Finish()
			end
		end)
	end
	
	local emitter2 = ParticleEmitter( Pos )
	
	for i = 1,72 do
		local particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle2 == nil then particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle2) then
			particle2:SetVelocity((-self.Normal+VectorRand() * 45):GetNormal() * math.Rand(95, 295) * 25)
			particle2:SetLifeTime(math.Rand(0.05, 0.5)) 
			particle2:SetDieTime(math.Rand(2,7)) 
			particle2:SetStartAlpha(255)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(2) 
			particle2:SetEndSize(0)
			particle2:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle2:SetGravity( Vector(0,0,-400) ) 
			particle2:SetAirResistance(0)  
			particle2:SetCollide(true)
			particle2:SetBounce(0)
		end
	end
	
local emitter5 = ParticleEmitter ( Pos )

	for i = 1,38 do
		local particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle5 == nil then particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle5) then
			particle5:SetVelocity((-self.Normal+VectorRand() * math.Rand(15,45)):GetNormal() * math.Rand(305, 965));
			particle5:SetLifeTime(0) 
			particle5:SetDieTime(math.Rand(0.7,1)) 
			particle5:SetStartAlpha(255)
			particle5:SetEndAlpha(0)
			particle5:SetStartSize(5) 
			particle5:SetEndSize(0)
			particle5:SetStartLength(100)
			particle5:SetEndLength(0)
			particle5:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle5:SetAngleVelocity( Angle(0) ) 
			particle5:SetRoll(0)
			particle5:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle5:SetGravity( Vector(0,0,0) ) 
			particle5:SetAirResistance( 0.5 )  
			particle5:SetCollide(true)
			particle5:SetBounce(0.1419790559388)
		end
	end

	emitter:Finish()
	emitter2:Finish()
	emitter5:Finish()
		
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()
end

