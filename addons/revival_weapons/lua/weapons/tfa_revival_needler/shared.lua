if CLIENT then
SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/entities/drchalo_needler" )
	
	killicon.Add( "tfa_revival_needler", "vgui/entities/drchalo_needler", color_white )
end
-- Variables that are used on both client and server
SWEP.Author                         = "ChaosNarwhal / Arthur"
SWEP.Contact                        = "ChaosNarwhal#9953"
SWEP.Purpose                        = "Created for revival using vuths models!"
SWEP.Base                           = "tfa_bash_base"
SWEP.Category                       = "Project Revival Armory xeno"
SWEP.Manufacturer                   = "Misriah Armory"
SWEP.Spawnable                      = true
SWEP.AdminSpawnable                 = true
SWEP.PrintName = "Needler"

SWEP.Slot					= 2				-- Slot in the weapon selection menu
SWEP.SlotPos				= 4				-- Position in the slot
SWEP.DrawAmmo				= true			-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox		= false			-- Should draw the weapon info box
SWEP.BounceWeaponIcon   	= false			-- Should the weapon icon bounce?
SWEP.DrawCrosshair			= true			-- Set false if you want no crosshair from hip
SWEP.Weight					= 30			-- Rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo			= true			-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true			-- Auto switch from if you pick up a better weapon
SWEP.XHair					= true			-- Used for returning crosshair after scope. Must be the same as DrawCrosshair
SWEP.BoltAction				= false			-- Is this a bolt action rifle?
SWEP.HoldType 				= "pistol"		-- how others view you carrying the weapon
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive 
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg and crossbow make for good sniper rifles

SWEP.ViewModelFOV			= 60
SWEP.ViewModelFlip			= false
SWEP.UseHands			= true
SWEP.ViewModel 			= "models/vuthakral/halo/weapons/c_hum_needler.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_needler.mdl"
SWEP.ShowWorldModel         = true
SWEP.VMPos 				= Vector(0, 2, -1)

SWEP.Spawnable					= true
SWEP.AdminSpawnable				= true

SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)

SWEP.Primary.Sound				= Sound("drc.needler_fire")		-- script that calls the primary fire sound
SWEP.Primary.RPM				= 510		-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 24		-- Size of a clip
SWEP.Primary.DefaultClip		= 120		-- Bullets you start with
SWEP.Primary.KickUp				= 0.2		-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.0		-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.01		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= true		-- Automatic/Semi Auto
SWEP.Primary.Ammo				= "smg1"	-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets

SWEP.ShellTime					= .5

SWEP.Secondary.ScopeZoom		= 5	
SWEP.Secondary.UseACOG			= false 	-- Choose one scope type
SWEP.Secondary.UseMilDot		= false		-- I mean it, only one	
SWEP.Secondary.UseSVD			= false		-- If you choose more than one, your scope will not show up at all
SWEP.Secondary.UseParabolic		= false	
SWEP.Secondary.UseElcan			= false
SWEP.Secondary.UseGreenDuplex	= false	
SWEP.Secondary.UseAimpoint		= false
SWEP.Secondary.UseMatador		= false

SWEP.Secondary.IronFOV			= 70	-- How much you 'zoom' in. Less is more!

SWEP.data 						= {}
SWEP.data.ironsights			= 1
SWEP.ScopeScale 				= 1.2
SWEP.ReticleScale 				= 0.6

SWEP.Primary.NumShots			= 1			-- how many bullets to shoot per trigger pull
SWEP.Primary.Damage				= 20		-- base damage per bullet
SWEP.Primary.Spread				= 0.01		-- define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy 		= 0.0063		-- ironsight accuracy, should be the same for shotguns

SWEP.Primary.SpreadMultiplierMax 	= 3.8 		--How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 1.49 	--What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 5.6012 		--How much the spread recovers, per second.

-- enter iron sight info and bone mod info below

SWEP.IronSightsPos 		= Vector(-6.5, 2, -2)
SWEP.IronSightsAng 		= Vector(0, 0, 0)

SWEP.RunSightsPos = Vector(7.742, -5.271, 2.709)
SWEP.RunSightsAng = Vector(-11.58, 31.843, 0)

SWEP.AmmoTypeStrings	= {["smg1"] = "Explosive Crystalline Shards"}
SWEP.Type = "Needler"

SWEP.DisableChambering = true

SWEP.MuzzleFlashEffect = "needler_muzzle"
SWEP.TracerName 	   = "needler_beam"
SWEP.TracerCount       = 1
SWEP.ImpactEffect      = "needle_rifle_beam_hit"

DEFINE_BASECLASS( SWEP.Base )

function SWEP:PreDrawViewModel(vm, wep, ply)

	local ammo = ply:GetActiveWeapon():Clip1()

	local AmmoCL = Lerp(FrameTime() * 25, AmmoCL or ammo, ammo)

	
	if ply != LocalPlayer() then return end


	if self.Primary.Ammo != nil && vm:GetPoseParameter("drc_ammo") != nil then
		vm:SetPoseParameter("drc_ammo", AmmoCL / self.Primary.ClipSize)
	end

	return false
end
 
