if CLIENT then
SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/entities/drchalo_dmr" )
	
	killicon.Add( "tfa_revival_ht_dmr", "vgui/entities/drchalo_dmr", color_white )
end
-- Variables that are used on both client and server
SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "Having been adopted in 2512, the M392 is a marksman rifle in use by all branches of the UNSC."
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory High Tier"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= false
SWEP.AdminSpawnable 				= false
SWEP.PrintName						= "M394 High Tier"		-- Weapon name (Shown on HUD)

SWEP.Slot                           = 3             -- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos                        = 59            -- Position in the slot

SWEP.ViewModelFOV			        = 65
SWEP.ViewModelFlip			        = false
SWEP.UseHands			            = true
SWEP.ViewModel 			            = "models/vuthakral/halo/weapons/c_hum_dmr.mdl"
SWEP.WorldModel			            = "models/vuthakral/halo/weapons/w_dmr.mdl"
SWEP.ShowWorldModel                 = true
SWEP.Base 					        = "tfa_bash_base"

SWEP.Primary.Sound			        = Sound("drc.dmr_fire")
SWEP.Primary.SilencedSound 	        = Sound("")
SWEP.Primary.RPM					= 350			-- This is in Rounds Per Minute
SWEP.Primary.ClipSize				= 15			-- Size of a clip
SWEP.Primary.ClipSizeFmj		    = 12
SWEP.Primary.DefaultClip			= 1200	-- Bullets you start with
SWEP.Primary.HullSize               = 2
SWEP.Primary.Knockback              = 0
SWEP.Primary.KickUp					= 0.05		-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0.05		-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.0		-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.2 		--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.MuzzleAttachment               = "2"
SWEP.Secondary.CanBash              = true -- set to false to disable bashing
SWEP.Secondary.BashDamage           = 300 -- Melee bash damage
SWEP.Secondary.BashLength           = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay            = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType       = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd              = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt        = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.HoldType 						= "ar2"
SWEP.TracerName 					= "drc_halo_ar_bullet" --Change to a string of your tracer name,or lua effect if chosen

SWEP.Primary.SpreadMultiplierMax 	= 3		--How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 1 	--What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 1.5		--How much the spread recovers, per second.
SWEP.Primary.Automatic				= false		-- Automatic/Semi Auto
SWEP.Primary.Ammo					= "ar2"		-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets
SWEP.SelectiveFire					= true
SWEP.CanBeSilenced					= true

SWEP.Scoped 						= true
SWEP.ScopeScale                     = 0

SWEP.AmmoTypeStrings	            = {["ar2"] = "M118 7.62x51mm FMJ-AP"}
SWEP.Type                           = "Bullpup Designated Marksman Rifle"

SWEP.Secondary.ScopeZoom			= 8
SWEP.LuaShellEject 					= false

SWEP.BlowbackEnabled                = true
--SWEP.BlowbackVector               = Vector(0,1,0)
--SWEP.BlowbackAngle                = Angle(5, 0, 0)
SWEP.BlowbackAllowAnimation         = false -- Allow playing shoot animation with blowback?
SWEP.Blowback_Shell_Effect          = "ShellEject"
SWEP.Blowback_PistolMode            = true
SWEP.Blowback_Only_Iron             = false -- Only do blowback on ironsights
SWEP.BlowbackBoneMods               = {
    ["b_ophandle"] = { scale = Vector(1, 1, 1), pos = Vector(-2.5, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.Primary.NumShots	           = 1		--how many bullets to shoot per trigger pull
SWEP.Primary.Damage		           = 180	--base damage per bullet
SWEP.Primary.Spread		           = 0.01	--define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy          = 0.0001 -- ironsight accuracy, should be the same for shotguns

-- enter iron sight info and bone mod info below

SWEP.IronSightsPos = Vector(-5.396, -18.848, -1.277)
SWEP.IronSightsAng = Vector(0, -2.063, 0)
SWEP.RunSightsPos = Vector(7.742, -5.271, 2.709)
SWEP.RunSightsAng = Vector(-11.58, 31.843, 0)

SWEP.VElements = {
	["ammo_counter"] = { type = "Quad", bone = "b_gun", rel = "", pos = Vector(1.651, 0, 5.95), angle = Angle(0, -90, 90), size = 0.0032, draw_func = nil}
}

SWEP.WElements = {
	["ammo_counterW"] = { type = "Quad", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.04, 1.18, -5.4), angle = Angle(0, 90, -78.5), size = 0.0032, draw_func = nil}
}

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = { "halo_shortscope", "halo_longscope" }, order = 1 },
	[2] = { offset = { 0, 0 }, atts = { "halo_fmj","halo_damage_rof" }, order = 2 },
}

DEFINE_BASECLASS( SWEP.Base )

function SWEP:Initialize()
	BaseClass.Initialize( self )
	
	local ply = self:GetOwner()
	
	if CLIENT then
		self.VElements["ammo_counter"].draw_func = function( weapon )
			if self:Clip1() < 10 then
				draw.SimpleTextOutlined("0 ".. self:Clip1() .."", "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			else
				draw.SimpleTextOutlined(self:Clip1(), "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			end
		end
		
		self.WElements["ammo_counterW"].draw_func = function( weapon )
			if self:Clip1() < 10 then
				draw.SimpleTextOutlined("0".. self:Clip1() .."", "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			else
				draw.SimpleTextOutlined(self:Clip1(), "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			end
		end
	end
	
	if ply:EntIndex() == 0 then
	else
		if ply:GetModel() == "models/vuthakral/halo/custom/usp/sangheili_h3.mdl" then
			self.ViewModel = "models/vuthakral/halo/weapons/v_m7_h3.mdl"
		else
			self.ViewModel = "models/vuthakral/halo/weapons/c_hum_dmr.mdl"
		end
	end
end

if CLIENT then


function SWEP:DetectionHUD(trace)

    if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
    color = Color(255,0,0,255)

    else
    color = Color( 0, 161, 255, 255 )
    end
    
    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_br"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-25, ScrH()/2+25 - 50, 50, 50 )

    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_br_dyn"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-25, ScrH()/2+25 - 50, 50, 50 )

end

function SWEP:DrawHUD()

    self.CLOldNearWallProgress = self.CLOldNearWallProgress or 0
    cam.Start3D() --Workaround for vec:ToScreen()
    cam.End3D()

    self:DoInspectionDerma()

    self:DrawHUDAmmo()

    if self.IronSightsProgress > self.ScopeOverlayThreshold then

    if self.SightsDown == false then return end

	if self.Owner:InVehicle() then return end

    local Trace = {}
    Trace.start = self.Owner:GetShootPos()
    Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
    Trace.filter = { self.Owner, self.Weapon, 0 }
    Trace.mask = MASK_SHOT
    local tr = util.TraceLine(Trace)
    
    self:DetectionHUD(tr)  

    local Scoped        = true
    local ScopeMat      = "models/vuthakral/halo/HUD/scope_rifle.png"
    local ScopeBlur     = true
    local ScopeBGCol    = Color(0, 0, 0, 200)
    local IronFOV       = 50
    local ScopeScale    = 0.65
    local ScopeWidth    = 1
    local ScopeHeight   = 1
    local ScopeYOffset = -1

    local w = ScrW()
    local h = ScrH()

    local ratio = w/h
    
    local ss = 4 * ScopeScale
    local sw = ScopeWidth
    local sh = ScopeHeight
    
    local wi = w / 10 * ss
    local hi = h / 10 * ss

    local Q1Mat = ScopeMat
    local Q2Mat = Q2Mat
    local Q3Mat = Q3Mat
    local Q4Mat = Q4Mat
    
    local YOffset = -ScopeYOffset
    
    surface.SetDrawColor( ScopeBGCol )
    
    surface.DrawRect( 0, (h/2 - hi * sh) * YOffset, w/2 - hi / 2 * sw * 2, hi * 2 )
    surface.DrawRect( w/2 + hi * sw, (h/2 - hi * sh) * YOffset, w/2 + wi * sw, hi * 2 )
    surface.DrawRect( 0, 0, w * ss, h / 2 - hi * sh )
    surface.DrawRect( 0, (h/2 + hi * sh) * YOffset, w * ss, h / 1.99 - hi * sh )
    

    surface.SetDrawColor(Color(0, 0, 0, 255))
	surface.SetMaterial(Material("models/vuthakral/halo/HUD/scope_elements/br_e1"))
	surface.DrawTexturedRectUV( wi * 1.4, h/2 * 1.1, hi * sw, hi / 2, 0, 0, 1, 1 )
	
	surface.SetMaterial(Material("models/vuthakral/halo/HUD/scope_elements/br_e2"))
	surface.DrawTexturedRectUV( w/2 - hi / 2 * 1.65, h/2 - (hi / 2 * 0.2), hi * sw, hi / 2 * 0.4, 0, 0, 1, 1 )
	surface.DrawTexturedRectUV( w/2 - hi / 6, h/2 - (hi / 2 * 0.2), hi * sw, hi / 2 * 0.4, 1, 0, 0, 1 )
	
	surface.SetMaterial(Material("models/vuthakral/halo/HUD/scope_elements/br_e3"))
	surface.DrawTexturedRectUV( w/2 - hi / 16, hi * 1.775, wi / 14, hi, 0, 1, 1, 0 )
	surface.DrawTexturedRectUV( w/2 - hi / 17, hi * 1.1, wi / 14, hi, 1, 0, 0, 1 )

    if ScopeCol != nil then
        surface.SetDrawColor( ScopeCol )
    else
        surface.SetDrawColor( Color(0, 0, 0, 255) )
    end
    
    if Q1Mat == nil then
        surface.SetMaterial(Material("sprites/scope_arc"))
    else 
        surface.SetMaterial(Material(Q1Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 1, 1, 0, 0 )
    
    if Q2Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q2Mat))
    end
    surface.DrawTexturedRectUV( w / 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 0, 1, 1, 0 )
    
    if Q3Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q3Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, h/2, hi * sw, hi * sh, 1, 0, 0, 1 )
    
    if Q4Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q4Mat))
    end
    surface.DrawTexturedRectUV( w/2, h/2, hi * sw, hi * sh, 0, 0, 1, 1 )

    end
end
end