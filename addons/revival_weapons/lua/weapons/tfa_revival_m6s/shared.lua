if CLIENT then
SWEP.WepSelectIcon            =     surface.GetTextureID( "vgui/entities/drchalo_m6s" )
    
killicon.Add( "tfa_revival_m6s", "vgui/entities/drchalo_m6s", color_white )
end

SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "Created for revival using vuths models!"
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= true
SWEP.AdminSpawnable 				= true

SWEP.PistolSlide = 1
SWEP.PrintName 						= "M6-S Pistol"
SWEP.Slot							= 2				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 50			-- Position in the slot
SWEP.DrawAmmo						= true			-- Should draw the default HL2 ammo counter if enabled in the GUI.
SWEP.DrawWeaponInfoBox				= false			-- Should draw the weapon info box
SWEP.BounceWeaponIcon   			= false			-- Should the weapon icon bounce?
SWEP.AutoSwitchTo					= true			-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom					= true			-- Auto switch from if you pick up a better weapon
SWEP.Weight							= 30			-- This controls how "good" the weapon is for autopickup.
SWEP.ThirdPersonReloadDisable		= false 		--Disable third person reload?  True disables.

SWEP.UseHands			= true
SWEP.ViewModel 			= "models/vuthakral/halo/weapons/c_hum_m6s.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_m6s.mdl"
SWEP.ViewModelFOV 					= 75
SWEP.HoldType 						= "pistol"

SWEP.Scoped 						= false

SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize 				= 12
SWEP.Primary.DefaultClip 			= 40


SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName 				  = "drc_halo_pistol_bullet_sil" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 				  = 1 --0 disables, otherwise, 1 in X chance
SWEP.HoldType 						= "revolver"

SWEP.Primary.Sound 					= Sound("drc.m6s_fire")
SWEP.Primary.Ammo 					= "pistol"
SWEP.Primary.Automatic 				= false
SWEP.Primary.RPM 					= 850
SWEP.Primary.Damage 				= 230
SWEP.Primary.HullSize = 3
SWEP.Primary.Knockback = 0
SWEP.Primary.NumShots 				= 1
SWEP.Primary.Spread					= .02					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .0001					-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire					= false 					--Allow selecting your firemode?


SWEP.Primary.KickUp					= 0.01				-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0.01					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.01			-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.41 					--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 4.8 					--How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 0.8 					--What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 4.5 					--How much the spread recovers, per second.

SWEP.Secondary.IronFOV 				= 70 					--Ironsights FOV (90 = same)
SWEP.BoltAction 					= false 				--Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 					--How long do we remain in ironsights after shooting?

SWEP.IronSightsPos 					= Vector(-4, -0, 1.41)
SWEP.IronSightsAng 					= Vector(0, 0.1, 0)

SWEP.IronSightsPos_KFA 				= Vector(-2.33, -1, .7)
SWEP.IronSightsAng_KFA 				= Vector(0.85, 0.43, 0)

SWEP.IronSightsPos_COMP 			= Vector(-2.42, -0, 1.55)
SWEP.IronSightsAng_COMP 			= Vector(-0.7, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(7.519, -9.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.Primary.Range 					= 16*164.042*3 			-- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 					-- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.
SWEP.Tracer							= 0
SWEP.TracerCount 					= 1

SWEP.BlowbackEnabled 				= false
--SWEP.BlowbackVector 				= Vector(0,1,0)
--SWEP.BlowbackAngle          = Angle(5, 0, 0)
SWEP.BlowbackAllowAnimation = true -- Allow playing shoot animation with blowback?
SWEP.Blowback_Shell_Effect 			= "ShellEject"
SWEP.Blowback_PistolMode 			= true
SWEP.Blowback_Only_Iron     = false -- Only do blowback on ironsights
SWEP.BlowbackBoneMods 				= {
	["slide"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}


SWEP.Primary.MaxPenetration			= 4

SWEP.LuaShellEject 					= true
SWEP.LuaShellEjectDelay 			= 0
SWEP.LuaShellEffect 				= "ShellEject" 	--Defaults to blowback

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = {"halo_fmj","halo_ext_mag"}, order = 1 },
}

DEFINE_BASECLASS( SWEP.Base )

function SWEP:PreDrawViewModel(vm, wep, ply)
	
	local ammo = ply:GetActiveWeapon():Clip1()
	
	if ply != LocalPlayer() then return end

	if ammo == 0 then
		vm:SetPoseParameter("drc_emptymag", 0) -- One is Fully Closed
	else
		vm:SetPoseParameter("drc_emptymag", 1) -- One is Fully Closed
	end

	return false
end
 

