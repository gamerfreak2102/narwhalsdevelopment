SWEP.Author 						= "ChaosNarwhal | Meliodas"
SWEP.Contact 						= "Discord: ChaosNarwhal#9953"
SWEP.Purpose 						= "Kill some covies and stop reading!"
SWEP.Base 							= "tfa_rebirth_base"
SWEP.Category 						= "Project Reclamation"
SWEP.Spawnable 						= false
SWEP.AdminSpawnable 				= true

SWEP.Slot							= 2				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 50			-- Position in the slot

SWEP.PrintName 						= "Commanders Blaster"

SWEP.ViewModel						= "models/chaosnarwhal/weapons/m247h/v_models/v_m247h.mdl"
SWEP.VMPos = Vector(0, 0, -5) -- The viewmodel positional offset, constantly.  Subtract this from any other modifications to viewmodel position.
SWEP.WorldModel						= "models/chaosnarwhal/weapons/m247h/w_models/w_m247h.mdl"
SWEP.ViewModelFOV 					= 60
SWEP.HoldType 						= "physgun"

SWEP.Scoped 						= false
SWEP.UseHands = true
SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize 				= 200
SWEP.Primary.DefaultClip 			= 600

SWEP.Primary.Sound 					= Sound("Weapon_chaosnarwhal_m247h.Single")
SWEP.Primary.Ammo 					= "ar2"
SWEP.Primary.Automatic 				= true
SWEP.Primary.RPM 					= 1000
SWEP.Primary.Damage 				= 250
SWEP.Primary.Knockback = 0
SWEP.Primary.HullSize = 1
SWEP.Primary.NumShots 				= 1
SWEP.Primary.Spread					= .022					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .01	-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire 					= false
	
SWEP.Primary.KickUp					= 0					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0					-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0 	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 1 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 0.5 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 3 --How much the spread recovers, per second.

SWEP.Secondary.IronFOV 				= 70 --Ironsights FOV (90 = same)
SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos 					= Vector(-3.34, -2, 0)
SWEP.IronSightsAng 					= Vector(1.5, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(10.519, -8.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.Primary.Range 					= 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.
SWEP.LuaShellEject 					= false
SWEP.TracerName 					= "effect_chaingun_tracer" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 					= 1 --0 disables, otherwise, 1 in X chance
SWEP.ImpactEffect 					= "halo_sniper_impact"--Impact Effect
SWEP.BlowbackEnabled 				= true
SWEP.BlowbackVector 				= Vector(0,-2.0,0)
SWEP.Blowback_Shell_Effect 			= "ShellEject"
SWEP.Blowback_PistolMode 			= true
SWEP.BlowbackBoneMods 				= {
	--["Bolt"] = { scale = Vector(1, 1, 1), pos = Vector(-3.537, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.Offset = { --Procedural world model animation, defaulted for CS:S purposes.
        Pos = {
        Up = 2,
        Right = 1,
        Forward = 3,
        },
        Ang = {
        Up = -15,
        Right = -51,
        Forward = 170
        },
		Scale = 1.25
}

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = { "halo_sprintattack" }, order = 1 },
	[2] = { offset = { 0, 0 }, atts = { "halo_shredder" }, order = 2 }
}

if SERVER then
function SWEP:Deploy()

    local ply = self:GetOwner()
    
    local AuthorizedUser = AuthorizedUser or {}

    local AuthorizedUser = {
        ["76561198056866810"] = true, --Nevada
        ["76561198096723612"] = true --Jesse
    }

    local plyid = ply:SteamID64()

    if not AuthorizedUser[plyid] then
        ply:StripWeapon("tfa_m247h_22")
    end

end
end 
