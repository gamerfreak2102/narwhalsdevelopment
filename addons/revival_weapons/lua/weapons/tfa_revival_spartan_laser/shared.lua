if CLIENT then
SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/entities/drchalo_gnr" )
	
	killicon.Add( "tfa_revival_spartan_laser", "vgui/entities/drchalo_gnr", color_white )
end

SWEP.Category					= "Project Revival Armory"
SWEP.Author						= "ChaosNarwhal"
SWEP.Contact					= ""
SWEP.Purpose					= ""
SWEP.Instructions				= ""
SWEP.MuzzleAttachment			= "2" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment		= "0" 	-- Should be "2" for CSS models or "1" for hl2 models
SWEP.PrintName					= "W/AV M6 G/GNR"		-- Weapon name (Shown on HUD)
SWEP.Slot						= 4				-- Slot in the weapon selection menu
SWEP.SlotPos					= 6			-- Position in the slot
SWEP.DrawAmmo					= true		-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox			= false		-- Should draw the weapon info box
SWEP.BounceWeaponIcon   		= false	-- Should the weapon icon bounce?
SWEP.DrawCrosshair				= true		-- Set false if you want no crosshair from hip
SWEP.Weight						= 30			-- Rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo				= true		-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom				= true		-- Auto switch from if you pick up a better weapon
SWEP.XHair						= true	-- Used for returning crosshair after scope. Must be the same as DrawCrosshair
SWEP.BoltAction					= false		-- Is this a bolt action rifle?
SWEP.HoldType 					= "rpg"		-- how others view you carrying the weapon
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg and crossbow make for good sniper rifles

SWEP.ViewModelFOV			= 60
SWEP.ViewModelFlip			= false
SWEP.UseHands			= true
SWEP.ViewModel			= "models/vuthakral/halo/weapons/c_hum_gnr.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_gnr.mdl"
SWEP.ShowWorldModel         = true
SWEP.Base 				= "tfa_bash_base"

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.HullSize = 3
SWEP.Primary.Knockback = 0
SWEP.Primary.Sound             	= nil
SWEP.Primary.Recoil            	= 5
SWEP.Primary.Damage            	= 100000
SWEP.Primary.Ammo              	= "AR2AltFire"
SWEP.Primary.ClipSize        	= 4
SWEP.Primary.DefaultClip       	= 4
SWEP.Primary.Automatic         	= false
SWEP.Primary.BlastRadius		= 50
SWEP.DamageType = bit.bor( DMG_BLAST, DMG_SHOCK )
SWEP.Primary.RPM = 60/3

SWEP.Primary.KickUp = 1
SWEP.Primary.KickDown = 0.8
SWEP.Primary.KickHorizontal = 0.6

SWEP.AmmoTypeStrings	= {["AR2AltFire"] = "Series 6971 Battery cell "}
SWEP.Type = "Spartan Laser"

SWEP.DisableChambering = true

SWEP.Scoped = true
SWEP.Secondary.ScopeZoom			= 6
SWEP.ScopeScale = 0

-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets
SWEP.SelectiveFire		= false
SWEP.CanBeSilenced		= false

SWEP.Primary.Spread		= 0.001	--define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = 0.001 -- ironsight accuracy, should be the same for shotguns

-- enter iron sight info and bone mod info below

SWEP.IronSightsPos = Vector(-5.512, -20, 3.569)
SWEP.IronSightsAng = Vector(-0.791, -0.424, 0)
SWEP.RunSightsPos = Vector(0, 0, 8.413)
SWEP.RunSightsAng = Vector(-17.522, 0.004, 0)

SWEP.VElements = {
	["bar_back"] = { type = "Model", model = "models/props_phx/construct/wood/wood_boardx1.mdl", bone = "gun", rel = "", pos = Vector(9.472, -2.763, 2.707), angle = Angle(0, 90, 90), size = Vector(0.021, 0.059, 0.021), color = Color(0, 0, 0, 212), surpresslightning = true, material = "models/debug/debugwhite", skin = 0, bodygroup = {} },
	["bar_back+"] = { type = "Model", model = "models/props_phx/construct/wood/wood_boardx1.mdl", bone = "gun", rel = "bar_back", pos = Vector(0.524, -0.01, -0), angle = Angle(0, -90, 90), size = Vector(0.009, 0.009, 0.009), color = Color(255, 255, 255, 255), surpresslightning = true, material = "models/debug/debugwhite", skin = 0, bodygroup = {} }
}

SWEP.TracerName 				  = "drc_halo_gnr_laser" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 				  = 1 --0 disables, otherwise, 1 in X chance

DEFINE_BASECLASS( SWEP.Base )

function SWEP:PrimaryAttack(...)

	if not self:CanPrimaryAttack() then return end
	
	if self.Owner:KeyPressed(IN_ATTACK) then
		self.Weapon:EmitSound("drc.gnr_charge")
		timer.Create("laserToggleShoot"..self.Owner:SteamID(), 2.5, 1, function()
			if (IsValid(self.Owner)) then
				if IsFirstTimePredicted() and not ( sp and CLIENT ) then
					BaseClass.PrimaryAttack( self )
						self:EmitSound("drc.gnr_charge")
					end
				end
				--return BaseClass.PrimaryAttack(self,...)
			end)
	end

end

DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

function SWEP:Think2(...) -- We're overriding Think2 without touching the main think function, which is called from there anyway
    BaseClass.Think2(self, ...) -- THE MOST IMPORTANT LINE! It calls the Think2 function of the parent class, which is the base itself
   	if self.Owner:KeyPressed(IN_ATTACK2) and !self.Owner:KeyDown(IN_SPEED) and self:GetIronSights()==true then
		self.Weapon:EmitSound("vuthakral/halo/weapons/spnkr/zoom_in.wav")
	end
	
	if self.Owner:KeyReleased(IN_ATTACK2) and !self.Owner:KeyDown(IN_SPEED) and self:GetIronSights()==false then
		self.Weapon:EmitSound("vuthakral/halo/weapons/spnkr/zoom_out.wav")
	end
	
	if self.Owner:KeyReleased(IN_ATTACK) then
		timer.Remove("laserToggleShoot"..self.Owner:SteamID())
		self.Weapon:StopSound("drc.gnr_charge")
	end
	
	if self.Owner:KeyDown(IN_SPEED) then
		timer.Remove("laserToggleShoot"..self.Owner:SteamID())
		self.Weapon:StopSound("drc.gnr_charge")
	end

    if CLIENT then
		if self.Owner:KeyDown(IN_ATTACK) and self.Weapon:CanPrimaryAttack() then
			if self.Weapon:Clip1() == 0 then return end
				chargeint = chargeint + 0.45
			else
				chargeint = 0
		end

	local col = LerpVector(chargeint/100, Vector(255, 255, 255), Vector(255, 0, 0))
	local colx = col.x
	local coly = col.y
	local colz = col.z
	
	self.VElements["bar_back+"].size = Vector(.008, .01, Lerp(chargeint, 0, 0.3))
	self.VElements["bar_back+"].color = Color( colx, coly, colz )

    end

	
end



if CLIENT then

function SWEP:DetectionHUD(trace)

    if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
    color = Color(255,0,0,255)

    else
    color = Color( 0, 161, 255, 255 )
    end
    
    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_sl"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-25, ScrH()/2+25 - 50, 50, 50 )

end



function SWEP:DrawHUD()

    if self.Owner:InVehicle() then return end

    local Trace = {}
    Trace.start = self.Owner:GetShootPos()
    Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
    Trace.filter = { self.Owner, self.Weapon, 0 }
    Trace.mask = MASK_SHOT
    local tr = util.TraceLine(Trace)
    
    self:DetectionHUD(tr)  

    self.CLOldNearWallProgress = self.CLOldNearWallProgress or 0
    cam.Start3D() --Workaround for vec:ToScreen()
    cam.End3D()

    self:DoInspectionDerma()

    self:DrawHUDAmmo()

    if self.IronSightsProgress > self.ScopeOverlayThreshold then

    if self.SightsDown == false then return end

    local Scoped        = true
    local ScopeMat      = "models/vuthakral/halo/HUD/scope_rifle.png"
    local ScopeBlur     = true
    local ScopeBGCol    = Color(0, 0, 0, 200)
    local IronFOV       = 200
    local ScopeScale    = 0.65
    local ScopeWidth    = 1
    local ScopeHeight   = 1
    local ScopeYOffset = -1

    local w = ScrW()
    local h = ScrH()
    
    local ratio = w/h
    
    local ss = 4 * ScopeScale
    local sw = ScopeWidth
    local sh = ScopeHeight
    
    local wi = w / 10 * ss
    local hi = h / 10 * ss
    
    local Q1Mat = ScopeMat
    local Q2Mat = Q2Mat
    local Q3Mat = Q3Mat
    local Q4Mat = Q4Mat
    
    local YOffset = -ScopeYOffset
    
    surface.SetDrawColor( ScopeBGCol )
    
    surface.DrawRect( 0, (h/2 - hi * sh) * YOffset, w/2 - hi / 2 * sw * 2, hi * 2 )
    surface.DrawRect( w/2 + hi * sw, (h/2 - hi * sh) * YOffset, w/2 + wi * sw, hi * 2 )
    surface.DrawRect( 0, 0, w * ss, h / 2 - hi * sh )
    surface.DrawRect( 0, (h/2 + hi * sh) * YOffset, w * ss, h / 1.99 - hi * sh )
    
    if ScopeCol != nil then
        surface.SetDrawColor( ScopeCol )
    else
        surface.SetDrawColor( Color(0, 0, 0, 255) )
    end
    
    if Q1Mat == nil then
        surface.SetMaterial(Material("sprites/scope_arc"))
    else 
        surface.SetMaterial(Material(Q1Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 1, 1, 0, 0 )
    
    if Q2Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q2Mat))
    end
    surface.DrawTexturedRectUV( w / 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 0, 1, 1, 0 )
    
    if Q3Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q3Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, h/2, hi * sw, hi * sh, 1, 0, 0, 1 )
    
    if Q4Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q4Mat))
    end
    surface.DrawTexturedRectUV( w/2, h/2, hi * sw, hi * sh, 0, 0, 1, 1 )

    end

end




end
