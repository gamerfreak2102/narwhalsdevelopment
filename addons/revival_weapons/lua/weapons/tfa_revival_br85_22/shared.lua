AddCSLuaFile()

if CLIENT then
SWEP.WepSelectIcon        = surface.GetTextureID( "vgui/entities/drchalo_br" )
    
    killicon.Add( "tfa_revival_br85_22", "vgui/entities/drchalo_br", color_white )
end

SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "Created for revival using vuths models!"
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= false
SWEP.AdminSpawnable 				= false

SWEP.Slot							= 3				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 59			-- Position in the slot

SWEP.PrintName 						= "BR85 Service Rifle"

SWEP.UseHands						= true
SWEP.ViewModel 						= "models/vuthakral/halo/weapons/c_hum_br85hb.mdl"
SWEP.WorldModel						= "models/vuthakral/halo/weapons/w_br85hb.mdl"
SWEP.ViewModelFOV 					= 70
SWEP.HoldType 						= "ar2"
SWEP.IronSightHoldTypeOverride  = "ar2" -- Defaults to nothing (disabled)

SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize 				= 36
SWEP.Primary.DefaultClip 			= 288


SWEP.Secondary.CanBash              = true -- set to false to disable bashing
SWEP.Secondary.BashDamage           = 700 -- Melee bash damage
SWEP.Secondary.BashLength           = 100 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay            = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType       = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd              = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt        = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName 					= "drc_halo_ar_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 					= 1 --0 disables, otherwise, 1 in X chance
SWEP.MuzzleAttachment               = "2"
SWEP.MuzzleFlashEnabled             = true

SWEP.Primary.Sound 					= Sound("drc.br55_h2a_fire")
SWEP.Primary.Ammo 					= "ar2"
SWEP.Primary.Automatic 				= true
SWEP.Primary.RPM 					= 750
SWEP.Primary.RPM_Burst 				= 995
SWEP.Primary.Damage 				= 250
SWEP.Primary.NumShots 				= 1
SWEP.Primary.HullSize 				= 3
SWEP.Primary.Knockback 				= 0
SWEP.Primary.Spread					= .008					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .001		-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire					= true 					--Allow selecting your firemode?
SWEP.OnlyBurstFire					= true				--No auto, only burst/single?

SWEP.Primary.KickUp					= 0					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0					-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0 	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 1.5 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 0.2 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 4.5 --How much the spread recovers, per second.

SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos 					= Vector(-2.828, -4.5, 0.055)
SWEP.IronSightsAng 					= Vector(0, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(10.519, -8.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.VMPos 							= Vector(0,0,-0.85)

SWEP.Primary.Range 					= 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.


SWEP.TracerCount 					= 1 --0 disables, otherwise, 1 in X chance
SWEP.Secondary.ScopeZoom			= 4
SWEP.Scoped 						= true
SWEP.ScopeScale                     = 0

SWEP.BlowbackEnabled                = true
--SWEP.BlowbackVector               = Vector(0,1,0)
--SWEP.BlowbackAngle                = Angle(5, 0, 0)
SWEP.BlowbackAllowAnimation         = true -- Allow playing shoot animation with blowback?
SWEP.Blowback_Shell_Effect          = "ShellEject"
SWEP.Blowback_PistolMode            = true
SWEP.Blowback_Only_Iron             = false -- Only do blowback on ironsights
SWEP.BlowbackBoneMods                   = {
    ["b_ophandle"] = { scale = Vector(1, 1, 1), pos = Vector(-2.5, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.Attachments = {
    [1] = { offset = { 0, 0 }, atts = {"halo_shredder","halo_ext_mag"}, order = 1 },
    [2] = { offset = { 0, 0 }, atts = {"halo_sprintattack"}, order = 1 },
}

SWEP.VElements = {
	["ammo_counter"] = { type = "Quad", bone = "b_gun", rel = "", pos = Vector(1.493, 0, 5.9), angle = Angle(0, -90, 45), size = 0.009, draw_func = nil},
	["ammo_counter_00"] = { type = "Quad", bone = "b_gun", rel = "", pos = Vector(1.493, 0, 5.9), angle = Angle(0, -90, 45), size = 0.009, draw_func = nil}
}

SWEP.WElements = {
	["ammo_counterW"] = { type = "Quad", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.8, 1.175, -5.77), angle = Angle(-1, 85, -125), size = 0.009, draw_func = nil}
}


DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

function SWEP:Initialize()
	BaseClass.Initialize( self )
	
	local ply = self:GetOwner()

	if CLIENT then
		self.VElements["ammo_counter"].draw_func = function( weapon )
			if self:Clip1() < 10 then
				draw.SimpleText("0".. self:Clip1() .."", "343_ammocounter", 0, 8.5, Color(18,18,18,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
			else
				draw.SimpleText(self:Clip1(), "343_ammocounter", 0, 8.5, Color(18,18,18,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
			end
		end
		
		self.VElements["ammo_counter_00"].draw_func = function( weapon )
			draw.SimpleTextOutlined("00", "343_ammocounter", 0, 8.5, Color(18,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 2, Color(115, 186, 214,150))
		end
		
		self.WElements["ammo_counterW"].draw_func = function( weapon )
			if self:Clip1() < 10 then
				draw.SimpleTextOutlined("0".. self:Clip1() .."", "343_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			else
				draw.SimpleTextOutlined(self:Clip1(), "343_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			end
		end
	end
end

if SERVER then
function SWEP:Deploy()

    local ply = self:GetOwner()
    
    local AuthorizedUser = AuthorizedUser or {}

    local AuthorizedUser = {
        ["76561198079227213"] = true, --Arthur
        ["76561198096723612"] = true --Jesse
    }

    local plyid = ply:SteamID64()

    if not AuthorizedUser[plyid] then
        ply:StripWeapon("tfa_revival_br85_22")
    end

end
end

if CLIENT then


function SWEP:DetectionHUD(trace)

    if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
    color = Color(255,0,0,255)

    else
    color = Color( 0, 161, 255, 255 )
    end
    
    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_br"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-25, ScrH()/2+25 - 50, 50, 50 )

    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_br_dyn"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-25, ScrH()/2+25 - 50, 50, 50 )

end

function SWEP:DrawHUD()

     if self.Owner:InVehicle() then return end

    local Trace = {}
    Trace.start = self.Owner:GetShootPos()
    Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
    Trace.filter = { self.Owner, self.Weapon, 0 }
    Trace.mask = MASK_SHOT
    local tr = util.TraceLine(Trace)
    
    self:DetectionHUD(tr)  

    self.CLOldNearWallProgress = self.CLOldNearWallProgress or 0
    cam.Start3D() --Workaround for vec:ToScreen()
    cam.End3D()

    self:DoInspectionDerma()

    self:DrawHUDAmmo()

    if self.IronSightsProgress > self.ScopeOverlayThreshold then

    if self.SightsDown == false then return end

    local Scoped        = true
    local ScopeMat      = "models/vuthakral/halo/HUD/scope_rifle.png"
    local ScopeBlur     = true
    local ScopeBGCol    = Color(0, 0, 0, 200)
    local IronFOV       = 50
    local ScopeScale    = 0.65
    local ScopeWidth    = 1
    local ScopeHeight   = 1
    local ScopeYOffset = -1

    local w = ScrW()
    local h = ScrH()

    local ratio = w/h
    
    local ss = 4 * ScopeScale
    local sw = ScopeWidth
    local sh = ScopeHeight
    
    local wi = w / 10 * ss
    local hi = h / 10 * ss

    local Q1Mat = ScopeMat
    local Q2Mat = Q2Mat
    local Q3Mat = Q3Mat
    local Q4Mat = Q4Mat
    
    local YOffset = -ScopeYOffset
    
    surface.SetDrawColor( ScopeBGCol )
    
    surface.DrawRect( 0, (h/2 - hi * sh) * YOffset, w/2 - hi / 2 * sw * 2, hi * 2 )
    surface.DrawRect( w/2 + hi * sw, (h/2 - hi * sh) * YOffset, w/2 + wi * sw, hi * 2 )
    surface.DrawRect( 0, 0, w * ss, h / 2 - hi * sh )
    surface.DrawRect( 0, (h/2 + hi * sh) * YOffset, w * ss, h / 1.99 - hi * sh )
    

    surface.SetDrawColor(Color(0, 0, 0, 255))
    surface.SetMaterial(Material("models/vuthakral/halo/HUD/scope_elements/br_e1"))
    surface.DrawTexturedRectUV( wi * 1.4, h/2 * 1.1, hi * sw, hi / 2, 0, 0, 1, 1 )
    
    surface.SetMaterial(Material("models/vuthakral/halo/HUD/scope_elements/br_e2"))
    surface.DrawTexturedRectUV( w/2 - hi / 2 * 1.65, h/2 - (hi / 2 * 0.2), hi * sw, hi / 2 * 0.4, 0, 0, 1, 1 )
    surface.DrawTexturedRectUV( w/2 - hi / 6, h/2 - (hi / 2 * 0.2), hi * sw, hi / 2 * 0.4, 1, 0, 0, 1 )
    
    surface.SetMaterial(Material("models/vuthakral/halo/HUD/scope_elements/br_e3"))
    surface.DrawTexturedRectUV( w/2 - hi / 16, hi * 1.775, wi / 14, hi, 0, 1, 1, 0 )
    surface.DrawTexturedRectUV( w/2 - hi / 17, hi * 1.1, wi / 14, hi, 1, 0, 0, 1 )

    if ScopeCol != nil then
        surface.SetDrawColor( ScopeCol )
    else
        surface.SetDrawColor( Color(0, 0, 0, 255) )
    end
    
    if Q1Mat == nil then
        surface.SetMaterial(Material("sprites/scope_arc"))
    else 
        surface.SetMaterial(Material(Q1Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 1, 1, 0, 0 )
    
    if Q2Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q2Mat))
    end
    surface.DrawTexturedRectUV( w / 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 0, 1, 1, 0 )
    
    if Q3Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q3Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, h/2, hi * sw, hi * sh, 1, 0, 0, 1 )
    
    if Q4Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q4Mat))
    end
    surface.DrawTexturedRectUV( w/2, h/2, hi * sw, hi * sh, 0, 0, 1, 1 )

    end
end
end