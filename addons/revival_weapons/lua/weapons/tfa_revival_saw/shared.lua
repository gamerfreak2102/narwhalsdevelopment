
if CLIENT then
    SWEP.WepSelectIcon				= surface.GetTextureID("vgui/entities/drchalo_saw")
    
    killicon.Add( "tfa_revival_saw", "vgui/entities/drchalo_saw", color_white )
end


SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Reclamation"
SWEP.Author                         = "ChaosNarwhal / Arthur"
SWEP.Contact                        = "ChaosNarwhal#9953"
SWEP.Purpose                        = "Misriah's latest squad automatic weapon in the line of LMGs, introduced to UNSC gunners in the mid-2500s"
SWEP.Base                           = "tfa_bash_base"
SWEP.Category                       = "Project Revival Armory"
SWEP.Manufacturer                   = "Misriah Armory"
SWEP.PrintName          			= "M739 LMG"
SWEP.Spawnable                      = true
SWEP.AdminSpawnable                 = true

SWEP.UseHands           			= true
SWEP.ViewModel          			= "models/vuthakral/halo/weapons/c_hum_saw.mdl"
SWEP.WorldModel         			= "models/vuthakral/halo/weapons/w_saw.mdl"
SWEP.VMPos              			= Vector(0, 0, -2)
SWEP.HoldType 						= "ar2"

SWEP.Slot							= 4				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 50			-- Position in the slot

SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize 				= 72
SWEP.Primary.DefaultClip 			= 588

SWEP.Secondary.CanBash            	= true -- set to false to disable bashing
SWEP.Secondary.BashDamage         	= 300 -- Melee bash damage
SWEP.Secondary.BashLength         	= 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          	= 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     	= DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            	= nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      	= true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName                   	= "drc_halo_pistol_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount                  	= 1 --0 disables, otherwise, 1 in X chance
SWEP.MuzzleAttachment             	= "2"
SWEP.MuzzleFlashEnabled           	= true

SWEP.Primary.Sound 				  	= Sound("drc.m739_fire_heavy")
SWEP.Primary.Ammo 					= "ar2"
SWEP.Primary.Automatic 				= true
SWEP.Primary.RPM 					= 1200
SWEP.Primary.Damage 				= 130
SWEP.Primary.NumShots 				= 1
SWEP.Primary.HullSize 				= 1
SWEP.Primary.Knockback 				= 0
SWEP.Primary.Spread					= 0.01					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .005	-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire 					= false
SWEP.OnlyBurstFire					= false --No auto, only burst?

SWEP.Primary.KickUp					= 0.15					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0.15					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.01				-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.15 	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 1.5 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 1 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 1.5 --How much the spread recovers, per second.

SWEP.Secondary.IronFOV 				= 60 --Ironsights FOV (90 = same)
SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos 					= Vector(-5.8, 0, -1.8)
SWEP.IronSightsAng 					= Vector(0, 0, -0.5)

SWEP.RunSightsPos 					= Vector(0, 0, 0)
SWEP.RunSightsAng 					= Vector(-11.869, 17.129, -16.056)

SWEP.InspectionPos 					= Vector(12.8, -10.653, -4.19)
SWEP.InspectionAng 					= Vector(36.389, 48.549, 22.513)
SWEP.ReticleScale 					= 0.7

SWEP.Primary.Range 					= 16*164.042*10 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.

SWEP.data 							= {}
SWEP.data.ironsights 				= 1 --Enable Ironsigh

SWEP.IronSightsPos 					= Vector(0, 0, 0)
SWEP.IronSightsAng 					= Vector(0, 0, 0)

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = { "halo_fmj_saw", "halo_ext_mag", "halo_rapidfire" }, order = 1 },
	[2] = { offset = { 0, 0 }, atts = { "halo_sprintattack"}, order = 2},
}