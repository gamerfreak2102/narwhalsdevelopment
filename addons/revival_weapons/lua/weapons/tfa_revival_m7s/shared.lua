if CLIENT then
    SWEP.WepSelectIcon             = surface.GetTextureID("vgui/entities/drchalo_m7s")
    
    killicon.Add( "tfa_revival_m7s", "vgui/entities/drchalo_m7s", color_white )
end

SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "Created for revival using vuths models!"
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= true
SWEP.AdminSpawnable 				= true

SWEP.PrintName 						= "M7/Caseless Submachine Gun SOCOM"
SWEP.Slot							= 2				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 50			-- Position in the slot
SWEP.DrawAmmo						= true			-- Should draw the default HL2 ammo counter if enabled in the GUI.
SWEP.DrawWeaponInfoBox				= false			-- Should draw the weapon info box
SWEP.BounceWeaponIcon   			= false			-- Should the weapon icon bounce?
SWEP.AutoSwitchTo					= true			-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom					= true			-- Auto switch from if you pick up a better weapon
SWEP.Weight							= 30			-- This controls how "good" the weapon is for autopickup.
SWEP.ThirdPersonReloadDisable		= false 		--Disable third person reload?  True disables.

SWEP.UseHands			= true
SWEP.ViewModel = 		"models/vuthakral/halo/weapons/c_hum_m7s.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_m7s.mdl"
SWEP.ViewModelFOV 					= 60
SWEP.HoldType 						= "smg"

SWEP.Scoped 						= false

SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

SWEP.DisableChambering 				= false
SWEP.Primary.ClipSize 				= 48
SWEP.Primary.DefaultClip 			= 500

SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName 				  = "drc_halo_smg_bullet_sil" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 				  = 1 --0 disables, otherwise, 1 in X chance


local ShootSound 					= Sound("drc.m7s_fire")
SWEP.Primary.Sound 					= Sound("drc.m7s_fire")
SWEP.Primary.Ammo 					= "smg1"
SWEP.Primary.Automatic 				= true
SWEP.Primary.RPM 					= 1000
SWEP.Primary.Damage 				= 115
SWEP.Primary.HullSize = 1
SWEP.Primary.Knockback = 0
SWEP.Primary.NumShots 				= 1
SWEP.Primary.Spread					= .001				--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .0001					-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire					= true 					--Allow selecting your firemode?
SWEP.DisableBurstFire				= false 				--Only auto/single?
SWEP.OnlyBurstFire					= false 				--No auto, only burst/single?
SWEP.DefaultFireMode 				= "" 					--Default to auto or whatev
SWEP.FireModeName 					= nil 					--Change to a text value to override it

SWEP.FireModes = {
	"Auto",
	"3Burst"
}

SWEP.Primary.KickUp					= 0.1					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0.1					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.01				-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.2 					--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 4.5 					--How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 0.7 					--What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 4.5 					--How much the spread recovers, per second.

SWEP.Secondary.IronFOV 				= 70 					--Ironsights FOV (90 = same)
SWEP.BoltAction 					= false 				--Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 					--How long do we remain in ironsights after shooting?

SWEP.IronSightsPos 					= Vector(-3.95, -4.02, -1.2)
SWEP.IronSightsAng 					= Vector(0, -2, 0)

SWEP.IronSightsPos_REFLEX 			= Vector(-1.945, -3, -0.075)
SWEP.IronSightsAng_REFLEX 			= Vector(0, 0.3, 0)

SWEP.IronSightsPos_M7REFLEX 		= Vector(-1.88, -3, -0.34)
SWEP.IronSightsAng_M7REFLEX 		= Vector(0, 0.3, 0)

SWEP.IronSightsPos_EVOS 			= Vector(-1.99, -1, -0.074)
SWEP.IronSightsAng_EVOS 			= Vector(-1.5, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(10.519, -8.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = {"halo_fmj","halo_ext_mag"}, order = 1 },
	[2] = { offset = { 0, 0 }, atts = {"halo_sprintattack"}, order = 1 },
}

SWEP.Primary.Range 					= 16*164.042*3 			-- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 					-- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.
SWEP.Tracer							= 0
SWEP.TracerCount 					= 2
SWEP.BlowbackEnabled 				= true
SWEP.BlowbackVector 				= Vector(-0.02,-3,0.1)
SWEP.Blowback_PistolMode 			= false

SWEP.Primary.MaxPenetration			= 4
SWEP.MuzzleAttachment				= "1" 					-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellAttachment				= "5" 					-- Should be "2" for CSS models or "shell" for hl2 models

SWEP.LuaShellEject 					= false
SWEP.LuaShellEjectDelay 			= 0
SWEP.LuaShellEffect 				= "ShellEject" 	--Defaults to blowback

