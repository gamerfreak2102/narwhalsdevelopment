if CLIENT then
SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/entities/drchalo_m45" )
	
	killicon.Add( "tfa_revival_ht_shotgun", "vgui/entities/drchalo_m45", color_white )
end
-- Variables that are used on both client and server
SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "The M45 is a short range tactical shotgun employed by the UNSC long predating the human-Covenant war."
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory High Tier"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= false
SWEP.AdminSpawnable 				= false
SWEP.PrintName						= "M45 High Tier"		-- Weapon name (Shown on HUD)

SWEP.Slot							= 3				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 59			-- Position in the slot


SWEP.ViewModelFOV			= 50
SWEP.ViewModelFlip			= false
SWEP.UseHands			= true
SWEP.ViewModel 			= "models/vuthakral/halo/weapons/c_hum_m45.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_m45.mdl"
SWEP.ShowWorldModel         = true
SWEP.Base 					= "tfa_bash_base"

SWEP.MuzzleAttachment             = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 200 -- Melee bash damage
SWEP.Secondary.BashLength         = 54 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName 				  = "drc_halo_shotgun_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 				  = 1 --0 disables, otherwise, 1 in X chance
SWEP.HoldType 						= "ar2"


SWEP.Primary.Sound				= Sound("drc.m90_fire")		-- script that calls the primary fire sound
SWEP.Primary.RPM				= 85		-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 6			-- Size of a clip
SWEP.Primary.DefaultClip		= 1200		-- Bullets you start with
SWEP.Primary.HullSize = 1
SWEP.Primary.Knockback = 0
SWEP.Primary.KickUp				= 0.2			-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.0		-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.01		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= false		-- Automatic/Semi Auto
SWEP.Primary.Ammo			= "buckshot"	-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets
SWEP.Shotgun 						= true
SWEP.FireModeName 					= "Pump Action"

SWEP.Primary.Range 					= 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.

SWEP.ShellTime					= .5

SWEP.Secondary.IronFOV			= 70		-- How much you 'zoom' in. Less is more! 

SWEP.Primary.NumShots			= 10			-- how many bullets to shoot per trigger pull
SWEP.Primary.Damage				= 140		-- base damage per bullet
SWEP.Primary.Spread				= 0.08		-- define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy 		= 0.08		-- ironsight accuracy, should be the same for shotguns

SWEP.Primary.SpreadMultiplierMax 	= 3.8 		--How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 1.49 	--What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 5.6012 		--How much the spread recovers, per second.

-- enter iron sight info and bone mod info below

SWEP.IronSightsPos 				= Vector(-4.542, -5.112, 0.238)
SWEP.IronSightsAng 				= Vector(0, -1.418, 0)
SWEP.RunSightsPos 				= Vector(7.742, -5.271, 2.709)
SWEP.RunSightsAng 				= Vector(-11.58, 31.843, 0)

SWEP.LuaShellEffect 				= "ShotgunShellEject"

SWEP.AmmoTypeStrings	= {["buckshot"] = "M296 SC"}
SWEP.Type = "Shotgun"

SWEP.DisableChambering = true

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = {"halo_sprintattack"}, order = 1 },
}