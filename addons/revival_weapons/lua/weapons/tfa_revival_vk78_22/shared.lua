if CLIENT then
    SWEP.WepSelectIcon             = surface.GetTextureID("vgui/hud/hr_swep_assault_rifle")
    
    killicon.Add( "tfa_hr_swep_assault_rifle", "vgui/hud/hr_swep_assault_rifle", color_white )
end

SWEP.Author                         = "ChaosNarwhal / Arthur"
SWEP.Contact                        = "ChaosNarwhal#9953"
SWEP.Purpose                        = "Created for revival using vuths models!"
SWEP.Base                           = "tfa_bash_base"
SWEP.Category                       = "Project Revival Armory"
SWEP.Manufacturer                   = "Misriah Armory"
SWEP.Spawnable                      = false
SWEP.AdminSpawnable                 = false

SWEP.Slot							= 3				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 50			-- Position in the slot

SWEP.PrintName 						= "VK78 Commando"

SWEP.UseHands           = true
SWEP.ViewModel          = "models/vuthakral/halo/weapons/c_hum_vk78.mdl"
SWEP.WorldModel         = "models/vuthakral/halo/weapons/w_vk78.mdl"
SWEP.ViewModelFOV 					= 70
SWEP.HoldType 						= "ar2"

SWEP.Scoped 						= false

SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize 				= 20
SWEP.Primary.DefaultClip 			= 352

SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 1000 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_SLASH -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName                     = "drc_halo_ar_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount                    = 1 --0 disables, otherwise, 1 in X chance

SWEP.Primary.Sound 					= Sound("drc.vk78_fire")
SWEP.Primary.Ammo 					= "ar2"
SWEP.Primary.Automatic 				= true
SWEP.Primary.RPM 					= 400
SWEP.Primary.Damage 				= 175
SWEP.Primary.NumShots 				= 1
SWEP.Primary.HullSize = 2
SWEP.Primary.Knockback = 0
SWEP.Primary.Spread					= .0003					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .0001	-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire 					= false
	
SWEP.Primary.KickUp					= 0					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0					-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0 	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 4.8 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 1.4 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 4.5 --How much the spread recovers, per second.

SWEP.Secondary.IronFOV 				= 70 --Ironsights FOV (90 = same)
SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos = Vector(-3.685, -4.454, 0.846)
SWEP.IronSightsAng = Vector(0.1, -2.45, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(10.519, -8.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.Primary.Range 					= 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.
SWEP.LuaShellEject 					= false
SWEP.Tracer							= 0
SWEP.TracerCount 					= 1

SWEP.BlowbackEnabled                = false
--SWEP.BlowbackVector               = Vector(0,1,0)
--SWEP.BlowbackAngle          = Angle(5, 0, 0)
SWEP.BlowbackAllowAnimation = false -- Allow playing shoot animation with blowback?
SWEP.Blowback_Shell_Effect          = "ShellEject"
SWEP.Blowback_PistolMode            = true
SWEP.Blowback_Only_Iron     = false -- Only do blowback on ironsights
SWEP.BlowbackBoneMods                   = {
    ["ophandle"] = { scale = Vector(1, 1, 1), pos = Vector(-2, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.VElements = {
    ["ammo_counter"] = { type = "Quad", bone = "b_gun", rel = "", pos = Vector(2.865, -1.532, 5.25), angle = Angle(0, -90, 90), size = 0.01, draw_func = nil},
    ["ammo_counter_scope"] = { type = "Quad", bone = "b_gun", rel = "", pos = Vector(6.5, 0.3, 7.8), angle = Angle(0, -90, 90), size = 0.00125, draw_func = nil},
    ["smartlink"] = { type = "Model", model = "models/vuthakral/halo/attachments/smart_vk78.mdl", bone = "b_gun", rel = "", pos = Vector(0, 0, 0), angle = Angle(0, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
    ["ammo_counterW"] = { type = "Quad", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.2, 2.825, -5), angle = Angle(0, 90, -78.5), size = 0.008, draw_func = nil}
}

SWEP.Attachments = {
    [1] = { offset = { 0, 0 }, atts = {"halo_fmj","halo_ext_mag"}, order = 1 },
    [2] = { offset = { 0, 0 }, atts = {"halo_sprintattack"}, order = 1 },
}

DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

function SWEP:Initialize()
    BaseClass.Initialize( self )
    
    local ply = self:GetOwner()
    
    if CLIENT then
        self.VElements["ammo_counter"].draw_func = function( weapon )
            if self:Clip1() < 10 then
                draw.SimpleTextOutlined("0".. self:Clip1() .."", "343_ammocounter", 0, 12.5, Color(251,240,90,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(151, 135, 41))
            else
                draw.SimpleTextOutlined(self:Clip1(), "343_ammocounter", 0, 12.5, Color(251,240,90,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(151, 135, 41))
            end
        end
        
        self.VElements["ammo_counter_scope"].draw_func = function( weapon )
            if self.SightsDown == true then
                if self:Clip1() < 10 then
                    draw.SimpleTextOutlined("0".. self:Clip1() .."", "343_ammocounter", 0, 12.5, Color(251,240,90,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(151, 135, 41))
                else
                    draw.SimpleTextOutlined(self:Clip1(), "343_ammocounter", 0, 12.5, Color(251,240,90,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(151, 135, 41))
                end
            else
                draw.SimpleTextOutlined("", "343_ammocounter", 0, 12.5, Color(251,240,90,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(151, 135, 41))
            end
        end
        
        self.WElements["ammo_counterW"].draw_func = function( weapon )
            if self:Clip1() < 10 then
                draw.SimpleTextOutlined("0".. self:Clip1() .."", "343_ammocounter", 0, 12.5, Color(251,240,90,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(151, 135, 41))
            else
                draw.SimpleTextOutlined(self:Clip1(), "343_ammocounter", 0, 12.5, Color(251,240,90,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(151, 135, 41))
            end
        end
    end
end

if SERVER then
function SWEP:Deploy()

    local ply = self:GetOwner()
    
    local AuthorizedUser = AuthorizedUser or {}

    local AuthorizedUser = {
        ["76561198056866810"] = true, --Nevada
        ["76561198096723612"] = true --Jesse
    }

    local plyid = ply:SteamID64()

    if not AuthorizedUser[plyid] then
        ply:StripWeapon("tfa_revival_vk78_22")
    end

end
end 

if CLIENT then


function SWEP:DetectionHUD(trace)

    if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
    color = Color(255,0,0,255)

    else
    color = Color( 0, 161, 255, 255 )
    end
    
    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_br"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-25, ScrH()/2+25 - 50, 50, 50 )

    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_br_dyn"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-25, ScrH()/2+25 - 50, 50, 50 )

end

function SWEP:DrawHUD()

     if self.Owner:InVehicle() then return end

    local Trace = {}
    Trace.start = self.Owner:GetShootPos()
    Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
    Trace.filter = { self.Owner, self.Weapon, 0 }
    Trace.mask = MASK_SHOT
    local tr = util.TraceLine(Trace)
    
    self:DetectionHUD(tr)  

    self.CLOldNearWallProgress = self.CLOldNearWallProgress or 0
    cam.Start3D() --Workaround for vec:ToScreen()
    cam.End3D()

    self:DoInspectionDerma()

    self:DrawHUDAmmo()
end

end
