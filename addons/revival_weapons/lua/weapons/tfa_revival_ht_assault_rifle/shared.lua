if CLIENT then
	SWEP.WepSelectIcon 			= surface.GetTextureID("vgui/entities/drchalo_ma37")
	
	killicon.Add( "tfa_revival_ht_assault_rifle", "vgui/entities/drchalo_ma37", color_white )
end

SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "Standard issue of the UNSC Army, the Ma37 was first introduced in 2437."
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory High Tier"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= false
SWEP.AdminSpawnable 				= false
SWEP.PrintName						= "MA37 High Tier"		-- Weapon name (Shown on HUD)

SWEP.Slot							= 3				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 59			-- Position in the slot


SWEP.ViewModelFOV					= 60
SWEP.ViewModelFlip					= false
SWEP.UseHands						= true
SWEP.ViewModel 						= "models/vuthakral/halo/weapons/c_hum_ma37.mdl"
SWEP.WorldModel						= "models/vuthakral/halo/weapons/w_ma37.mdl"
SWEP.ShowWorldModel         		= true
SWEP.Base 							= "tfa_bash_base"
SWEP.ViewModelBoneMods = {
	["b_Root"] = { scale = Vector(1, 1, 1), pos = Vector(0, -1.241, -1.267), angle = Angle(0, 0, 0) },
	["port"] = { scale = Vector(1, 1, 1), pos = Vector(0, -1.38, -1.339), angle = Angle(0, 0, 0) }
}

SWEP.Secondary.CanBash            	= true -- set to false to disable bashing
SWEP.Secondary.BashDamage         	= 300 -- Melee bash damage
SWEP.Secondary.BashLength         	= 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          	= 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     	= DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            	= nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      	= true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName 					= "drc_halo_ar_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 					= 1 --0 disables, otherwise, 1 in X chance
SWEP.MuzzleAttachment           	= "2"
SWEP.MuzzleFlashEnabled         	= true
SWEP.HoldType 						= "ar2"


SWEP.Primary.Sound					= Sound("drc.ma37_fire")
SWEP.Primary.SilencedSound 			= Sound("")
SWEP.Primary.RPM					= 950		-- This is in Rounds Per Minute
SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize				= 36		-- Size of a clip
SWEP.Primary.ClipSizeFmj			= 32
SWEP.Primary.ClipSize_ExtRifle 		= 40
SWEP.Primary.HullSize 				= 1
SWEP.Primary.Knockback 				= 0
SWEP.Primary.DefaultClip			= 5000	-- Bullets you start with
SWEP.Primary.KickUp					= 0.01				-- Maximum up recoil (rise)
SWEP.Primary.KickDown				= 0.0			-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.01	-- Maximum up recoil (stock)
SWEP.Primary.Automatic				= true		-- Automatic/Semi Auto
SWEP.Primary.Ammo					= "ar2"	-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets
SWEP.SelectiveFire					= true
SWEP.CanBeSilenced					= true


SWEP.AmmoTypeStrings				= {["ar2"] = "M118 7.62x51mm FMJ-AP"}
SWEP.Type 							= "Assault Rifle"

SWEP.ScopeScale 					= 0.4
SWEP.ReticleScale 					= 0.6

SWEP.Primary.NumShots				= 1		--how many bullets to shoot per trigger pull
SWEP.Primary.Damage					= 125	--base damage per bullet
SWEP.Primary.Spread					= 0.001	--define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy 			= 0.0001 -- ironsight accuracy, should be the same for shotguns

SWEP.Primary.SpreadMultiplierMax 	= 3.8 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 0.7 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 3.2 --How much the spread recovers, per second.

-- enter iron sight info and bone mod info below

SWEP.Secondary.IronFOV				= 60	-- How much you 'zoom' in. Less is more!

SWEP.data 							= {}
SWEP.data.ironsights				= 1

SWEP.IronSightsPos 					= Vector(-3.7, 20, -1.5)
SWEP.IronSightsAng 					= Vector(0, 0, 0)

SWEP.RunSightsPos 					= Vector(7.742, -5.271, 2.709)
SWEP.RunSightsAng 					= Vector(-11.58, 31.843, 0)

SWEP.VElements = {
	["ammo_counterV"] = { type = "Quad", bone = "b_gun", rel = "", pos = Vector(5.393, 0, 7.596), angle = Angle(180, 90, -116.362), size = 0.005, draw_func = nil}
	
}

SWEP.WElements = {
	["ammo_counterW"] = { type = "Quad", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6.8, 1.25, -6.7), angle = Angle(0, 90, -100.362), size = 0.005, draw_func = nil}
}
SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = {"halo_fmj","halo_ext_mag"}, order = 1 },
	[2] = { offset = { 0, 0 }, atts = {"halo_sprintattack"}, order = 1 },
}

DEFINE_BASECLASS( SWEP.Base )

function SWEP:Initialize()
	BaseClass.Initialize( self )
	
	local ply = self:GetOwner()
	
	if CLIENT then
		self.VElements["ammo_counterV"].draw_func = function( weapon )
			if self:Clip1() < 10 then
				draw.SimpleTextOutlined("0".. self:Clip1() .."", "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(16, 60, 80))
			else
				draw.SimpleTextOutlined(self:Clip1(), "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(16, 60, 80))
			end
		end
	
		self.WElements["ammo_counterW"].draw_func = function( weapon )
			if self:Clip1() < 10 then
				draw.SimpleTextOutlined("0".. self:Clip1() .."", "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			else
				draw.SimpleTextOutlined(self:Clip1(), "reach_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 60, 80))
			end
		end
	end
end
--[[
if CLIENT then

function SWEP:DetectionHUD(trace)

	if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
	color = Color(255,0,0,255)

	else
	color = Color( 0, 161, 255, 255 )
	end
	
	surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_ma5c"))
	surface.SetDrawColor( color )
	surface.DrawTexturedRect( ScrW()/2 - 25, ScrH()/2+25 - 50, 50, 50 )

	surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_ma5c_dyn"))
	surface.SetDrawColor( color )
	surface.DrawTexturedRect( ScrW()/2 - 25, ScrH()/2+25 - 50, 50, 50 )

end

function SWEP:DrawHUD()

	if self.Owner:InVehicle() then return end

		local Trace = {}
	Trace.start = self.Owner:GetShootPos()
	Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
	Trace.filter = { self.Owner, self.Weapon, 0 }
	Trace.mask = MASK_SHOT
	local tr = util.TraceLine(Trace)
	
	self:DetectionHUD(tr)
    
    
end

end
]]