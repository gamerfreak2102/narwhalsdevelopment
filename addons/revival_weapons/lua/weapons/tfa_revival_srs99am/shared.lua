if CLIENT then
    SWEP.WepSelectIcon             = surface.GetTextureID("vgui/hud/hr_swep_srs99")
    
    killicon.Add( "tfa_hr_swep_assault_rifle", "vgui/hud/hr_swep_srs99", color_white )
end

SWEP.Author                         = "ChaosNarwhal / Arthur"
SWEP.Contact                        = "ChaosNarwhal#9953"
SWEP.Purpose                        = "Created for revival using vuths models!"
SWEP.Base                           = "tfa_bash_base"
SWEP.Category                       = "Project Revival Armory"
SWEP.Manufacturer                   = "Misriah Armory"
SWEP.Spawnable                      = true
SWEP.AdminSpawnable                 = true

SWEP.Slot                           = 4             -- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos                        = 57            -- Position in the slot

SWEP.PrintName                      = "SRS-99AM"

SWEP.UseHands           = true
SWEP.ViewModel          = "models/vuthakral/halo/weapons/c_hum_srs99am.mdl"
SWEP.WorldModel         = "models/vuthakral/halo/weapons/w_srs99am.mdl"
SWEP.ViewModelFOV                   = 60
SWEP.HoldType                       = "ar2"

SWEP.DisableChambering              = false
SWEP.Primary.ClipSize               = 4
SWEP.Primary.DefaultClip            = 24


SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_SLASH -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName                     = "drc_halo_ar_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount                    = 1 --0 disables, otherwise, 1 in X chance

SWEP.Primary.Sound                  = Sound("drc.srs99d_fire")
SWEP.Primary.Ammo                   = "sniperpenetratedround"
SWEP.Primary.Automatic              = false
SWEP.Primary.RPM                    = 75
SWEP.Primary.Damage                 = 1300
SWEP.Primary.Knockback              = 1
SWEP.Primary.NumShots               = 1
SWEP.Primary.Spread                 = .00001                    --This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy           = .00001    -- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire                  = false
    
SWEP.Primary.KickUp                 = 0.2                   -- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown               = 0.2                   -- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal         = 0.2                   -- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor     = 0.45  --Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax    = 4.8 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement        = 0.8 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery         = 4.5 --How much the spread recovers, per second.

SWEP.BoltAction                     = false --Un-sight after shooting?
SWEP.BoltTimerOffset                = 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos                  = Vector(0, -20, 0)
SWEP.IronSightsAng                  = Vector(0, 0, 0)

SWEP.RunSightsPos                   = Vector(0, 0, -0.202)
SWEP.RunSightsAng                   = Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos                     = Vector(10.519, -8.502, 1)
SWEP.InspectAng                     = Vector(36.583, 53.466, 34.472)

SWEP.Primary.Range                  = 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff           = 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.

SWEP.CustomMuzzleFlash              = true
SWEP.MuzzleFlashEffect              = "sanctum2_rg_muzzle"

SWEP.TracerName                     = "drc_halo_sniper_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount                    = 1 --0 disables, otherwise, 1 in X chance
SWEP.ImpactEffect                   = "halo_sniper_impact"--Impact Effect

SWEP.TracerCount                    = 1 --0 disables, otherwise, 1 in X chance
SWEP.Secondary.ScopeZoom            = 6
SWEP.Scoped                         = true
SWEP.ScopeScale = 0
SWEP.BoltAction                     = false
SWEP.ScopeOverlayThreshold = 0.75
SWEP.IronSightsProgress = 0

SWEP.Attachments = {
    [1] = { offset = { 0, 0 }, atts = { "halo_ext_mag", "halo_damage_rof" }, order = 1 },
    [2] = { offset = { 0, 0 }, atts = { "halo_shortscope", "halo_longscope"}, order = 2},
}

DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

function SWEP:Think2(...) -- We're overriding Think2 without touching the main think function, which is called from there anyway
    BaseClass.Think2(self, ...) -- THE MOST IMPORTANT LINE! It calls the Think2 function of the parent class, which is the base itself


    if self.Owner:KeyPressed(IN_ATTACK2) and self:GetIronSights()==true then
        self.Weapon:EmitSound("vuthakral/halo/weapons/srs99c/zoom_in.wav")
    end
    
    if self.Owner:KeyReleased(IN_ATTACK2) and self:GetIronSights()==false then
        self.Weapon:EmitSound("vuthakral/halo/weapons/srs99c/zoom_out.wav")
    end
end

if CLIENT then

function SWEP:DetectionHUD(trace)

    if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
    color = Color(255,0,0,255)

    else
    color = Color( 0, 161, 255, 255 )
    end
    
    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_sr"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-8, ScrH()/2+44 - 50, 15, 15 )

end



function SWEP:DrawHUD()

    if self.Owner:InVehicle() then return end

    local Trace = {}
    Trace.start = self.Owner:GetShootPos()
    Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
    Trace.filter = { self.Owner, self.Weapon, 0 }
    Trace.mask = MASK_SHOT
    local tr = util.TraceLine(Trace)
    
    self:DetectionHUD(tr)  

    self.CLOldNearWallProgress = self.CLOldNearWallProgress or 0
    cam.Start3D() --Workaround for vec:ToScreen()
    cam.End3D()

    self:DoInspectionDerma()

    self:DrawHUDAmmo()

    if self.IronSightsProgress > self.ScopeOverlayThreshold then

    if self.SightsDown == false then return end

    local Scoped        = true
    local ScopeMat      = "models/vuthakral/halo/HUD/scope_sniper.png"
    local ScopeBlur     = true
    local ScopeBGCol    = Color(0, 0, 0, 200)
    local IronFOV       = 200
    local ScopeScale    = 0.65
    local ScopeWidth    = 1.75
    local ScopeHeight   = 1
    local ScopeYOffset = -1

    local w = ScrW()
    local h = ScrH()
    
    local ratio = w/h
    
    local ss = 4 * ScopeScale
    local sw = ScopeWidth
    local sh = ScopeHeight
    
    local wi = w / 10 * ss
    local hi = h / 10 * ss
    
    local Q1Mat = ScopeMat
    local Q2Mat = Q2Mat
    local Q3Mat = Q3Mat
    local Q4Mat = Q4Mat
    
    local YOffset = -ScopeYOffset
    
    surface.SetDrawColor( ScopeBGCol )
    
    surface.DrawRect( 0, (h/2 - hi * sh) * YOffset, w/2 - hi / 2 * sw * 2, hi * 2 )
    surface.DrawRect( w/2 + hi * sw, (h/2 - hi * sh) * YOffset, w/2 + wi * sw, hi * 2 )
    surface.DrawRect( 0, 0, w * ss, h / 2 - hi * sh )
    surface.DrawRect( 0, (h/2 + hi * sh) * YOffset, w * ss, h / 1.99 - hi * sh )
    
    if ScopeCol != nil then
        surface.SetDrawColor( ScopeCol )
    else
        surface.SetDrawColor( Color(0, 0, 0, 255) )
    end
    
    if Q1Mat == nil then
        surface.SetMaterial(Material("sprites/scope_arc"))
    else 
        surface.SetMaterial(Material(Q1Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 1, 1, 0, 0 )
    
    if Q2Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q2Mat))
    end
    surface.DrawTexturedRectUV( w / 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 0, 1, 1, 0 )
    
    if Q3Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q3Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, h/2, hi * sw, hi * sh, 1, 0, 0, 1 )
    
    if Q4Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q4Mat))
    end
    surface.DrawTexturedRectUV( w/2, h/2, hi * sw, hi * sh, 0, 0, 1, 1 )

    end

end




end