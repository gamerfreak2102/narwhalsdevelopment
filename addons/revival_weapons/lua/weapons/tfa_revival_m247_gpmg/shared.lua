if CLIENT then

	killicon.Add( "tfa_revival_m247_gpmg" , "VGUI/optre/m247_icon", Color(255, 0, 0, 255) )

	SWEP.WepSelectIcon = surface.GetTextureID("VGUI/optre/m247_icon")
end

-- Variables that are used on both client and server
SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "A Straight Splitlip killing MACHINE!"
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= true
SWEP.AdminSpawnable 				= true
SWEP.PrintName 						= "M247 GPMG"


SWEP.Slot							= 4				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 59			-- Position in the slot

SWEP.ViewModelFOV			= 70
SWEP.ViewModelFlip			= false
SWEP.UseHands			= true
SWEP.ViewModel 			= "models/vuthakral/halo/weapons/c_hum_m247.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_m247.mdl"
SWEP.ShowWorldModel         = true
SWEP.Base 				= "tfa_bash_base"

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true


SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 800 -- Melee bash damage
SWEP.Secondary.BashLength         = 70 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName 					= "drc_halo_ar_bullet" --Change to a string of your tracer name,or lua effect if chosen

SWEP.Primary.Sound			= Sound("drc.m247_fire")		-- script that calls the primary fire sound
SWEP.Primary.RPM				= 1200		-- This is in Rounds Per Minute
SWEP.Primary.HullSize = 1
SWEP.Primary.Knockback = 0
SWEP.Primary.ClipSize			= 200		-- Size of a clip
SWEP.Primary.DefaultClip		= 1000	-- Bullets you start with
SWEP.Primary.KickUp				= 0.13				-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.13			-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.001		-- Maximum up recoil (stock)
SWEP.Primary.StaticRecoilFactor 	= 0.38 					--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.Automatic			= true		-- Automatic/Semi Auto
SWEP.Primary.Ammo			= "357"	-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets


--SWEP.Secondary.ScopeZoom			= 5
SWEP.Secondary.UseACOG			= false -- Choose one scope type
SWEP.Secondary.UseMilDot		= false	-- I mean it, only one
SWEP.Secondary.UseSVD			= false	-- If you choose more than one, your scope will not show up at all
SWEP.Secondary.UseParabolic		= false
SWEP.Secondary.UseElcan			= false
SWEP.Secondary.UseGreenDuplex	= false
SWEP.Secondary.UseAimpoint		= false
SWEP.Secondary.UseMatador		= false


SWEP.Primary.NumShots	= 1		--how many bullets to shoot per trigger pull
SWEP.Primary.Damage		= 150	--base damage per bullet
SWEP.Primary.Spread		= 0.01	--define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = 0.001 -- ironsight accuracy, should be the same for shotguns


SWEP.SelectiveFire					= true 					--Allow selecting your firemode?
SWEP.DisableBurstFire				= false 				--Only auto/single?
SWEP.OnlyBurstFire					= false 				--No auto, only burst/single?
SWEP.DefaultFireMode 				= "auto" 					--Default to auto or whatev
SWEP.FireModeName 					= nil 


SWEP.Primary.SpreadMultiplierMax 	= 3.5 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= -0.7 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= -3.5 --How much the spread recovers, per second.

-- enter iron sight info and bone mod info below

SWEP.Secondary.IronFOV 				= 70 --Ironsights FOV (90 = same)
SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos = Vector(-4.8, -1, 0.305)
SWEP.IronSightsAng = Vector(0, 0.05, 0)

SWEP.RunSightsPos = Vector(0, 0, 0)
SWEP.RunSightsAng = Vector(-11.869, 20.129, -20.056)


SWEP.InspectPos = Vector(18.8, -5.653, -4.19)
SWEP.InspecAng = Vector(26.389, 48.549, 22.513)

SWEP.IronSightTime = 0.5 --The time to enter ironsights/exit it.
SWEP.NearWallTime = 0.5 --The time to pull up  your weapon or put it back down
SWEP.ToCrouchTime = 0.5 --The time it takes to enter crouching state
SWEP.WeaponLength = 80 --Almost 3 feet Feet.  This should be how far the weapon sticks out from the player.  This is used for calculating the nearwall trace.
SWEP.MoveSpeed = 0.9 --Multiply the player's movespeed by this.
SWEP.IronSightsMoveSpeed = 0.45 --Multiply the player's movespeed by this when sighting.

SWEP.LuaShellEject = true
SWEP.LuaShellEffect = "RifleShellEject"
SWEP.LuaShellEjectDelay = 0

SWEP.ShellAttachment            = "shell"       -- Should be "2" for CSS models or "shell" for hl2 models