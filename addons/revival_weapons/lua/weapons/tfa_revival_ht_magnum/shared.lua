if CLIENT then
SWEP.WepSelectIcon			=	 surface.GetTextureID( "vgui/entities/drchalo_magnum" )
	
killicon.Add( "tfa_revival_ht_magnum", "vgui/entities/drchalo_magnum", color_white )
end
-- Variables that are used on both client and server
SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "The 2552 modification package of the M6G is the latest revision of the M6-series."
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory High Tier"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= false
SWEP.AdminSpawnable 				= false
SWEP.PrintName						= "M6G High Tier"		-- Weapon name (Shown on HUD)


SWEP.Slot							= 2				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 59			-- Position in the slot

SWEP.ViewModelFOV				= 75
SWEP.ViewModelFlip				= false
SWEP.UseHands			= true
SWEP.ViewModel 			= "models/vuthakral/halo/weapons/c_hum_m6g_reach.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_m6g_reach.mdl"
SWEP.ShowWorldModel         	= true
SWEP.Base 						= "tfa_bash_base"

SWEP.PistolSlide = 1
SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName 				  = "drc_halo_pistol_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 				  = 1 --0 disables, otherwise, 1 in X chance
SWEP.HoldType 						= "revolver"

SWEP.Primary.Sound				= Sound("drc.m6gr_fire")		-- script that calls the primary fire sound
SWEP.Primary.RPM				= 700		-- This is in Rounds Per Minute
SWEP.DisableChambering 			= true
SWEP.Primary.ClipSize			= 8			-- Size of a clip
SWEP.Primary.ClipSizeFmj		= 7
SWEP.Primary.ClipSize_ExtRifle	= 12
SWEP.Primary.HullSize = 3
SWEP.Primary.Knockback = 0
SWEP.Primary.DefaultClip		= 1200		-- Bullets you start with
SWEP.Primary.KickUp				= 0.07		-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.07		-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.0		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= false		-- Automatic/Semi Auto
SWEP.Primary.Ammo			= "pistol"		-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets

SWEP.ShellTime			= .5

SWEP.Scoped 						= true

SWEP.data 							= {}
SWEP.data.ironsights				= 1

if CLIENT then
	SWEP.Secondary.ScopeTable = {
		scopetex = surface and surface.GetTextureID("magnum_scope/magnum_scope") or 0,
		reticletex = surface and surface.GetTextureID("crosshairs/magnum_hr") or 0
	}
	SWEP.ScopeScale 				= 0.7
	SWEP.ReticleScale 				= 0.2
end


SWEP.LuaShellEject 					= false
SWEP.BlowbackEnabled 				= false
--SWEP.BlowbackVector 				= Vector(0,1,0)
--SWEP.BlowbackAngle          		= Angle(5, 0, 0)
SWEP.BlowbackAllowAnimation 		= true -- Allow playing shoot animation with blowback?
SWEP.Blowback_Shell_Effect 			= "ShellEject"
SWEP.Blowback_PistolMode 			= true
SWEP.Blowback_Only_Iron     		= false -- Only do blowback on ironsights
SWEP.BlowbackBoneMods 				= {
	["frame slide"] = { scale = Vector(1, 1, 1), pos = Vector(-2, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.Secondary.UseACOG				= false -- Choose one scope type
SWEP.Secondary.UseMilDot			= false	-- I mean it, only one
SWEP.Secondary.UseSVD				= false	-- If you choose more than one, your scope will not show up at all
SWEP.Secondary.UseParabolic			= false
SWEP.Secondary.UseElcan				= false
SWEP.Secondary.UseGreenDuplex		= false
SWEP.Secondary.UseAimpoint			= false
SWEP.Secondary.UseMatador			= false

SWEP.AmmoTypeStrings	= {["pistol"] = "12.7x40mmn M225 SAP-HE"}
SWEP.Type = "Magnum"

SWEP.Primary.NumShots	= 1		--how many bullets to shoot per trigger pull
SWEP.Primary.Damage		= 250	--base damage per bullet
SWEP.Primary.Spread		= 0.0001	--define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = 0.0001 -- ironsight accuracy, should be the same for shotguns

-- enter iron sight info and bone mod info below

SWEP.IronSightsPos = Vector(0, -20, 0)
SWEP.IronSightsAng = Vector(-0.203, -0.202, 0)
SWEP.RunSightsPos = Vector(13.038, -20, -1.829)
SWEP.RunSightsAng = Vector(-3.462, 59.97, 0.769)

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = {"halo_fmj","halo_ext_mag"}, order = 1 },
	[2] = { offset = { 0, 0 }, atts = {"halo_sprintattack"}, order = 1 },
}

DEFINE_BASECLASS( SWEP.Base )

function SWEP:PreDrawViewModel(vm, wep, ply)
	if game.SinglePlayer() then return end -- Find the singleplayer compatible version inside of Think() because why.
	 -- Thank you Yurie holy fuck why are lerps so mean to me
	
	local ammo = ply:GetActiveWeapon():Clip1()
	local slide = 1
	local health = ply:Health()
	
	if ply != LocalPlayer() then return end

	if ammo == 0 then
		vm:SetPoseParameter("drc_emptymag", 0) -- One is Fully Closed
	else
		vm:SetPoseParameter("drc_emptymag", 1) -- One is Fully Closed
	end

	return false
end
