if CLIENT then
    SWEP.WepSelectIcon             = surface.GetTextureID("vgui/entities/drchalo_ma5c")
    
    killicon.Add( "tfa_revival_ma5c", "vgui/entities/drchalo_ma5c", color_white )
end

SWEP.Author                         = "ChaosNarwhal / Arthur"
SWEP.Contact                        = "ChaosNarwhal#9953"
SWEP.Purpose                        = "Created for revival using vuths models!"
SWEP.Base                           = "tfa_bash_base"
SWEP.Category                       = "Project Revival Armory"
SWEP.Manufacturer                   = "Misriah Armory"
SWEP.Spawnable                      = true
SWEP.AdminSpawnable                 = true

SWEP.Slot							= 3				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 50			-- Position in the slot

SWEP.PrintName 						= "MA5C ICWS"

SWEP.UseHands           = true
SWEP.ViewModel =        "models/vuthakral/halo/weapons/c_hum_ma5c.mdl"
SWEP.WorldModel         = "models/vuthakral/halo/weapons/w_ma5c.mdl"
SWEP.ViewModelFOV 					= 70
SWEP.HoldType 						= "ar2"

SWEP.Scoped 						= false

SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize 				= 32
SWEP.Primary.DefaultClip 			= 352

SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)
SWEP.TracerName                     = "drc_halo_ar_bullet" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount                    = 1 --0 disables, otherwise, 1 in X chance

SWEP.Primary.Sound 					= Sound("drc.ma5c_fire")
SWEP.Primary.Ammo 					= "ar2"
SWEP.Primary.Automatic 				= true
SWEP.Primary.RPM 					= 750
SWEP.Primary.Damage 				= 125
SWEP.Primary.NumShots 				= 1
SWEP.Primary.HullSize = 1
SWEP.Primary.Knockback = 0
SWEP.Primary.Spread					= .0003					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .0001	-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire 					= false
	
SWEP.Primary.KickUp					= 0.1					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0.1					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.01					-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.41 	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 4.8 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 1.4 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 4.5 --How much the spread recovers, per second.

SWEP.Secondary.IronFOV 				= 70 --Ironsights FOV (90 = same)
SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos      = Vector(-3.75, -2, -1.25)
SWEP.IronSightsAng      = Vector(1, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(10.519, -8.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.Primary.Range 					= 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.
SWEP.LuaShellEject 					= false
SWEP.Tracer							= 0
SWEP.TracerCount 					= 1

SWEP.BlowbackEnabled                = true
--SWEP.BlowbackVector               = Vector(0,1,0)
--SWEP.BlowbackAngle          = Angle(5, 0, 0)
SWEP.BlowbackAllowAnimation = false -- Allow playing shoot animation with blowback?
SWEP.Blowback_Shell_Effect          = "ShellEject"
SWEP.Blowback_PistolMode            = true
SWEP.Blowback_Only_Iron     = false -- Only do blowback on ironsights
SWEP.BlowbackBoneMods                   = {
    ["ophandle"] = { scale = Vector(1, 1, 1), pos = Vector(-2, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.VElements = {
    ["ammo_counter"] = { type = "Quad", bone = "gun", rel = "", pos = Vector(4.525, 0, 6.44), angle = Angle(0, -90, 70.733), size = 0.009, draw_func = nil}
}

SWEP.WElements = {
    ["ammo_counterW"] = { type = "Quad", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.65, 0.675, -6.225), angle = Angle(0, 90, -100), size = 0.01, draw_func = nil}
}

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = { "halo_ext_mag", "halo_fmj" }, order = 1 }
}

DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

function SWEP:Initialize()
    BaseClass.Initialize( self )
    
    local ply = self:GetOwner()
    
    if CLIENT then
        self.VElements["ammo_counter"].draw_func = function( weapon )
            if self:Clip1() < 10 then
                draw.SimpleTextOutlined("0".. self:Clip1() .."", "h3_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 160, 180))
            else
                draw.SimpleTextOutlined(self:Clip1(), "h3_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 160, 180))
            end
        end
        
        self.WElements["ammo_counterW"].draw_func = function( weapon )
            if self:Clip1() < 10 then
                draw.SimpleTextOutlined("0".. self:Clip1() .."", "h3_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 160, 180))
            else
                draw.SimpleTextOutlined(self:Clip1(), "h3_ammocounter", 0, 12.5, Color(37,141,170,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(16, 160, 180))
            end
        end
    end
end
