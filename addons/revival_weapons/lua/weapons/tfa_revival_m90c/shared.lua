if CLIENT then
SWEP.WepSelectIcon        = surface.GetTextureID( "vgui/entities/drchalo_shotgun" )
    
    killicon.Add( "tfa_revival_m90c", "vgui/entities/drchalo_shotgun", color_white )
end

SWEP.Author 						= "ChaosNarwhal / Arthur"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "Created for revival using vuths models!"
SWEP.Base 							= "tfa_bash_base"
SWEP.Category 						= "Project Revival Armory"
SWEP.Manufacturer 					= "Misriah Armory"
SWEP.Spawnable 						= true
SWEP.AdminSpawnable 				= true

SWEP.Slot							= 3				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 54			-- Position in the slot

SWEP.PrintName 						= "M90A"

SWEP.UseHands			= true
SWEP.ViewModel 			= "models/vuthakral/halo/weapons/c_hum_m90.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_m90.mdl"
SWEP.ViewModelFOV 					= 60
SWEP.HoldType 						= "shotgun"

SWEP.Scoped 						= false

SWEP.Shotgun 						= true

SWEP.DisableChambering 				= false
SWEP.Primary.ClipSize 				= 6
SWEP.Primary.DefaultClip 			= 36

SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)

SWEP.Primary.Sound 					= Sound("drc.m90_fire")
SWEP.Primary.Ammo 					= "buckshot"
SWEP.Primary.Automatic 				= false
SWEP.Primary.RPM 					= 70
SWEP.Primary.Damage 				= 85
SWEP.Primary.HullSize = 1
SWEP.Primary.NumShots 				= 15
SWEP.Primary.Spread					= .065					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .065	-- Ironsight accuracy, should be the same for shotguns
SWEP.FireModeName 					= "Pump Action"
	
SWEP.Primary.KickUp					= 0.48					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0.3					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.3					-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.41 	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.KickUp					= 1.5					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 1.2					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.5					-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.25 

SWEP.Secondary.IronFOV 				= 70 --Ironsights FOV (90 = same)
SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos 					= Vector(-4.65, -2, 0)
SWEP.IronSightsAng 					= Vector(0, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(10.519, -8.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.Primary.Range 					= 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.

SWEP.CustomMuzzleFlash 				= true
SWEP.MuzzleFlashEffect 				= "gdcw_muzzle_heat"

SWEP.TracerCount 					= 0 --0 disables, otherwise, 1 in X chance
SWEP.BlowbackEnabled 				= true
SWEP.BlowbackVector 				= Vector(0.075,-3,0.00)
SWEP.BlowbackBoneMods 					= {
	--["Bolt"] = { scale = Vector(1, 1, 1), pos = Vector(-3.537, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.LuaShellEject 					= true
SWEP.LuaShellEffect 				= "ShotgunShellEject"
SWEP.LuaShellEjectDelay 			= 0