if CLIENT then
SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/hud/hr_swep_needle_rifle" )
	
	killicon.Add( "tfa_revival_needlerifle", "vgui/hud/hr_swep_needle_rifle", color_white )
end
-- Variables that are used on both client and server
SWEP.Author                         = "ChaosNarwhal / Arthur"
SWEP.Contact                        = "ChaosNarwhal#9953"
SWEP.Purpose                        = "The T-31 rifle is effectively a rifle variant of the T-33 GML."
SWEP.Base                           = "tfa_bash_base"
SWEP.Category                       = "Project Revival Armory xeno"
SWEP.Manufacturer                   = "Misriah Armory"
SWEP.Spawnable                      = true
SWEP.AdminSpawnable                 = true
SWEP.PrintName 						= "Needle Rifle"

SWEP.Slot					= 2							-- Slot in the weapon selection menu
SWEP.SlotPos				= 4							-- Position in the slot
SWEP.DrawAmmo				= true						-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox		= false						-- Should draw the weapon info box
SWEP.BounceWeaponIcon   	= false						-- Should the weapon icon bounce?
SWEP.DrawCrosshair			= true						-- Set false if you want no crosshair from hip
SWEP.Weight					= 30						-- Rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo			= true						-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true						-- Auto switch from if you pick up a better weapon
SWEP.XHair					= true						-- Used for returning crosshair after scope. Must be the same as DrawCrosshair
SWEP.BoltAction				= false						-- Is this a bolt action rifle?
SWEP.HoldType 				= "ar2"						-- how others view you carrying the weapon
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive 
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg and crossbow make for good sniper rifles

SWEP.ViewModelFOV			= 60
SWEP.ViewModelFlip			= false
SWEP.UseHands			= true
SWEP.ViewModel = 		"models/vuthakral/halo/weapons/c_hum_needlerifle.mdl"
SWEP.WorldModel			= "models/vuthakral/halo/weapons/w_needlerifle.mdl"
SWEP.ShowWorldModel         = true

SWEP.DisableChambering = true

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.MuzzleAttachment           = "2"
SWEP.Secondary.CanBash            = true -- set to false to disable bashing
SWEP.Secondary.BashDamage         = 300 -- Melee bash damage
SWEP.Secondary.BashLength         = 50 -- Length of bash melee trace in units
SWEP.Secondary.BashDelay          = 0.2 -- Delay (in seconds) from bash start to bash attack trace
SWEP.Secondary.BashDamageType     = DMG_ENERGYBEAM -- Damage type (DMG_ enum value)
SWEP.Secondary.BashEnd            = nil -- Bash end time (in seconds), defaults to animation end if undefined
SWEP.Secondary.BashInterrupt      = true -- Bash attack interrupts everything (reload, draw, whatever)

SWEP.Primary.Sound				= Sound("drc.needlerifle_fire")		-- script that calls the primary fire sound
SWEP.Primary.RPM				= 200		-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 21		-- Size of a clip
SWEP.Primary.DefaultClip		= 120	-- Bullets you start with
SWEP.Primary.KickUp				= 0.5				-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.151			-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.1	-- Maximum up recoil (stock)
SWEP.Primary.StaticRecoilFactor = 0.3 		--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.
SWEP.Primary.Automatic			= true	-- Automatic/Semi Auto
SWEP.Primary.Ammo			= "smg1"	-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets

SWEP.ShellTime			= .5

SWEP.Scoped 						= true

SWEP.MuzzleFlashEffect = "needle_rifle_muzzle"
SWEP.ImpactEffect = "needle_rifle_beam_hit"

SWEP.TracerCount = 1
SWEP.TracerName = "needle_rifle_beam"


SWEP.AmmoTypeStrings	= {["smg1"] = "Crystalline Explosive Projectiles"}
SWEP.Type = "Needle Rifle"

SWEP.Secondary.IronFOV			= 40		-- How much you 'zoom' in. Less is more! 

SWEP.Primary.NumShots			= 1		--how many bullets to shoot per trigger pull
SWEP.Primary.Damage				= 30	--base damage per bullet
SWEP.Primary.Spread				= 0.01	--define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy 		= 0.002 -- ironsight accuracy, should be the same for shotguns

SWEP.Primary.SpreadMultiplierMax 	= 3.8 		--How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 1.49 	--What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 3.103 		--How much the spread recovers, per second.

SWEP.ScopeScale = 0

-- enter iron sight info and bone mod info below

SWEP.IronSightsPos = Vector(-9.343, -20, 0.833)
SWEP.IronSightsAng = Vector(0, 0.086, 0)

SWEP.RunSightsPos = Vector(7.742, -5.271, 2.709)
SWEP.RunSightsAng = Vector(-11.58, 31.843, 0)

DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

function SWEP:Initialize()
	BaseClass.Initialize( self )
    
    local ply = self:GetOwner()

	CreateMaterial( "revival_needleRifle", "VertexLitGeneric", {
  	["$basetexture"] = "models/vuthakral/halo/weapons/needlerifle/needle_rifle_diffuse",
  	["$bumpmap"] = "models/vuthakral/halo/weapons/needlerifle/needle_rifle_normal",
  	["$lightwarptexture"] =	"models/vuthakral/halo/weapons/lw_metaldull",

  	["$detail"]	= "models/vuthakral/halo/detail/worn_metal",
	["$detailscale"] = 3,
	["$detailblendmode"] = 0,
	["$detailblendfactor"] =	2,

	["$phong "] = 1,
	["$phongboost"] = 24,
	["$phongexponenttexture"] = "models/vuthakral/halo/weapons/needlerifle/needle_rifle_exp",
	["$phongtint"] = "[ 2 1 2 ]",
	["$phongfresnelranges"] = "[ .25 .3 2 ]",
	["$color2"] = "[ .2 .115 .2 ]",
} )

end

if CLIENT then

DEFINE_BASECLASS( SWEP.Base )

function SWEP:PreDrawViewModel(vm, wep, ply)

	vm:SetSubMaterial(3, "!revival_needleRifle")

	local ammo = ply:GetActiveWeapon():Clip1()

	local AmmoCL = Lerp(FrameTime() * 25, AmmoCL or ammo, ammo)

	
	if ply != LocalPlayer() then return end


	if self.Primary.Ammo != nil && vm:GetPoseParameter("drc_ammo") != nil then
		vm:SetPoseParameter("drc_ammo", AmmoCL / self.Primary.ClipSize)
	end

	return false
end

function SWEP:DetectionHUD(trace)

	if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
	color = Color(255,0,0,255)

	else
	color = Color( 0, 161, 255, 255 )
	end
	
	surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_nr"))
	surface.SetDrawColor( color )
	surface.DrawTexturedRect( ScrW()/2-8, ScrH()/2+44 - 50, 15, 15 )

	surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_nr_dyn"))
	surface.SetDrawColor( color )
	surface.DrawTexturedRect( ScrW()/2-8, ScrH()/2+44 - 50, 15, 15 )

end

function SWEP:DrawHUD()

	if self.Owner:InVehicle() then return end

	local Trace = {}
	Trace.start = self.Owner:GetShootPos()
	Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
	Trace.filter = { self.Owner, self.Weapon, 0 }
	Trace.mask = MASK_SHOT
	local tr = util.TraceLine(Trace)
	
	self:DetectionHUD(tr)  

	self.CLOldNearWallProgress = self.CLOldNearWallProgress or 0
    cam.Start3D() --Workaround for vec:ToScreen()
    cam.End3D()

    self:DoInspectionDerma()

    self:DrawHUDAmmo()

	if self.IronSightsProgress > self.ScopeOverlayThreshold then

	if self.SightsDown == false then return end

	local Scoped		= true
	local ScopeMat		= "models/vuthakral/halo/HUD/scope_needlerifle.png"
	local ScopeBlur 	= true
	local ScopeBGCol 	= Color(0, 0, 0, 200)
	local IronFOV		= 200
	local ScopeScale	= 0.65
	local ScopeWidth	= 1.2
	local ScopeHeight	= 1
	local ScopeYOffset = -1

	local w = ScrW()
	local h = ScrH()
	
	local ratio = w/h
	
	local ss = 4 * ScopeScale
	local sw = ScopeWidth
	local sh = ScopeHeight
	
	local wi = w / 10 * ss
	local hi = h / 10 * ss
	
	local Q1Mat = ScopeMat
	local Q2Mat = Q2Mat
	local Q3Mat = Q3Mat
	local Q4Mat = Q4Mat
	
	local YOffset = -ScopeYOffset
	
	surface.SetDrawColor( ScopeBGCol )
	
	surface.DrawRect( 0, (h/2 - hi * sh) * YOffset, w/2 - hi / 2 * sw * 2, hi * 2 )
	surface.DrawRect( w/2 + hi * sw, (h/2 - hi * sh) * YOffset, w/2 + wi * sw, hi * 2 )
	surface.DrawRect( 0, 0, w * ss, h / 2 - hi * sh )
	surface.DrawRect( 0, (h/2 + hi * sh) * YOffset, w * ss, h / 1.99 - hi * sh )
	
	if ScopeCol != nil then
		surface.SetDrawColor( ScopeCol )
	else
		surface.SetDrawColor( Color(0, 0, 0, 255) )
	end
	
	if Q1Mat == nil then
		surface.SetMaterial(Material("sprites/scope_arc"))
	else 
		surface.SetMaterial(Material(Q1Mat))
	end
	surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 1, 1, 0, 0 )
	
	if Q2Mat == nil then
		if Q1Mat == nil then
			surface.SetMaterial(Material("sprites/scope_arc"))
		else
			surface.SetMaterial(Material(Q1Mat))
		end
	else 
		surface.SetMaterial(Material(Q2Mat))
	end
	surface.DrawTexturedRectUV( w / 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 0, 1, 1, 0 )
	
	if Q3Mat == nil then
		if Q1Mat == nil then
			surface.SetMaterial(Material("sprites/scope_arc"))
		else
			surface.SetMaterial(Material(Q1Mat))
		end
	else 
		surface.SetMaterial(Material(Q3Mat))
	end
	surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, h/2, hi * sw, hi * sh, 1, 0, 0, 1 )
	
	if Q4Mat == nil then
		if Q1Mat == nil then
			surface.SetMaterial(Material("sprites/scope_arc"))
		else
			surface.SetMaterial(Material(Q1Mat))
		end
	else 
		surface.SetMaterial(Material(Q4Mat))
	end
	surface.DrawTexturedRectUV( w/2, h/2, hi * sw, hi * sh, 0, 0, 1, 1 )

	end

end

end