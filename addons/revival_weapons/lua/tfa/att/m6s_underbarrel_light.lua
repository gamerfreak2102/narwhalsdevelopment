if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6-series flashlight module"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Flashlight module with a powerfule LED lamp."}
ATTACHMENT.Icon = "entities/m6/m6s_underbarrel_light.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6 Flashlight"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[2] = 0
	},
	["Bodygroups_W"] = {
		[2] = 0
	},
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end