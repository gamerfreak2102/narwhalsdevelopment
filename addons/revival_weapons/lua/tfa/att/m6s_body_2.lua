if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6G frame w/ KFA-2 SL Sights"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "M6G variant frame w/ KFA-2 smart linked optics module. Fires AP rounds."}
ATTACHMENT.Icon = "entities/m6/m6s_body_optic.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6G"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[1] = 1
	},
	["Bodygroups_W"] = {
		[1] = 1
	},
	
	["Primary"] = { 
	["Sound"] = Sound("Weapon_reachishi_m6g.Single"),
	["Damage"] = 125,
	["KickUp"] = 0.4,
	["StaticRecoilFactor"] = 0.4,
	},
	
	["IronSightsPos"] = function(wep, val) return wep.IronSightsPos_KFA or val, true end,
	["IronSightsAng"] = function(wep, val) return wep.IronSightsAng_KFA or val, true end,
	["Secondary"] = {
		["IronFOV"] = function(wep, val) return val * 0.85 end
	},
	["IronSightTime"] = function(wep, val) return val * 1.15 end,
	["IronSightsSensitivity"] = 0.3,
	["BlowbackVector"] = Vector(0.075,-3,0.00),
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end