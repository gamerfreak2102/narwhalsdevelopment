if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Armalite Light module"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Armalite LED rail-mounted flashlight."}
ATTACHMENT.Icon = "entities/m7/smg_light.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "Flashlight"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[5] = 1
	},
	["Bodygroups_W"] = {
		[4] = 1
	},
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end

