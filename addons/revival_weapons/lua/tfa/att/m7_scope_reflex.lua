if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "ASV Reflex Sight"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Reflex Sight & Ammo counter", TFA.AttachmentColors["+"], "Quick target acquisition"}
ATTACHMENT.Icon = "entities/m7/smg_reflex.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "ASV Reflex"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[3] = 2
	},
	["Bodygroups_W"] = {
		[2] = 2
	},
	["IronSightsPos"] = function(wep, val) return wep.IronSightsPos_REFLEX or val, true end,
	["IronSightsAng"] = function(wep, val) return wep.IronSightsAng_REFLEX or val, true end,
	["Secondary"] = {
		["IronFOV"] = function(wep, val) return val * 0.85 end
	},
	["IronSightTime"] = function(wep, val) return val * 0.95 end,
	["IronSightsSensitivity"] = 0.25,
	["BlowbackVector"] = Vector(0.05,-3,0.00)
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end