if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6-series silencer"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Silencer for those times when you dont wanna wake your wife."}
ATTACHMENT.Icon = "entities/m6/m6_silencer.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6 Silencer"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[4] = 1
	},
	["Bodygroups_W"] = {
		[4] = 1
	},

	["VElements"] = {
		["silencer"] = {
			["active"] = true
		}
	},
	["WElements"] = {
		["silencer"] = {
			["active"] = true
		}
	},

	["Primary"] = { ["Sound"] = Sound("Weapon_reachishi_m6_sil.Single") },
	["MuzzleAttachmentMod"] = 3,
	["MuzzleFlashEffect"] = "tfa_muzzleflash_silenced"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end