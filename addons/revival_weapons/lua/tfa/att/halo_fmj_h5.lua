if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Full Metal Jacket Rounds"

ATTACHMENT.Description = {
	TFA.AttachmentColors["-"], "Decreases rounds to 28.",
	TFA.AttachmentColors["+"], "Increases DMG by 25."
}

ATTACHMENT.ShortName = "DMG+"

local function checkSeqExists(wep, name)
	if not IsValid(wep) then return false end
	local owner = wep:GetOwner() or NULL
	if not wep:IsValid() then return false end
	local vm = owner:GetViewModel() or NULL
	if not vm:IsValid() then return false end
	local id = vm:LookupSequence(name)
	if id >= 0 then return true end
	return false
end

ATTACHMENT.WeaponTable = {
	["Primary"] = {
		["ClipSize"] = function(wep, val)
			return 28
		end,
		["Damage"] = function(wep, val)
			return 125
		end,
	},
}

function ATTACHMENT:Attach(wep)
	wep:Unload()
end

function ATTACHMENT:Detach(wep)
	wep:Unload()
end

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end
