if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6-series handguard"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Handguard for additional stability."}
ATTACHMENT.Icon = "entities/m6/m6_handguard_max.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6 Handguard"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[3] = 1
	},
	["Bodygroups_W"] = {
		[3] = 1
	},
	["Primary"] = {
		["KickUp"] = function(wep,stat) return (stat - 0.15) end,
		["StaticRecoilFactor"] = function(wep,stat) return (stat - 0.15) end
	},
	["MoveSpeed"] = 0.95,
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end