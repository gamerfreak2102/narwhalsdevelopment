if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6-series silencer"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Silencer built for use on small-arms."}
ATTACHMENT.Icon = "entities/m7/smg_silencer.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6 Silencer"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[4] = 1
	},
	["Bodygroups_W"] = {
		[3] = 1
	},
	["Primary"] = {
		["Sound"] = Sound("Weapon_reachishi_m7s.Single"),
		["KickUp"] = function(wep,stat) return (stat - 0.05) end,
		["StaticRecoilFactor"] = function(wep,stat) return (stat - 0.05) end
	},
	["MuzzleAttachmentMod"] = 2,
	["MuzzleFlashEffect"] = "tfa_muzzleflash_silenced",
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end