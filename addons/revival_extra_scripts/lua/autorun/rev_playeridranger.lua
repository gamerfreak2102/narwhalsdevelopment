local ConVars = {}
local HUDWidth
local HUDHeight

local Color = Color
local CurTime = CurTime
local cvars = cvars
local draw = draw
local GetConVar = GetConVar
local hook = hook
local IsValid = IsValid
local Lerp = Lerp
local math = math
local pairs = pairs
local ScrW, ScrH = ScrW, ScrH
local SortedPairs = SortedPairs
local string = string
local surface = surface
local table = table
local timer = timer
local tostring = tostring

local PMeta = FindMetaTable( "Player" )

local PlayerTrace = {
    mask = MASK_SOLID,
    dist = 125,
}

function PMeta:GetCloaked()
    local Col = self:GetColor()
    if Col.a <= 0 then
        return true
    end
    if self:GetNoDraw() or self:IsDormant() then 
        return true 
    end
    return false
end

hook.Add( "HUDDrawTargetID", "RevivalIDSystem", function()
    local Ply = LocalPlayer()
	
	local text = "ERROR"

    local traced = table.Copy(PlayerTrace) -- copy the table to tracedata, or ,traced so it doens't get overridden
    traced.start = Ply:EyePos()
    traced.endpos = traced.start + Ply:EyeAngles():Forward() * traced.dist
    traced.owner = Ply
    traced.filter = Ply

    local htr = util.TraceLine(traced)
    local ent = htr.Entity
	
    if (htr.Hit and ent) and ent:IsPlayer() then	
        if not ent:GetCloaked() then
            local Bh,Th = ent:GetHull()
            local Pos = ent:GetPos() + ent:OBBCenter()
            local TeamCol = team.GetColor( ent:Team() )
            Pos.z = Pos.z + (Th.z/1.8)

            local PosToScreen = Pos:ToScreen()

            local Text = ent:GetName() or ent:GetNick()
            local font = "GModNotify"
            surface.SetFont( font )
			local w, h = surface.GetTextSize( Text )
            draw.SimpleText( Text, font,PosToScreen.x-(w/2), PosToScreen.y+5, TeamCol )

            local Health = math.floor((ent:Health() / ent:GetMaxHealth()) * 100)
            local Text = "HP:" .. Health .. "%"
			local font = "DermaDefault"
			surface.SetFont( font )
            local w, h = surface.GetTextSize( Text )
            
            draw.SimpleText( Text, font,PosToScreen.x-(w/2), PosToScreen.y+30, Color(255,255,255) )
        end
    end
    return false
end)

local function JesseChaosJoinMessage(ply)

	local ChaosNarwhal = "Impending Doom Approaches."
	local Jesse = "The black swordsman has come....."
	
	if ply:SteamID() == "STEAM_0:0:68228942" then
		PrintMessage( HUD_PRINTTALK, Jesse )
	end
	
	if ply:SteamID() == "STEAM_0:1:59480742" then
		PrintMessage( HUD_PRINTTALK, ChaosNarwhal )
	end
	
end

hook.Add("PlayerInitialSpawn","JesseChaosJoinMessage",JesseChaosJoinMessage)