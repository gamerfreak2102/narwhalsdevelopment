RevivalDmgScaling = RevivalDmgScaling or {}

RevivalDmgScaling = {
    ["tfa_revival_ht_dmr"] = 3,
    ["tfa_revival_ht_magnum"] = 3,
	["tfa_revival_br55"] = 3,
	["tfa_revival_m394"] = 3,
	["tfa_revival_m6s"] = 3,
	["tfa_revival_m6g"] = 3,
	["tfa_revival_br85"] = 3,
	["tfa_revival_vk78"] = 3,
	["tfa_revival_br85_22"] = 3,
	["tfa_revival_vk78_22"] = 3,
}
-- Player Damage Scaling
function ScaleHeadShot(ply, hitgroup, dmginfo)

	if not IsValid( ply ) then return end
	
	local attacker = dmginfo:GetAttacker()
	
	local Wep = attacker:GetActiveWeapon()
	
	if (hitgroup == HITGROUP_HEAD) then
		if IsValid(Wep) then
		
			local WepClass = Wep:GetClass()
			--if ply.Shield_System.Shield <= 0 then
				if RevivalDmgScaling[WepClass] then
					dmginfo:ScaleDamage(RevivalDmgScaling[WepClass])
				else
					dmginfo:ScaleDamage(1.5)
				end
			--end
		end
	end
	
end
--NPC Damage Scaling
function ScaleHeadShotNPC(npc, hitgroup, dmginfo)

	if not IsValid( npc ) then return end
	
	local attacker = dmginfo:GetAttacker()
	
	local Wep = attacker:GetActiveWeapon()
	
	if (hitgroup == HITGROUP_HEAD) then
		if IsValid(Wep) then
		
			local WepClass = Wep:GetClass()
			
			if RevivalDmgScaling[WepClass] then
				dmginfo:ScaleDamage(RevivalDmgScaling[WepClass])
			else
				dmginfo:ScaleDamage(1.5)
			end
		end
	end
	
end

hook.Add("ScaleNPCDamage","HeadShotScaling",ScaleHeadShotNPC) --Create the hook for the NPC Damage Scale
hook.Add("ScalePlayerDamage","HeadShotScaling",ScaleHeadShot) --Create the hook for the Player Damage Scale